# Projektiblogi-pohja
## Ketterä kehitysprojekti, syksy 2019
### Tekijä: 1804189 Mika Marjomaa


# HUOM! KURSSIBLOGI ON 'LAAJA', JOTEN ASIOITA LÖYTYY USEAMMASTA DOKUMENTISTA JA KUN TÄMÄN MAKSIMI MERKKIMÄÄRÄ ON 100K, NIIN TIEDOSTOJA ON USEAMPI. PARHAITEN KOKO KURSSIBLOGIN LÖYTÄÄ OMASTA REPOSTANI, LINKKI ALHAALLA (GITLAB).

### Linkki omiin dokumentteihin
- https://gitlab.com/Maici/agile2019


- **Kurssiblogi**
- https://gitlab.com/Maici/agile2019/blob/master/BlogiBackup.md
- https://gitlab.com/Maici/agile2019/blob/master/BlogiBackupOSA2.md

- **Oma ajanseuranta**
- https://gitlab.com/Maici/agile2019/blob/master/BlogiAjanSeuranta.md

- **Kurssin retrospektiivi omalta kannaltani**
- https://gitlab.com/Maici/agile2019/blob/master/BlogiRetro.md

- **Ryhmän sekä ryhmän jäsenten arviointi**
- https://gitlab.com/Maici/agile2019/blob/master/BlogiArviointi.md



### Scrum listan todentaminen

- Pääsääntöisesti kannan vastuun Scrum listan asioita, jotka ovat epäonnistuneet kurssilla ryhmässämme.

- Omasta mielestäni olen 'joutunut' tekemään jokaisesta scrum roolista löytyviä tehtäviä. Mistä tämä johtuu pitää selvittää myöhemmin.

- 1.
    - 1.1
        - Olen pyrkinyt tuoteomistajana miettimään, mitä meidän tulisi tehdä seuraavassa sprintissä. Alhaalta löytyy osa sprinttien suunnitteluista. Puuttuvat sprintti suunnittelut löytyvät mahdollisesti, jostain dokumenteista gitlab repossa.
            - https://gitlab.com/Maici/agile2019/tree/master/Sprinttien_Suunnittelut

    - 1.2
        - Kursin myötä on tullut selväksi se, että ryhmän sisällä ei ole kauheasti taitoa toteuttaa tuoteideaa, joten olen matkan varrella miettynyt uudestaan, mitä pystymme tekemään ja näin ollen laittanut asioita järjestykseen, mitä pitäisi olla tehtynä. Olen käyttänyt tässä myös hyväksi Pekan kommmentteja tuotteestamme ja näin ollen jättänyt viilailun taka-alalle ja priorisoinut tärkeiden toiminnallisuuksien valmistumista. Toisaalta, kun kehitystiimissä on vain yksi tekijä, on priorisointi osoittautunut todella tärkeäksi (saisimme edes jotain aikaiseksi).
            - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/86/Pekan-tuokiot

    - 1.3
        - Olen ollut mukana tiimin työskentelyssä tiivistii. Alussa pidin Daily Scrummeja, joissa puutuin epäkohtiin työskentelyssä. Olen autellut ryhmämme toimintaa kurssin ajan, kuten selventänyt ideoitani ja vastannut kysymksiin. Olen osallistunut jokaiseen ryhmämme tilaisuuteen, paitsi yhteen Daily Scrumiin.
            - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/10/Daily-Scrum-tapaamiset

    - 1.4
        - LIKOlaisten kanssa olen viestejä vaihdellut ahkerasti Discordissa sekä dokumenttejen muodossa.
            - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/47/Liko
        - Olen esitellyt asiakkaallemme tuotteemme viikon välein.
            - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/86/Pekan-tuokiot

    - 1.5
        - Lähes tulkoon kaikki kommunikaatio, joka on dokumentaation arvoista on lisätty Azuren wikiin.
            - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/51/Hackathon-Wiki
        - Pienemmät asiat, kuten ohjauspalavereiden sopiminen ja ajankohdan tuonti ilmi ryhmälle, on tapahtunut Daily Scrumeissa, muistutuksina Discordissa ja wikiin aikatauluun lisättynä.

- 2.

    - Suurimman osan ajasta SBL on ollut käytössä, ainut poikkeus taisi olla 4. sprintti, jossa tehtäviä asioita oli listattu TODO- listaan repoon.

        - https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1?path=%2FAproto%2FreadME.md&_a=preview

    - SBL on parantunut jokaisen sprintin jälkeen ja nyt 5. SBL on ainakin omasta mielestäni 'hyvä'.

        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_sprints/taskboard/Ryhm%C3%A4%20A_1%20Team/Ryhm%C3%A4%20A_1/Sprint%205

    - 2.1
        - Lähes tulkoon jokaisen sprintin työt ovat löytyneet Azuren sprintti taululta.
    
    - 2.2
        - Mikäli tehtävän teko aloitetaan merkataan se 'in progress', jonka jälkeen tila muokataan 'to internal test', jossa toinen henkilö katsoo toisen henkilön tekemän työn ja samalla siirtää tilan 'testing', mikäli kaikki on kunnossa siirtyy tehtävä 'done', mikäli ongelmia ilmenee, niin menee tehtävä takaisin alkuun.

    - 2.3
        - Tuoteomistajana olen katsonut, mitä pitäisi tehdä sprintissä, jonka jälkeen käymme kehitystiimin kanssa läpi asioita, jotka otamme seuraavaan SBL.

- 3

    - Alussa tapaamiset olivat enemmän kokousmaisia, jotka kestivät noin 1h. Kyseisissä kokouksissa kävimme läpi myös ryhmän asiota, joten kyseessä oli enemmänkin ryhmän alustaminen, jotta työn tekeminen onnistuisi. Tämän jälkeen Scrum periaatteihin tutustuttuani muutimme Daily Scrumit oikeaoppisiksi. Pidin alussa ensimmäiset kaksi viikkoa daily scrummeja, kun en tiennyt paremmin. Tämän jälkeen vetovastuu siirtyi scrummasterille. Scrummasteri ei ole tehnyt dokumentaatiota daily scrumeista, tämä perustuu omaan havaintoihin wikissä (niitä ei siellä ole). Mainittakoon vielä, että tapahtumat olivat kirjoituspohjaisia alussa ja siitä johtuen päätin, että kyseiset ja kaikki muutkin pidetään äänitse, jolloin tapahtumat eivät kuluta aikaa ja asiat käydään nopeasti läpi.
        
        - https://gitlab.com/Maici/agile2019/blob/master/Kokous_Asialistat/7.11.md

    - 3.1
        - Jokainen jäsen on vähintään osallistunut 4/ 5 daily scrumiin viikossa, eli lähes aina on jokainen tullut paikalle. Osallistunut myös tuoteomistajana tapahtumiin.

    - 3.2
        - Heidän osalta, ketkä työskentelevät ryhmässä ovat tuonneet ongelmia/ esteitä/ huomioita esiin. Nämä asia ovat selvitetty samantien, mikäli ongelmat ovat olleet pieniä, isommat ongelmat selvitetään daily scrumin jälkeen.
            - https://gitlab.com/Maici/agile2019/tree/master/Kokous_Asialistat
            - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/10/Daily-Scrum-tapaamiset
        - Yllä löytyy omia muistiinpanoja/ valmisteluita tapahtumiin, kun vedin niitä.

- 4.
    - Demo on pidetty asiakkaalle aina, kun asiakas on ollut paikalla. 
        - https://gitlab.com/Maici/agile2019/tree/master/Asiakas_Asialistat
        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/86/Pekan-tuokiot

    - 4.1
        - Demossa tuodaan esiin uudet toiminnallisuudet/ ominaisuudet

    - 4.2
        - Olen antanut palautetta kehitystiimille Discordissa. (rakentavaa, kehuja, parannusehdotuksia), tosin asiat löytyvät Discordista general kanaalista, joten ainut tapa on käydä läpi viestihistoria (sama pätee muutamassa muussakin kohdassa).
        - Asiakkaan palaute tiivistettynä löytyy wikistä.
            - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/86/Pekan-tuokiot

- 5.
    - Alku päässä DoD tuli käytettyä, mutta siitä hieman lipsuttu ajan myötä.
        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_workitems/recentlycreated/

    - 5.1
        - Todennäköisesti ei, sillä kehitystiimin taidot olivat mitä olivat, näin ollen asioita jäi tekemättä. Tosin tämä on tuoteomistajan syy (minun), sillä kunnollisia sprintin suunnitteluita ei pidetty kurssin ensimmäisten sprinttien aikana.
    - 5.2
        - Tähän mennessä meillä on toimiva järjestelmä ja niissä tehtävissä, missä DoD on käytetty on olleet suurimmilta osin hyvin tehty.

    - Tämä kohta epäonnistui ryhmässämme.

- 6.
    - Kurssin alussa työntekeminen oli hieman kankeaa, joten kunnollisten retrospektien pitämiseen en nähnyt syytä, sillä ensimmäisen sprintin jälkeen suurinosa tehtävistä jäi tekemättä ja täten puutuin kehitystiimin työskentelyyn ja yritin saada sen toimimaan. Siedettävä työn tekeminen ryhmässä alkoi 3. sprintin loppuvaiheessa ja 4. sprinttiin muuta kehitystiimin jäsen moninkertaisti työn tekemisen.
    - Ongelma kohtien selvetessä minulle, katsoin touhua pari päivää, jonka jälkeen otin asian esiin daily scrumissa ja yritin kysellä, mistä ongelmat johtuivat ja miten asioita voisi parantaa. Näin ollen miniretrospektiivejä tuli pidettyä vähän väliin.'
    - Mikäli ryhmän kokoonpanoa saisi vaihtaa nyt ja kurssia olisi vielä useampi sprintti aikaa, olisi myös retrospektiivit parempia.
    - Kunnollinen retrospektiivi on pidetty 3. sprintistä ja se oli tuoteomistajan vetämä. Scrummasteri hoitaa 4. sprintin retrospektiivin.

    - 6.1
        - Osa ryhmästä toi asioita esille ja näin ollen pyrin yhdessä miettimään ratkaisua ongelmien korjaamiseksi. Alhaalta saattaa löytyy joitain asioita, mitä on tuotu esille ja joita olen pyrkinyt korjaamaan. Tosin osa asioista on hukkunut eri puolille dokumentteihin ja osa asioista on piiloutunut Discordiin. Dokumentaatiossa olisi huomattavasti voinut toimia paremmin.
            - https://gitlab.com/Maici/agile2019/tree/master/Kokous_Asialistat

    - 6.2
        - Dokumentaatio puuttuu lähestulkoon kokonaan ja retrospektiivejä ei sinänsä pidetty, joten tämä kohta epäonnistui ryhmässämme.
        - Ongelma kohtia olen hoitanut yhdessä ryhmämme kanssa, tuotti ne ratkaisuja ongelmiin tai ei, on se eri asia.

    - 6.3
        - 3. sprintin retrospektiiviin ja 'miniretrospektiiveihin' osallistuivat kaikki.

    - Tämä kohta epäonnistui ryhmässämme.

- 7.
    - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_backlogs/backlog/Ryhm%C3%A4%20A_1%20Team/Epics/?showParents=true

    - 7.1
        - Olen pyrkinyt viimeisten sprinttien ajan priorisoida tehtäviä asioita, joita asiakas on tuonut esille.
            - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/86/Pekan-tuokiot
            - https://gitlab.com/Maici/agile2019/tree/master/Sprinttien_Suunnittelut

    - 7.2
        - Asioita on tullut arvioitua ja perustuu osittain clockifyin merkintöjen mukaan sekä omien tuntemuksen mukaan osana kehitystiimiä ollessa.

![](https://i.imgur.com/svNkdMy.png)

- 
    - 7.3
        - Olen laittanut tehtäviin arvion, jonka jälkeen saatoin kerran mainita, että niitä voi muokata oman mielensä muokaisemmiksi. Epäonnistuin todennäköisesti tässä itse.

    - 7.4
        - Toiminnallisuuksia on pilkottu pieniksi osiksi, toisaalta sillä meidän ryhmässä ei välttämättä ole mitään väliä, sillä pelaamme huomattavaa alivoimaa.

    - 7.5
        - Oman rajoitetun ymmärryksen mukaisesti olen pyrkinyt priorisoimaan asioita, joita pitäisi tehdä projektimme eteenpäin viemiseksi, tosin asiat ovat olleet hieman hankalia, sillä olen joutunut miettimään samalla myös, kuinka asioita voisi toteuttaa ja pystyykö kehitystiimi tekemään niitä. Onneksi edes yksi jäsen löytyi kehitystiimistä, joka on painanut kovasti töitä 3. sprintin lopusta tänne asti. Ilman kyseistä henkilöä, meillä tuskin olisi mitään merkittävää valmiina.

    - Tämä kohta on siinä ja siinä onnistuiko se vai ei.

- 8.
    - https://gitlab.com/Maici/agile2019/tree/master/Sprinttien_Suunnittelut
    
    - Asiat ovat olleet hieman haasteellisia, sillä kurssin alussa tiedustelin kehitystiimin osaamista ja niiden lausuntojen perusteella ongelmia ei olisi pitänyt ilmetä, toisinpa kävi, kun aloitimme kehitystyön oli hommissa ainostaan minä ja yksi jäsen kehitystiimistä.

    - 8.1
        - Olen katsonut jokaiselle sprintille tehtäviä, jotka tulisi tehdä, että projektimme etenee. Olen osallitunut jokaiseen sprintin suunnitteluun.

    - 8.2
        - PBL on tullut päiviteltyä läpi kurssin, alussa asioita tuli tehtyä paljon, jonka jälkeen kävi pieni notkahdus, josta olen taas palautunut.
    
            - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_workitems/recentlycreated/

    - 8.3
        - Jokainen ryhmän jäsen on osallistunut kokouksiin

    - 8.4
        - Lähestulkoon jokaiselle sprintille meiltä on löytynyt SBL, lukuun ottamatta 3. sprinttiä, jolloin teimme asioita TODO- listalta.

    - 8.5
        - Alku sprintti suunnitelmien tekemisessä ei ollut mukana muut, sillä en tiennyt paremmin silloin. 5. sprintin suunnittelussa osallistui kaikki jäsenet, tosin tapaaminen meni osittain kaksinpuheluksi, sillä ratkaisevien toiminnallisuuksien tekemiseen löytyy vain kehitystiimistä yksi jäsen.

    - 8.6
        - Olen tyytyväinen tällä hetkellä, siihen mitä 'pystymme' tekemään. Omiin asioihin olen pettynyt ja ryhmän asioihin en ota edes kantaa.

- 9.
    - Jokainen iteraatio on ollut saman mittainen ja alkanut sekä loppunut samoina päivinä.

    - 9.1
        - Olemme pitäneet 1. viikon mittaisia sprinttejä.
    
    - 9.2
        - Kyllä ovat, tehtävien onnistuminen ei.
    
    - 9.3
        - Sinänsä olen joutunut puuttumaan kehitystiimin toimiin, sillä useammalla näytti alussa olevan ongelmia työn tekemisessä. Tämän jälkeen suurin osa ryhmästä paransi asiaa, mutta yksi jäsen ainakin omiin havaintoihin perustuen ei ole tehnyt mitään. Ensimmäisen tehtävän tekemiseen meni se ~4 viikkoa ja sekin oli 5 minuutin homma...

- Kommitit
    - Commiter { _CommiterName: 'Mika Marjomaa', _CommitsCount: 89 },
    - Commiter { _CommiterName: 'JaniHarkonen', _CommitsCount: 50 },
    - Commiter { _CommiterName: 'Valeria Vasylchenko', _CommitsCount: 23 },
    - Commiter { _CommiterName: 'Antti Salo', _CommitsCount: 1 },
    - Commiter { _CommiterName: 'Lauri Hiltunen', _CommitsCount: 82 },
    - Commiter { _CommiterName: 'Riku Härkönen', _CommitsCount: 48 }

    - 9.4
        - Sinänsä sprintin suunnittelut ovat hoituneet omasta takaa, jolloin kehitystiimillä ei ollut niinkään sanomista, tosin toin aina sprintin suunnitelman esille Daily Scrumissa ja niissä ei ongelma kohtia havaittu, käytännössä suostuivat, mutta loppu pelissä olen syyllinen tähän.
        - Pääsääntöisesti on saatu asioita vietyä eteenpäin sprintien aikana.
    
    - Tämä kohta on siinä ja siinä onnistuiko se vai ei.

- 10.
    
    - He ketkä ovat työskennelleet kurssin ajan ovat aktiivisesti olleet mukana työnteossa ja kokouksissa.
        
    - 10.1
        - Omien laskujen mukaan ryhmässämme oli 1 tuote omistaja, 1 scrum masteri ja 3 kehitystiimin jäsentä. 5/ 7 ryhmän alkuperäisen kokoonpanon mukaan.


- 11. 
    - Siinä ja siinä. Tehtävän anto olisi voinut olla toisenlainen, mikäli tämän kurssin tarkoitus oli **ainoastaan** opiskella scrumin periaatteita.

- 12.
    - Kaikki ketkä ovat osallistuneet työntekemiseen aktiivisesti, niin ovat tehneet eri asioita.

- 13.
    - Koko ryhmän työskentely olisi pitänyt lopettaa aikoja sitten.

- 14.
    - Kyllä on.
        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/44/T%C3%A4m%C3%A4n-hetkinen-suunnitelma
        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_backlogs/backlog/Ryhm%C3%A4%20A_1%20Team/Epics/?showParents=true
        - 
- 15.
    - Kaikki asiat löytyvät Azuresta

- 16.
    - Yksi jäsen ei ole tehnyt mitään niistä asioista, joista sovimme kurssin alussa. clockify, töiden tekeminen, repon käyttäminen.

- 17.
    - Olen ollut tavoitettavissa jokaisena päivänä.

- 18.
    - Taitaa olla ajan perusteella määritelty tehtävät.

- 19.

- 20.
    - kyllä on.
    
    - 20.1
        - kyllä työskenteli aika ajoin

- 21.
    - SBL on tullut tehtyä
    
    - 21.1
        - tehtävät ovat arvioitu ajan mukaan
    - 21.2

- 22.

- 24.

    - Daily scrumit on pidetty, joka päivä. Käytännössä ainut asia ryhmässä, joka onnistui hyvin.

    - 24.1
        - Olen osallistunut jokaiseen, paitsi yhteen
    - 24.2 
        - 5- 15 minuuttia kestänyt
    - 24.3
        - Kyllä ja samalla mitä muut eivät tee.





## Sprintti 1 (5.11.-11.11.)

### Aikataulun seuranta:
#### https://clockify.me/bookmarks/5dc96ccc6afcf51b0128b689


### Työpanokseni ja sitoutumiseni projektiin

Vaikeaa näyttää osalle ryhmän jäsenille työnteko olevan, katsoo nyt, kuinka pitkään tätä jaksaa katsoa, ennen kuin hommien teko itsellänikin loppuu kokonaan.

Sinänsä mielenkiinnolla olen mukana tällä kurssilla, sillä haluasin nähdä sen, miten sovelluksia tuotetaan ryhmässä. Mikäli ryhmän jäsenet ovat saman henkisiä, eli tahto viedä projektia eteenpäin omia taitojaan ja soveltamista hyväksikäyttäen kehitystyössä, olisi se paras vaihtoehto kuulua kyseiseen ryhmään. Näin olisin osa suurempaa kokonaisuutta, joka tuottaa toimivan ja hyödyllisen sovelluksen maailmalle. Vielä kyseistä kokemusta en ole kokenut opintojen aikana, vaikka "ryhmätöitä" on tullut tehtyä.

Aikaisemmasta ryhmätyöstä osasin odottaa jo, mitä tuleman pitää ja odotukset osoittuivat lähestulkoon toteen, mikä kurssin alussa lähestulkoon tappoi kokonaan kiinnostukseni kurssia kohtaan. Joten kului kaksi viikkoa tekemättä oikeastaan mitään. Tämän jälkeen haettuani "lisämoraalia" aloin kokeilemaan kepillä jäätä, mitä projektin aikana mahdollisesti voitaisiin tehdä ja kuka olisi "valmis" osallistumaan projektin viemiseen eteenpäin. Tällä hetkellä minulla on aika hyvä käsitys siitä, kuka "jättää" tekemättä tehtäviä, joka sitten kostautuu toisille lisätehtävien valossa. Tosin voihan asiat muuttua siinä, kun pääsemme kehittämään sovellusta.

Projekttipäälikkönä olen kartoittanut jäsenten taitoja ja millä tavalla he suhtautuvat kurssiin. Osa ryhmästä tekee tehtäviä ja näin ollen ovat oikeasti mukana tekemässä asioita, joka sinällänsä on plussaa. Paljoa tehtävää ei ole vielä ollut ja sen seurauksena tehtävien tekeminen on jakautunut huomattavasti toisten harteille, tosin en voi kiistää, etteikö osa ryhmän jäsenistä olisi vältelleet tehtävien tekoa. Ensimmäisten tehtävien osalta, tuli niitä jaettua ryhmän kesken, jonka jälkeen seuraavia tehtäviä osa alkoi jo tekemään itsenäisesti (ei tarvinnut määrätä tekemään). Sprintti 2:seen pitää keksiä jonkin näköinen systeemi, jos sama homma jatkuu.

Omasta tekemisestäni ei ensimmäisten kahden päivän jälkeen tullut mitään kahteen viikkoon. Tämän jälkeen hyväksyin asiat, kuinka ne ovat ja aloin johtamaan ryhmää, kuten sitä pystyy/ pitää. Itse olen tehnyt ensimmäisen sprintin ajan ryhmämme johtamiseen liittyviä asioita, jotka olen yrittänyt tehdä parhaimmalla tavalla, tosin aina on parannettavaa ja testailemalla asioita pystyn saamaan parannusideoita, kuinka tehdä asiat seuraavalla kerralla paremmin. Esimerkkinä päivittäiset tapaamiset, jossa kommunikoimme kirjoittamalla, olivat kyseiset tapaamiset pitkähköjä ja näin ollen aikaa kului hukkaan. Näin ollen aloimme maanantaista 11.11. lähtien pitämään tapaamiset puheyhteyksen välityksellä. Ensimmäisen tapaamisen jälkeen oli havaittavissa huomattava tehokkuuden lisääntyminen ja muutenkin asiat kävimme läpi nopeammin.

Tehtäviä suunnitellessa olen yrittänyt pyrkiä priorisoimaan tärkeitä tehtäviä, jotka tulisi tehdä ensin. Sinällänsä olemme vieläkin suunnitteluvaiheessa ja näin ollen tehtäviä ei voi taikoa ilmasta, tosin vaikka meillä olisikin ryhmässä lisää suunnittelutehtäviä, olisi ne todennäköisesti tehty suppeasti ja vain vastattu apukysymyksiin, joita tehtävänannossa on tuotu esille, joten suunnittelussa tultaisiin periaatteessa tekemään kahteen kertaan suunnittelu. Todennäköisesti projekti olisi vielä lähtöviivoilla, mikäli en olisi ilmoittautunut PO:ksi.

Muiden ryhmien tekemisiä tulee seurattua aina, kun kokoonnumme kurssin tunneilla. Sinällänsä paljoa ei toisten ryhmien tekemisistä saa otetta, mitä he ovat tekemässä, joten toisten ryhmien tekemiset eivät ole olleet muokkaamassa meidän ryhmämme toimitapoja.




### Oman työn seuranta ja kehittäminen

Käytämme clokckifyitä porukassa ryhmässä, eli jokainen kuuluu samaan tiimiin, josta näemme omat, sekä koko ryhmän käyttämän ajan. Clockifyissa onnistuu helposti rajaamaan työajat aikaväleille ja näin ollen olen tutkinut, kuinka ryhmämme käyttää aikaa ja kuinka paljon.

Ensimmäisen sprint aikana olemme edenneet hitaasti eteenpäin ja näin ollen kävin katsomassa clokifyitä ja syy selvisi siinä, kun ryhmämme ensimmäisen sprintin ajankäyttö oli noin. 55h, joka on noin kolmas siitä mihin pitäisi pyrkiä. Näin ollen toin asian ryhmälle esiin ja pyrimme lisäämään ajankäyttö projektin eteenpäin viemiseksi. Samalla tuli katsoattua, mikä olisi mahdollinen palkkamme työstä, mitä olle tehneet. Muutaman laskutoimituksen jälkeen kävi ilmi, jos olisimme oikea yritys, niin joutuisimme luopumaan muutamasta työntekijästä.

Toisessa sprintissä tullaan hyödyntämään ensimmäisen sprintin tehtävien clockify aikoja, jota käytetään toisen sprintin tehtävien ajan arvionnissa.




### Reflektio- ja itseohjautuvuusosaaminen


Projektipäälikkönä olen kartoittanut ryhmän jäsenien taidot ja kiinnostuksen kurssia kohtaan. Nämä asiat olen ottanut huomioon työtehtäviä miettiessäni. Olen pyrkinyt kannustamaan tuomaan ongelmat esiin ja niiden selvittäminen samantien. Olen pyrkinyt kannustamaan ryhmän jäseniä tekemään tehtäviä, osan kanssa ei ole ongelmaa ja taas osan kanssa hieman ongelmia, joihin koitin etsiä syytä heidän kanssaan, mutta kyseessä oli kiinnostuksen puute, johon sinänsä en voi vaikuttaa.

Olen pyrkinyt informoimaan ryhmämme jäseniä, sekä liiketalouden opiskelijoita. Olen tuonut esille ryhmässä sen, että ongelmien tuonti esiin olisi tärkeää, jotta ongelma kohdat voitaisiin korjata ryhmämme töiden teossa. Isoja ongelmia ei ole vielä ollut, sen sijaan pienempiä ongelmia ja väärinymmärryksiä on osa ryhmämme jäsenistä tuoneet ilmi, jotka olemme pystyneet nopeasti käymään läpi ja ratkaisemaan. Sen sijaan osa ryhmämme jäsenistä ovat jättäneet asiat tekemättä ja näin ollen kirjoittaneet "ei ole ongelmaa" silloin, kun vielä käytimme keskustelussa kirjottamischattiä. Nyt äänichatissä olisi mahdollisesti "vaikeampaa" olla tekemättä tehtäviä. Muutaman päivän tulen seurailemaan tilannetta kehitystyön edetessä ja mikäli sama homma jatkuu, tulen keksimään jotain asian korjaamiseksi.

Lähestulkoon kaikki asiat, joita teen/ olen tehnyt olisi voinut tehdä paremmin. Sinällänsä, kun asioita olen tehnyt, olen pyrkinyt samalla aikaa miettimään ongelmia ja parannusehdotuksia, kuinka parantaa ensi kerralle tekemisiäni, kuten esimerkiksi palavereiden suunnittelussa, teen asialistan, jonka käymme läpi porukassa. Tämä asialista on parantunut joka kerta ja näin ollen parantanut kokouksien kulkua huomattavasti.

Vastuunottaminen omista tekemisistäni on tärkeää minulle, sillä pystyn perustelemaan mielipiteeni ja pyrin aina tekemään omat asiani ennen määräaikaa. Mikäli olen tehnyt jotain väärin/ huolimattomasti tai muuta, olen valmis ottamaan palautetta ja korjaamaan virheeni. Olen pyrkinyt tuomaan ryhmäämme yhtenäiseksi kokonaisuudeksi, joka sinällänsä on vielä kesken. 



### Omien työtehtävien hallinta


Olen koittanut tehdä selkeitä työtehtäviä, joissa käy selville mitä pitää tehdä ja mitä tarvitaan. Nämä pitävät sisällään apukysymyksiä ja mahdollisesti esimerkkejä, mikäli koen tehtävän haasteelliseksi. Tehtävien vastaukset ovat käytännössä olleet tähän mennessä vain suoriavastauksia apukysymyksiin, jolloin tehtävän tekijä ei välttämättä itse ideoi tai mieti, mitä hän on tekemässä. Tähän yritän miettiä ratkaisua, kuinka voisin saada ryhmämme jäsenistä enemmän irti (ideointi ja oman osaamisen soveltaminen). Sinällänsä tehtävänä hankala, mutta odotukset ovat korkealla, joten luulen, että tekemellä asioita tulee mahdolliset ongelma kohdat esille, joita en toista seuraavalla kerralla.

Sinällänsä, jos tekisin tehtävistä muutaman sanan mittaisia, voi vastaukset olla jopa lyhyempiä kuin, mitä ne tällä hetkellä ovat. Tosin ongelma kohdaksi tämmöisessä kohdassa todennäköisesti tulisi se, ettei tehtävän tekijä välttämättä edes ymmärrä tehtävänantoa ja täten hukkaisi resursseja molemmilta sekä minulta että työntekijältä, kun käymme läpi aina mitä tehtävässä haetaan. Toisaalta, jos teen taas tehtävänannon todella tarkasti ja käyn läpi kaikki, mitä tarvitsemme tehtävänantoon läpi ja näin ollen teemme tehtävät periaatteessa kahteen kertaan, jossa ei myöskään ole järkeä. Pyrin joka sprintin jälkeen pyrkiä parantamaan ryhmämme toimintaa, kuinka toimimme jatkossa. Tämän hetkiset tehtävät ovat olleet suunnittelua, jolloin koodaamistehtävät tulevat olemaan erilaisia ja täten mahdollisesti avaamaan ryhmän jäsenien lähestymistapaa tehtäviin.


### Oma viestintä


Kauheasti ei ole ennen tullut puhuttua randomejen kanssa tai ylipäätänsä puhuttua toisille siitä, mitä olen tekemässä. Lähestulkoon tuli uutena asiana johtaminen tällä kurssilla, joka ainakin omasta mielestäni on mennyt kohtalaisen hyvin. Tietenkin alussa tuli käytyä pohjalla, mutta osa ryhmän halusta oikeasti tehdä jotain sai minuun luotua uutta uskoa, että tulemme selviytymään projektista. 

Alussa "vallan" käyttö muihin, kuten tehtävänannoissa ja muihin asioihin, jotka kuuluvat ryhmälle, tuntui oudolta ja näin ollen olen kommunikoinut "pehmeästi", en vielä ole määräillyt, "kuinka teemme asioita" ja "teemme näin kun käsken", josta olen "ylpeä", tosin en voi kieltää, etteikö mielessä ole käynyt tiukentaa lähestymistapaa niiden jäsenten kohdalla, jotka eivät ole vielä oikeastaan tehneet mitään.

Jokaisessa palaverissa olen muistuttanut ryhmällemme, että ongelmien tuonti esille aina, vaikka kuinkakin pieni ongelma olisi kyseessä, jonka ratkaisemme sitten yhdessä. Osa tuo ongelmat esiin, joka on erinomainen juttu ja pääsemme etenemään projektissa. Sen sijaan taas osalta niitä pitää metsästää, joka alkaa jossain vaiheessa tuntumaan, miksi edes yritämme tehdä mitään.

Pyrin aina kysymään ryhmältä mielipiteitä ja olen yrittänyt tuoda esille, että palautetta voi antaa minulle, oli se sitten nag tai pos. Kun aloimme käyttämään äänichattiä, kommunikointimme on nopeampaa ja täten aika, joka vastauksen kirjottamiseen kuluu on postettu, joten käymme tehokkaammin asiat läpi ja kysymykset ovat saman tien saatavilla vastattavaksi.

Viestinnässä aina on parannettavaa ja näin ollen tarkkailen omia toimia aina, kun pidämme palaverin tai viestitän LIKOn opiskelijoille mitä teemme.






## Sprintti 2 (12.11.-18.11.)

### Aikataulun seuranta:
#### https://clockify.me/bookmarks/5dd45c029fbfdb5c528abd05



### Työpanokseni ja sitoutumiseni projektiin

Aloimme tekemään jo pientä kehitystyötä tämän sprintin loppuvaiheessa. Yllätyin positiivisesti siitä, kun toisen ryhmän jäsenen kanssa saimme jo toimivaa prototyyppiä pohjapiirrutsuksien piirtämisestä aikaiseksi. Sama homma, jos jatkuu ja mahdollisesti muutkin ryhmän jäsenet osallistuvat kehitystyöhön voimme joka oikeasti toteuttaa jotain tällä kurssilla. Tällä hetkellä olen ryhmän tekemisissä mukana lähestulkoon täydellä painolla.

Mikäli olen ymmärtänyt oikein, niin osaamista ryhmästä löytyy, joten odotan mielenkiinnolla sitä, mitä tästä kehitystyöstä tulee, lähinnä Sprintti kolmosen osalta. Tietenkin, jos ryhmästä löytyy vapaamatkustajia, niin oma kiinnostukseni saattaa laskea, tosin mikä kannustaa tekemään, on osan ryhmän jäsenten antama panos suunnittelutehtävien tekemisessä ja itse kehitystyön teko porukassa, jota olemme saaneet jo vietyä hieman eteenpäin.

Olen jo tuonut esille ryhmälle, että mikäli heiltä löytyy teknillisiä toteutusideoita, niin siinä tapauksessa voisivat tuoda niitä esille ja mahdollisesti alkaa toteuttamaan järjestelmää. Tällä hetkellä, niitä ei ole tullut, tosin ei siitä ole kulunut kuin vasta muutama päivä. Yksi mahdollisuus on se, että tekisin nopean demon, jonka ympärille lähtisimme yhdessä kehittämään toiminnallisuutta. Tämä saattaa olla lähestymistapa, jota tulemme käyttämään järjestelmän teossa. Entuudestaan minulla on ideoita, kuinka järjestelmän voisi toteuttaa ja osaan jo hieman varoa ansoja, joita kehitystyössä ilmeni viime kerralla. Joten huomattavasti tarkempi tällä kertaan ja puutun samantien omaan tekemiseeni, mikäli toteutus alkaa näyttämään huonolta. Tähän myös toivon ja kannustan muita ryhmän jäseniä, että he toisivat huolet ja kehitysideat kaikille esiin, jonka jälkeen voisimme etsiä parhaimman toteutustavan eri tilanteissa.

Tähän mennessä itsestäni on tuntunut siltä, että kun olen saanut esimerkin tehtyä uusista asioista, niin sen jälkeen muutkin alkavat tekemään niitä. Toki suunnitellu vaiheessa löytyi myös muiden ryhmän jäsenten itsealoitteisuutta, kuten mm. käyttöliittymän prototeosta, tietokannan suunnittelusta ja näin pois päin. Miten tämä sitten kehitystyöhön vaikuttaa ja minkälainen alustajan rooli työnteossa minulle tulee on sitten eri asia. Lähtökohtaisesti teen erilaisia demoja toiminnallisuuksista, joista sitten toivon saavani palautetta muilta ja mikäli hyvin käy, niin joku ryhmästä voisi alkaa itsenäisesti tekemään toiminnallisuutta järjestelmään. Tämä selvinee, sitten seuraavan sprintin aikana.

Kehitystyössä työtuntejen jakaminen tasaisesti ryhmän jäsenten kesken todennäköisesti tulee tuottamaan ongelmia, etenkin silloin, jos osa ryhmästä pyrkii pääsemään mahdollisimman vähällä. Daily Scrummeissa kuuntelee kunkin jäsenen tekemiä asioita, joista pystyy vetämään johtopäätöksiä tehdyn työn määrästä. Tästä sitten voi mahdollisesti alkaa miettimään erilaisia toimenpiteitä sille, jos osalle ryhmän jäsenistä kasaantuu kaikki työt ja osa ryhmästä käy osallistumassa kokouksiin. Tietenkin tähän voi myös vaikuttaa ryhmän jäsenten taidot, mutta useamman kerran olen kysellyt ryhmältä mitä he osaavat tehdä, niin tämän ei pitäisi olla osa tekijä. Mutta loppujen lopuksi tämä asia tulee elämään kurssin aikana. Mikäli selvästi työn tekeminen alkaa painamaan vain muutamalle jäsenelle, niin tulen ottamaan asian esille Daily Scrummeissa ja mahdollisesti kyseisten henkilöiden kanssa, mikäli työn tekeminen alkaa olla työtehtävien välttelemistä viimeiseen asti.

Paljoa kokemusta ryhmätöistä minulla ei ole, mutta se ymmärrys, minkä olen kerännyt työskennellessä erilaisissa ryhmissä on jo tuotu tämän ryhmän tapoihin. Käyn myös läpi tälläkin hetkellä, missä olemme onnistuneet ja missä epäonnistuneet. Näitä tietoja käytän ryhmän asioiden eteenpäin viemisessä. Sinänsä muiden kurssin ryhmien työskentelystä tulee informaatio niukasti, oikeastaan se mitä käymme aina yhteisillä tunneilla läpi. Roolikokous sinänsä olisi hyvä idea, jossa voisin vaihtaa tietoa muiden ryhmien PO:den kanssa. Näissäkin tilaisuuksissa saattaisi jopa syntyä uusia ideoita, kun kaksi näkökulmaa törmää.



### Oman työn seuranta ja kehittäminen

Clockifyissa aloin ja toin esille ryhmän tietoon tarkemman ajan hallinnan, toki voi olla, että olin viimeinen kuka tätä toimintatapaa alkoi käyttämään, sillä ainakin muutama ryhmän jäsen jo käyttikin kyseistä merkkaustapaa. Eli käytännössä task ID käyttö Azuresta merkitään clockifyihin tehtävän nimeen.

Työtunteja on tullut käytyä läpi ja niiden kannalta mitä olemme saaneet aikaan, niin joutuisimme todennäköisesti poistamaan henkilökuntaa "yrityksestämme". Toisaalta osalla ryhmän jäsenellä näyttäisi tämä "työ" olevan enemmänkin freelancerimaista toimintaa, eli työtunteja tulee niukasti viikoittain. Toki on mahdollista, että yrityksessämme on kovat tuntipalkat, jolloin tuntejakaan ei tarvitsesi tehdä niinkään montaa viikossa. Lyhyesti sanottuna, tuntimäärä on ollut niukkaa ja ryhmän koon perusteella ainakin omasta mielestäni toteutuksemme olisi pitänyt olla valmis jo ainakin jossain määrin.

Kehitystyön alkaessa nyt, olen hieman yrittänyt miettiä työpanoksen määrää eri tehtäville ja jossain määrin todennäköisesti onnistunut osassa niissä. Mutta tämä todennäköisesti tarkentuu seuraavilla sprinteillä, kun alamme lisäämään toiminnallisuutta jo tehdyn koodin sekaan.


### Reflektio- ja itseohjautuvuusosaaminen


Sinänsä olisi mukavaa jos saisimme tehtyä toimivan järjestelmän, jolla edes jotain voisi tehdä. Pyrin kannustamaan toisia ja ottamaan kaikkien näkemykset eri tehtävistä mitä teemme ryhmän sisällä huomioon ja tekemään muutokset toimintaamme. Olen avoin muiden ehdotuksille ja tälläkin kurssilla ryhmä on tuonut esille erilaisia ajatuksia omista jo tehdyistä asioista poiketen ja nopeasti olen pyrkinyt muokkaamaan asioita parempaan suuntaan. Ideana itselläni PO:n ei ole johtaa ryhmää tekemään juuri niin kuin minä sanon ja näin ollen muiden mielipiteillä ei ole merkitystä. Olen jatkuvasti kysellyt muiden mielipiteitä, joita tulee vähän väliin ja eritysesti niistä olen mielissäni, jotka tulevat kysymättä. Tarkoitukseni on oppia muilta ja näin ollen tehdä ryhmämme työskentelystä mahdollisimman tehokasta ja tuottavaa. Tietenkin asioihin joihin en saa muilta mielipiteitä, joudumme ne tekemään minun mallin mukaan, mutta näihin on puututtu/ annettu parannusehdotuksia, jolloin muokkaamme toimintaamme ryhmänä.

Otan vastuun omista tekemisistäni täysin, en yritä luikerrella vastuistani pois. Johdan ryhmää ja teen ne asiat, jotka minulta vaaditaan, kuten esim. viestintä eri sidosryhmien välillä, asioiden ilmoittaminen ryhmän keskuudessa, parannusehdotuksien kyseleminen vähän väliin ja muutoksien teko toimintaamme. Mikäli epäonnistun asissa tuon sen esiin muille ja pyrin parempaan jatkuvalla itseni sekä ryhmän toiminnan analysoimisella. Lähtökohtaisesti ideana olisi tehdä asiat siinä mielessä, että nämä opitaan ja näin ollen voisin niitä hyödyntää työelämässä. Tähän auttaa huomattavasti se, että olen opiskelemassa alaa, joka oikeasti kiinnostaa minua.

Scrum menetelmä oli uusi itselleni ja olen pyrkinyt koko ajan etsimään internet.com välityksellä hyviä tapoja, miten scrumin tulisi toimia ja mikäli sisäistän asioita, niin tuon ne ryhmämme työskentelyyn mukaan. Jatkuvasti omassa toiminnassa on parannettavaa, joten pyrin aina etsimään omasta toiminnasta epäkohtia, joita oyrin parantamaan jatkossa. Ryhmän johtaminen onnistuu, mutta siinäkin on vielä paljon hiomista. Esim. olen laatinut kokouksiin asialistoja, joiden mukaan pidämme kokouksia ryhmämme sisällä. Valmistaudun kokouksiin huolella ja pyrin pitämään asiat mahdollisimman selkeänä. Ongelmia esityksissä tuppaa tuottamaan hengittämisen unohtuminen ja itseni asettaminen myös kuuntelijan rooliin esityksen aikana, jolloin ongelmia syntyy helposti, jotka itse tiedostan jokaisen kokouksen jälkeen. Tähän pitää jonkinlainen ratkaisu jossain vaiheessa keksiä.

Kyselen vähän väliin muiden mielipiteitä päättämistäni asioita ja toimintamalleista, jota pidän jonkinlaisena vahvuutena, sillä välillä saan aina palautetta, jonka pystyn ottamaan vastaan ilman minkäänlaisia ongelmia, oli se sitten negatiivista tai positiivista. Palautteen saannissa saatan haastaa palautteen antajan, mikäli palaute ei ole tarpeeksi yksityiskohtainen ja mahdollisesti pitää sisällään aukkoja. Näin ollen näistä tilanteista saattaa syntyä uusia ideoita. Tietenkin mahdollinen ongelma tässä saattaa olla se, jo palaute/ idea on reikiä täynnä, mutta hyvä, niin saataan kysymyksilläni "pelottaa" palautteen antajan, jolloin saatan joutua itse punnitsemaan asiaa. Lupaamani asiat tulee tehtyä ennen deadlinea 99% ajasta, oli se sitten hyvin ajoissa tai sitten viimeisenä yönä, mutta pyrin täsmällisyyteen, joka myös sama on sovittujen tapaamisien alkamisajankohdissa, mikäli tapaaminen alkaa 9:00, niin silloin olen sopimassani paikassa ajallaan, yleensä hyvissä ajoin.



### Omien työtehtävien hallinta

Pidän huolta ryhmämme asioita, että ne tulee tehtyä. Ohjauspalaverien varaaminen, asiakastapaamisien suunnittelu ja sen vetäminen, viestintä sidosryhmien kesken, Daily scrumeihin osallistuminen, sprinttien suunnitteluun osallistuminen, sprinttien retrospektiiveihin osallistuminen.

Järjestelmän idea on selkeä, mutta esim. asiakkaan kanssa kommunikointi voisi olla tarkempaa. Product backlogin ylläpidossa vielä huomattavasti harjoittelemista, sinänsä jotenkin se onnistuu, mutta voisi olla paremminkin ylläpidetty.

- Pyrin antamaan tarkennusta aina mikäli kehitystiimiltä semmoista tulee.
- Pyrin parantamaan ryhmämme toimintamenetelmiä kokoajan
- Kuuntelen muita ja kerään palautetta
- Osallistun kokouksiin (Daily scrum, sprintin suunnittelu/ retrospekti)
- Pekan pitäminen ajantasalla tekemisistämme
- Jonkinlainen product backlogin hallinta (parannettavaa löytyy)
- Sprinttien maalit tiedossa, jotka tuon esiin kehitystiimille
- Pientä ideointia tekemästämme asiasta

Osallistun myös kehitystiimin työskentelyyn "eri henkilönä".


### Oma viestintä


Pidän kiinni lupaamistani asioista, jolloin odotan näin myös muidenkin toimivan. Pidän yllä viestintää ryhmämme sisällä, LIKOn kanssa ja Pekan/ opettajien kanssa. Viestit kulkevat niille henkilöille, ketä asiat koskevat. Olen mukana Daily Scrumeissa, sprintin suunnittelussa, sprintin retrospektiivissä, ohjauspalavereissa, asiakkaan tapaamisissa ja mahdollisesti muissa ryhmäämme koskevissa kokouksissa. Pyrin saamaan mahdollisimman paljon tietoa, jonka sitten jaan ryhmämme sisälle heille keitä asia koskee. Suurin osa ajata tulee vietettyä koneella ja discordi on avoinna suurimman osan ajasta yhdellä näytöllä, josta pystyn lähestulkoon saman tien antamaan vastauksen kysymyksiin, joita ryhmämme jäsenet laittavat.

Jaamme LIKOn kanssa tilanneraportteja viikoittain tekemistämme asioista toisillemme, näin ollen molemmat osapuolet pysyvät kartalla siitä mitä toinen tekee. Pyrin viemään kaiken viestinnän, jonka koen tarpeelliseksi taltioida Azuren wikiin.

Oma viestintäni onnistuu tällä hetkellä hyvin, tosin varmasti parannettavaakin löytyy jatkossa. Mutta lyhyesti, kuuntelen muiden palautetta mielelläni, oli se sitten negatiivista tai positiivista. Otan ylös ideoita, joita käymme läpi mahdollisesti siinä hetkessä tai myöhemmin. Viestintä menee oikeille henkilöille ryhmässämme. Pyrin vastaamaan kysymyksiin mahdollisimman nopeasti, enkä jätä kysymyksiä/ ideoita roikkumaan turhaan. Olen tietoinen suurimmalta osin asioista, mitä ryhmämme Discordista löytyy (keskustelut/ ideat/ muut asiat).






## Sprintti 3 (19.11.-25.11.)
#### Aikataulun seuranta:
#### https://clockify.me/bookmarks/5ddbf83ad1f7b05531e0018b

### Työpanokseni ja sitoutumiseni projektiin

Voimakkaasti työnjako on, etenkin itse koodaamisen suhteen, painottunut vain muutamalle henkilölle. Käytännössä kaksi henkilöä tekee toiminnallisuutta järjestelmään, jonka jälkeen yksi henkilö korjailee yleisesti toisten tekemisiä ja samalla tekee pieniä muutoksia, kuten kommentointia/ tyylitystä/ oikeinkirjoitusta. Yksi henkilö on tehnyt tietokantaa ja pitää sen ajan tasalla, eli muokkaa tietokanna rakennetta, kun sille ilmenee tarvetta. Yhden henkilön tekemisistä en tiädä itse kehitystyön ohelta juurikaan mitään, tosin hän on tehnyt töitä suunnittelu vaiheessa paljon. Ja viimeiseksi löytyy tämä henkilö, joka käytännössä ei tee mitään, mikäli hän tekee jotain on se viisi minuuttia maksimissaan päivässä.

Näin ollen oma kiinnostukseni kurssia kohtaan alkaa olemaan hyvin lopussa, sillä mikäli en olisi "puuttunut" kehitystyöhön lainkaan, olisi todennäköisesti vain yksi henkilö ollut toteuttamassa käyttöliittymää ja näin ollen palvelin puoli olisi mahdollisesti jäänyt kokonaan tekemättä. Sinänsä kurssin asia kiinnostaa, mutta ryhmän kokoonpano hieman vie mieltä matalaksi. Omista koodin tuotoksistakin olisi voinut tehdä paljon parempia ensimmäisellä kerralla, tosin järjestelmässä on jo toimintalogiikka hyvin pitkälti tehty, jolloin sen uudelleen kirjoittaminen ja koodin paranteleminen olisi ainoat tehtävät viimeisillä sprinteillä kurssilla.

Toki voi olla, että ryhmän "huono/ hidas" työskentely on kokonaan/ osittain minun syytäni, tosin odotin kurssin alussa, että ryhmäni olisi ollut täynnä saman henkisiä jäseniä, joilla olisi itsealoitteisuutta ja taitoa oppia itsekseen asioita ja näin ollen olisimme todennäköisesti jo saaneet järjestelemän valmiiksi. Oli oikea syy mikä tahansa, otan kumminkin täyden vastuun ryhmämme toimimisesta. Kuten ensimmäisenkin vuoden ryhmätyöstä kävi ilmi, että siellä sai tehdä aikalailla kaiken yksin/ muutaman henkilön kanssa, niin sama tapahtui tälläkin kurssilla. Kun jo kyseessä on toinen vuosi näin ollen, olettaisin kaikkien jo olevan perillä, kuinka koodata/ oppia itsekseen asioita/ taito käyttää googlea, kun asia ei näin olekkaan, niin seuraavan kerran ryhmässä työskentely itselläni tapahtuu työelämässä tai sitten mahdollisesti randomien kanssa hajautetusti aivoimen lähdekoodin toteuttamista tai muuta vastaava. Tosin pitää vielä selvittää oliko omassa ryhmän "johtamisessa" ongelmia, joka tämän työskentelyn mahdollisesti aiheutti.

Olen tuonut työntekemisen ryhmässämme useamman kerran esille, toisilla ei ole ollut ollenkaan ongelmaa työn tekemisessä, joten heitä asiat eivät sinänsä koske, mutta ryhmässä on henkilö, jonka työpanos ryhmässämme on hyvin pieni, ellei kokonaan olematon. Kun taitotaso on hyvin laajasti jakautunut ryhmässämme, en ole enää kiinnostunut ryhmämme tekemisistä juuri ollenkaan, lähinnä koskien työn jakamista tasapuolisesti ryhmämme jäsenten kesken. Minun puolesta voi ryhmämme jäsenet tehdä mitä haluavat milloin haluvat ja keskityn itse työskentelemään niiden kanssa, jotka tuottavat arvoa ryhmällemme.

Muutama viikko kurssia jäljellä, niin en näe järkeä enää edes yrittää saada kaikkia työskentelemään ja alunperinkin olin sitä mieltä, ettei tämä edes ole minun asiani, sillä oletin kaikkien haluavan oppia lisää ja näin ollen osallistumalla aktiivisesti ryhmässä työskentelyyn. Sinänsä olen sisäistänyt kurssin asiat, joita pystyn tulevaisuudessa itse soveltamaan/ käyttämään, mm. ajatuksena tänä vuonna oli alkaa tekemään F1 peliin telemetri sovellusta, mutta ainakin ensimmäiset kurssit tänä vuonna veivät kaiken aikani, sillä ne olivat mielenkiintoisia ja opin paljon uutta. Näin ollen kaikki aikana kuluikin unreal enginen parissa ja blenderillä tehdessä 3D-malleja. Mutta, kunhan itseltäni löytyy aikaa, niin toteutan kyseisen sovelluksen varmasti käyttäen tämän kurssin asioita. Sillä asioiden organisointi auttaa huomattavasti niiden hallinnassa ja tähän, kun lisää huolellisen suunnittelun ja sprinttimäisen kehittämisen ei paremmasta ole tietoakaan. Mutta tämä on tulossa itselläni tulevaisuudessa. Keskityn vielä loput sprintit tällä kurssilla tekemään ryhmämme projektia. 


### Oman työn seuranta ja kehittäminen

Clockifyita tullut käytettyä ankarasti kellottamalla oman ajan käytön työnteossa. Tässä vaiheessa työnajan määrällä ei ole enää väliä, sillä pyrin saamaan tämän järjestelmän asiakkaalle niin nopeasti, kuin mahdollista. (sopimussakot painavat päälle). Omalla kohdalla työaikaa menee sen verran, mitä työn tekemiseen valmiiksi asti kuluu. Nyt mahdollisesti seuraavassa sprintin suunnittelussa käytän tämän sprintin työaika seurantaa.


### Reflektio- ja itseohjautuvuusosaaminen

Kaikki asiat, jotka minun pitää hoitaa tulee tehtyä ennen dealinia.

Itseltäni löytyisi oikeaa halua työskennellä isommassa ryhmässä, jossa jokainen oikeasti tekisi jotain ilman, että heidän tekemisiään pitäisi kytätä ja kehottaa tekemään työtä päivittäin.

-    Pystyn ottamaan palautetta vastaan ilman, että suuttuisin, sillä todennäköisesti jollain toisella on tietoa, mitä minulla ei ole. Ja näin ollen tekemämme asia saattaisi parantua huomattavasti. Toki vaadin perusteluita ja pystyn itsekkin puolustamaan kantaani ja perustelemaan näkemyksiäni asioista. 
-    Ryhmän johtajana olen pyytänyt, että ongelma tilanteissa käydään porukassa ongelma läpi.
-    Pysyn aikataulussa kiinni, mikä on sovittu, niin teen sen ajoissa
-    Olen luotettava, mikäli lupaan jotain, niin pidän lupaukseni, näin ollen en jae lupauksia miten sattuu
-    Pystyn soveltamaan asioita ja ongelma tilanteissa voin neuvoa lähestulkoon vähintään pääpirteittäin, kuinka purkaa ongelmaa


### Omien työtehtävien hallinta

Pyrin viemään projektin loppuun...


### Oma viestintä

Samalla tavalla olen jatkanut viestintääni viime sprintistä asti. Pyrin olemaan mahdollisimman avoin ryhmämme kanssa ja otin kaikki huomioon työskennellessämme. Tosin nyt näyttäisi siltä, että tällä kurssilla olen "huipulla" kurssin asioista ja kehittämistä ei varmaan kauheasti tule tehtyä, ellei seuraavassa sprintissä tapahdu jotain hyvin outoa, mikä nostaisi kiinnostustani kurssia kohtaan. Hieman epäilen, joten tästä sprintti 3. lähtee vain yksi työ, joka on laskusuuntaan.


---








### 26.11.2019

Noin, nyt rauhoittuneena viikon rantti postin jälkeen kulkee taas ajatus paremmin. Alunperin piti kurssiblogia täytellä, joka päivä sillä, mitä oli tullut tehtyä kyseisellä päivällä, mutta se sitten jäi aika lyhyeen kurssin alussa, jonka jälkeen aloin varsinaisesti täyttämään kurssia blogia ensimmäisen sprintin jälkeen viikoittain. Ja nyt sähköposti Sepolta, jossa ohjeistettiin tarkempaan blogien kirjoittamiseen. Kirjoittaminen itselläni on onnistunut paremmin, kuin huonosti, mutta tätäkin olisi voinut tehdä paremmin läpi kurssin. Nimenomaan päivittäisiä muistiinpanoja/ kirjoituksia, jonka jälkeen sprintin jälkeen yhteenveto sprintistä. Kurssi kohta, niin sinällänsä tällä ei ole väliä muuttaa lähestymistapaa, mutta teen sen kumminkin ja päivittelen edellistenkin viikkojen kirjoituksia, mikäli mahdollista. Tosiaan tarkoituksena nyt kurssin loppuun asti tehdä päivittäisiä merkkauksia omista tehtävistä kurssilla ja linkittää näyttöä niiden takaamiseksi oikeasti tehdyiksi. Ja mahdollisesti sprintin jälkeisen yhteenvedon kirjoittaminen.

Aluksi voisin kertoa omasta tilanteestani tässä näin, joka on se etten välitä enää ryhmän jäsenistä, ketkä tähän mennessä eivät ole osallistuneet aktiivisesti ryhmämme toimintaan, jolloin kyseiset henkilö/ henkilöt voivat tehdä, mitä haluavat, sillä se ei enää kiinnosta minua ollenkaan. Sanottakaan tähän vielä se, että ryhmässä on vain yksi henkilö, jonka työn tekemisessä olisi ollut huomattavasti parannettavaa. Mitä tulee omaan työskentelyyn, niin keskityn nyt enemmän tuoteomistajan asioihin ja jätän kehitystyön todennäköisesti taka-alalle.

Tämän päivän kirjoituksesta tuleekin varmaan hieman pitempi, sillä ajattelin käydä läpi tässä jokaisen asian, jonka olen kurssilla tehnyt ja samalla pohdin onnistuiko omieni asioiden tekeminen vai ei ja tuon samalla esiin ajatuksiani, miksi näin on käynyt, mikä onnistui, missä olisi voinut tehdä paremmin asioita.

---

### Kurssin alku

Kurssin alkamista odotin innolla, sillä olisin halunnut oikeasti toteuttaa jonkinlaisen ohjelman, josta olisi oikeasti hyötyä muille/ itselleni. Tietenkin parempi olisi, jos asia olisi kiinnostava itselleni, esim. pelin toteuttaminen tai ohjelmisto, jota mahdollisesti itse voisin hyödyntää arjessani tai harrastuksissani. Muutama esimerkki näistä asioista olisi ollut F1 peliin telemetri sovelluksen teko, urheilun tilastojen analyyseri, esim. jääkiekko, tennis tai F1, jossa menneiden kausien tilastoja olisi voinut tarkastella laajasti ja tehdä "johtopäätöksiä", mitä ne sitten olisikaan olleet tai mahdollisesti laajempi peli, kuten esim. RTS, RPG tai muu vastaava. Tietenkin ongelmana tässä on se, että varmasti jokaisella opiskelijalla olisi ollut omat ideat, mitä olisi halunnut tehdä, joten ryhmien muodostaminen olisi varmaan näin ollen ollut hankalaa.

#### Ajattelin antaa hieman palautetta tässä näin kurssista
- Suurin ongelma itselleni oli se, että jouduin taas katselemaan vierestä, kun ryhmän jäsen pyörittää sormia ja näyttää olevan mukana ryhmätyössä.
- Ryhmien jakaminen omasta mielestäni olisi voitu tehdä parremin, toki en tiädä minkälaista algorytmiä olette käyttäneet, joten en ota kantaa onnistuiko ryhmien jakaminen vai ei kokonaisuutta tarkastellessa.
- Kurssin aiheen antaminen valmiiksi omasta mielestäni ei välttämättä ole se paras idea, toki tässä tapauksessa idea oli mielenkiintoinen ja se myös toimikin osittain. 
---
- Kurssille ryhmien jakaminen olisi voitu tehdä sillä periaattella, että opiskelijat, jotka tekevät/ saavat aikaan jotain olisivat olleet yhdessä poolissa, jonka jälkeen niistä olisi muodostettu ryhmät, jolloin ryhmiin ei olisi päässyt "päivän pilaajia". Toki tässä on varmaan hyötyä sinänsä, sillä ryhmän vetäjä saa käsityksen siitä, minkälaista on toimia vastahakoisen henkilön kanssa. Joten sinänsä asia ei ole kokonaan miinusta ryhmätyössä ja näin ollen auttaa seuraavissa ryhmätöissä toimimista, esim. työelämä ja muut ryhmässä työskentelyt. Tietenkin olisi ollut mukaa, että kaikki ryhmän jäsenet olisivat olleet aktiivisia ja osaavia, tosin kaikkea ei voi saada ja etenkin jos ymmärsin oikein, niin kurssin arvosanassa ei itse kurssityö paina niinkään.
- Se mietityttää, miksi suuntautumislinjat jaettiin omiin ryhmiin? Sinänsä omasta mielestäni olisi voinut olla parempi, jos ryhmät olisivat pitäneet sisällään eri suuntautumislinjoista henkilöitä, jolloin osaaminen ryhmän sisällä olisi ollut mahdollisesti laajempaa. Toisaalta en tiädä, mitä muut suuntautumisryhmät ovat tarkkalleen ottaen opiskelleet, joten en ota vahvaa kantaa tässä asiassa. Mutta esim. jos jokaisesta ryhmästä olisi löytynyt pari "raha-asiantuntija", muutama koodari ja sitten ICT- polkulaisia, jolloin ryhmän ideat olisivat mahdollisesti olleet mitä tahansa. 
- Olisiko kurssin osallistujien seulominen ollut mahdollista, eli jokainen olisi valinnut roolit, joita he voisivat edustaa ryhmässä, jonka jälkeen ryhmät olisivat jaettu niin, että jokaisesta ryhmästä löytyy jokainen rooli. Myös kiinnostus kurssia kohtaan olisi mahdollisesti voitu ottaa huomioon. Tosin ryhmässämme ei ollut ongelmia roolien jakamisessa, joten asia sinänsä ei ollut ongelma, vaan lähinnä komentti.
- Jos opiskelijoille olisi annettu mahdollisuus valita roolinsa, joissa hän voisi toimia, jonka jälkeen katsotaan, että jokaisessa roolissa on vaadittu määrä henkiköitä. Tämän jälkeen olisi voitu pitää nopea perehdyttäminen kyseisille rooliryhmille, mitä heiltä odotetaan kurssilla, miten toimia ja yleistä infoa. Tähän olisi mahdollisesti riittänyt nopea muutaman tunnin pikakoulutussessio + mahdollisesti yhdessä tehtäviä tehtäviä, esim. tuoteomistajat olisivat tehneet palautettavia tehtäviä tai muuta vastaavaa, jolloin ryhmänä olisi voinut oppia toisilta ja näin ollen mahdollisesti jakaa mielipiteitä ja muuta. Tästä olikin se kysymys muutama päivä sitten, jossa Seppo ehdotti roolien ryhmätapaamista, jossa olisi voinut jakaa esim. tuoteomistajat ideoita keskenään ja niin pois päin. Mutta, kun pikakoulutus olisi ollut ohi, niin olisi ryhmien muodostaminen onnistunut mahdollisesti helposti.
- Ja nopeasti vielä kurssin aiheesta, jossa tuoteomistajat olisi voinut tuoda omia toteutusideoita opettajalle julki, jonka perusteella muut opiskelijat olisivat voineet liittyä ryhmään. Tässä ideassa tuoteomistajan olisi hyvä miettiä useita ideoita, jotta ryhmän kesken päästään yhteysymmärrykseen tehtävästä asiasta. Mikäli ideoita ei olisi ollut riittävästi, oli opettajalla ollut oma kokoelma ideoita, joiden perusteella olisi sitten tehty valinta toteutettavasta asiasta.

---

#### Takaisin kurssin alku katselmointiin

Noin, eli nyt olisi tarkoitus käydä läpi, mitä olen tehnyt kurssin alusta tähän päivään asti ja samalla pohtia asioita, mitä olisi voinut tehdä toisin ja mitkä asiat onnistuivat.

Kurssin alussa ryhmäytyminen onnistui nopeasti. Sain yhteyden luotua kaikkiin TIKOlaisiin päivän sisään ja LIKOkaiset olivat saavutettu muutaman päivän sisällä kurssin alusta. Yhtä henkilö en TIKOlaisista tavoittanut ollenkaan, vaikka kyseiselle henkilölle laitoin useamman sähköpostin. Ryhmän jäseniin yhteydenotto toimi sähköpostin avulla, joka osoittautui nopeaksi välineeksi.

### Käytössä olevat ohjelmat

- Discord - https://discord.gg/e74VUM
Nopea viestintä mahdollisuus ryhmän jäsenten välillä ja mikäli olen koneella on 99% ajasta discord ikkuna auki yhdellä näytöistäni, jolloin ryhmän viesteihin pystyn vastaamaan nopeasti, mikäli asia koskee minua tai se on viesti, johon hän odottaa vastausta. Mitä tulee viestinnän hallintaan Discordissa, niin se on hieman hankalaa, sillä viestit katoavat viestimereen nopeasti. Olemme kuitenkin pyrkineet pitää asiat omissa kanavissa, mutta todennäköisesti osa viesteistä on pitkän etsimisen takana ja näin ollen discordista ei nopeasti pysty linkittämään näyttöä kurssin asioista, toki siellä muutama kanava on pelkästään ilmoitusluontoisille asioille ja näin pois päin, mutta suurinosa asioita on hukkunut. Myös opettaja pystyy, mikäli kiinnostusta löytyy, niin käymään läpi koko viestihistorian ja saada käsitys, kuinka ryhmämme on toiminut. Ja vielä yksi plussa on se, että materiaalin jakaminen onnistuu nopeasti ryhmän sisällä työtätehdessä ja sovelluksesta löytyy myös äänichatti, jota olemme käyttäneet Daily Scrumeissa, Sprint Planningissä ja Sprint Retrospektissä. 

- Azure devops - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1
Näppärä paikka taltioida kaikki oleellinen ryhmämme tekemisistä REPOon, Wikiin sekä pitää yllä toimintasuunnitelmaa, mitä olemme tekemässä kurssilla (Product BackLog). Wikin päivittelystä on vastuussa kaksi henkilöä, joista toinen olen minä.
- Wikin sisältö
    - Ilmoitustaulu
        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/51/Hackathon-Wiki
    - Pekan tapaamiset
        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/86/Pekan-tuokiot
    - Ohjauspalaverit
        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/64/Ohjaustunnit
    - Kokoukset (myöhemmin Daily Scrumit)
        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/10/Daily-Scrum-tapaamiset
    - LIKO viestintä
        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/47/Liko
    - Suunnitelma
        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/44/T%C3%A4m%C3%A4n-hetkinen-suunnitelma
    - Ongelmat
        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/14/Ongelmat

- Wiki
Wikiin on lisätty oleellisia asioita ryhmästämme dedikoidun kirjurimme toimesta, kuten Pekan tapaamisista ja ohjauspalavereista muistiinpanot tilaisuudesta. Asialistat tulevat lötymään jossain vaiheessa opettajan nähtäville, joko sähköpostilla, palautuksena moodleen tai sitten REPO linkin kautta. Nämä dokumentit pitävät sisällään ensimmäisten kokouksien asialistoja, joihin olen listannut, mitä käymme läpi kokouksissa. Kyseiset kokoukset ovat, ryhmänsisäiset aloituskokoukset, Pekan tapaamiset sekä ohjauspalaverit. Näyttäisi myös seassa olevan kurssin alusta omia muistiinpanoja ensimmäiseltä päivältä, jonka jälkeen loppui omat työni kahdeksi viikoksi.

- REPO
Repon alustelin aikaisessa vaiheessa ja käytin sitä jakaakseni demoja, kuinka voisimme toteuttaa järjestelmämme. Alla olevasta linkistä näkee commitit kyseisitä asioista.
- https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1/commits?itemPath=%2F&itemVersion=GBmaster&user=Mika%20Marjomaa&alias=mika.marjomaa%40edu.karelia.fi&toDate=2019-11-19T23%3A59%3A59.999Z

Kävimme Repon käyttämisen porukassa läpi sen jäkeen, kuin sain sen itselleni toimimaan ja katsoimme siinä, että jokainen saa sen toimimaan. Käytännössä pidin perehdyttämistuokion ensimmäisten kokouksien jälkeen.

Alhaalla seoluttu omat kommitointini sprinteittäin.

1. Sprintti
https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1/commits?itemPath=%2F&itemVersion=GBmaster&fromDate=2019-11-04T00%3A00%3A00.000Z&user=Mika%20Marjomaa&alias=mika.marjomaa%40edu.karelia.fi&toDate=2019-11-11T23%3A59%3A59.999Z
2. Sprintti
https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1/commits?itemPath=%2F&itemVersion=GBmaster&fromDate=2019-11-11T00%3A00%3A00.000Z&user=Mika%20Marjomaa&alias=mika.marjomaa%40edu.karelia.fi&toDate=2019-11-19T23%3A59%3A59.999Z
3. Sprintti
https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1/commits?itemPath=%2F&itemVersion=GBmaster&fromDate=2019-11-18T00%3A00%3A00.000Z&user=Mika%20Marjomaa&alias=mika.marjomaa%40edu.karelia.fi&toDate=2019-11-25T23%3A59%3A59.999Z

- Sprinttien nopea selittely:
    - Ensimmäisen sprintin aikana tein nopeasti demoja ryhmälle, joista he voisivat opetella perusasioita nodejs ja jquerystä mikäli asia on heille uusi.
    - Toisen sprintin käytin muutaman suunnittelu tehtävän tekemiseen, lähinnä archimatella yrittäen piirtää jotain. Lisäksi pieni muotoista demojen päivittelemistä ja piirtoohjelman alulle laitto.
    - Kolmannessa sprintissä siirryin voimakkaasti kehitystiimin osaksi, jolloin kommitteja tuli useita tehtyä ja näin järjestelmämme meni eteenpäin huomattavalla vauhdilla.

- Product BackLog

Product BackLogin idea sinänsä on itselläni suunnilleen hallussa, tietenkin siinä on paljon parantelemista, mutta näin ensimmäiseksi kertaa projektin johtamiseksi sanoisin, että olen suunnilleen tehnyt asioita sinne päin. Useita eri product backlogien malleja on tullut katseltua läpi ja näyttäisi siltä, että asialle ei ole yhtä oikeaa tekotapaa, toki voin olla tässä väärässä, mutta jatkan sen tekemistä tämän hetkisillä tiedoilla ja etsin lisää tietoa jatkuvasti.

PBL käytön harjoittamista tuli tehtyä oikeastaan ensimmäisen sprintti ajan, jolloin lisäilin asioita miten sattuu sinne. Työtehtävät lähinnä koskivat suunnittelua. 
https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_sprints/taskboard/Ryhm%C3%A4%20A_1%20Team/Ryhm%C3%A4%20A_1/Sprint%201

Seuraavan sprintin backlogin tekeminen onnistui hieman paremmin, mutta toteutus olisi voinut olla myös parempi, mutta omasta mielestäni ainakin viime sprinttiin nähden, sai homma päivitystä ja näin ollen parempi versio.
https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_sprints/taskboard/Ryhm%C3%A4%20A_1%20Team/Ryhm%C3%A4%20A_1/Sprint%202

Kolmanen sprintin backlogia tehdessä, näköjään kävi itselläni moka, sillä sekoitin PBL ja SBL kesekenään, jolloin teimme porukassa kolmannen sprintin BL:a yhdessä. Ja lopputulos oli todella heikko. Ja tämä nimenomaan johtui omasta virheestä, sillä sekoitin asioita keskenään.
https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_sprints/taskboard/Ryhm%C3%A4%20A_1%20Team/Ryhm%C3%A4%20A_1/Sprint%203

Neljänteen sprintin baacklogiin olin ottanut enemmän selvää asioita ja ymmärsin tässä vaiheessa viimeinkin kokonaisuudessaan, että minä tuoteomistajana olen vastuussa PBL. Eli se on minun vastuullani päivittää PBL, jonka jälkeen sprintin suunnittelussa valitaan tehtäviä PBL, jotka suoritetaan sprintin aikana.
https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_sprints/taskboard/Ryhm%C3%A4%20A_1%20Team/Ryhm%C3%A4%20A_1/Sprint%204

Näin käytännössä oma PBL lahaa tällä hetkellä aika paljon jäljessä. Näin ollen olisi ollut huomattavasti parempi, mikäli olisin ymmärtänyt PBL toiminnan heti kurssin alussa, jolloin olisi sprintin suunnittelutkin onnistuneet edes hieman paremmin.

---

Noin nyt pääsen kertomaan kurssin kulusta todennäköisesti ilman katkoksia. Aloitin kurssin kutsumalla ryhmämme jäsenet Discordiin, jonka jälkeen kävimme hieman keskustelua siitä, kuka olisi ryhmämme johtaja. Vapaaehtoisia ei tuolloin löytynyt, joten ehdotin itseäni johtajaksi ja näin ollen minusta tuli silloin ryhmämme johtaja. Ajatus oli ryhmässämme ideoida kukin omalla kohdallaan muutama päivä, siitä miten/ mitä tekisimme. Muutaman päivän ajan olin valmistellut omia ideoitani piirrustuksien sekä selittelyjen kera. Jonka jälkeen oli aika ryhmässä käydä läpi kunkin jäsenen tuotokset. Sinänsä sain tilaisuudessa sen käsityksen, että osa ei ollut tehnyt juurikaan mitään, joten pidin esittelyni ryhmälle, jonka seurauksena syntyi keskustelua muutaman henkilön kanssa. Tämän jälkeen palautin palautettavat tehtävät ja heitin käytännössä hanskat naulaan vähäksi aikaa kurssista, sillä mikäli tästä nyt tulee taas samanlainen kurssi, kuin viime kertainen, on minun siihen hyvä valmistua huolella. Joten tästä kaksi viikkoa en käytännössä tehnyt juurikaan mitään kurssilla. Saatoin katsoa jotain asioita Scrumista, mutta en ole täysin varma siitä.

Kahden viikon kuluttua, huomasin selatessani kurssin moodletilaa, jossa törmäsin Pekan tapaamisajankohtiin. Tämän seurauksena kutsuin "hätäkokuksen" ryhmässämme seuraavaksi maanantaiksi. Sinänsä koko homma johtui minusta, joten otan kaiken vastuun omasta tekemisistäni koko kurssin ajalta. Kokous pidettiin maanantaina päivää ennen ensimmäisen Pekan tapaamista. Kävimme läpi mitä olemme tekemässä ja saimme käsityksen silloin tulevasta järjestelmästä. Samaisena päivänä yksi ryhmän jäsenemme teki ensimmäisen prototyypin käyttöliittymästä. Saatuani kuvat haltuuni muistaakseni tein esittelystä scriptin, mutta voi olla, että näin en tehnytkään tai sitten kyseinen tiedoto on vain kadonnut jonnekkin.

Aloin tällöin katsomaan, kuinka PBL ja SBL toimivat. Saattoi olla, että tässä vaiheessa molemmat asiat olivat minulle yksi ja sama. Joten tein ensimmäisen SBL kokeilu mielessä, jonka jälkeen lähdimme ensimäisessä sprintissä tekemään kyseisen sprintin tehtäviä. Työn tekeminen sinällänsä näytti olevan jo tuolloin hankalaa, sillä suurinosa tehtävistä jäi tekemättä. Kyseiset tehtävät olivat suunnitelu tehtäviä. Voi olla, että SBL oli sekava, mutta varmaa on ainakin se, että se olisi voinut olla parempi. Pidimme ryhmässämme Daily Scrumin sijaan päivittäin kokouksia kirjotuschattiä käyttämällä, jolloin kyseiset kokoukset olivat noin tunnin pituisia ja todennäköisesti todella turhia. Pidimme näitä kokouksia 4.11.- 12.11. välillä.

- https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/52/1.Kokous

Ylhäällä olevasta linkistä voi tarkastella kokouksista tehtyjä muistiinpanoja (itse tehtyjä). Näihin kokouksiin valmistuin aina huolella, eli tein asialista jokaiseen kokoukseen ja kävimme yhdessä ne läpi. Asialistat löytyvät omalta koneelta, mutta ne tuon julki jossain vaiheessa, joko sähköpostilla, repon välityksellä tai moodlenpalautukseen. Nämä asialistat pitävät sisällään asioita, jotka omasta mielestäni oli hyvä selvittää ennen, kuin aloimme tekemään yhtään mitään. esimerkiksi jäsenten taitojen kartoitus, ideointia, huolia, kysymyksiä, miten jatkamme etenpäin ja ongelmia. Nyt jälkeenpäin olisi hyvä ollut nostaa esille enemmänkin asioita, kuten riskit ja muut ongelma tekijät, jotka mahdollisesti haittaavat ryhmämme tekemisiä.

Olin itse kokouksien vetäjä ja tämä johtuu lähinnä siitä, että olin joka paikassa tekemässä, mitä sattuu, mutta käyn läpi tarkemmin omista rooleistani ryhmässämme läpi myöhemmin. Scrumlistaa katsoessani otin asioita yksi kerrallaan listasta ja selvitin google.comin avulla, mitä asiat ovat ja miten ne toimivat. Suurin asia, jonka olin tehnyt väärin oli Daily Scrumit. Viimeinen kokous pidettiin 12.11., eli siirryimme Daily Scruimeihin 13.11. ja olin niiden vetäjä. Samalla etsiessäni tietoa kävi ilmi, että scrummasteri on henkilö, joka pitää daily scrumit. Joten sovin hänen kanssaan, että voi ottaa ohjat tapaamisissa, kun hänelle se sopisi. Joten pidin noin viikon verran Daily Scrummeja niin, kuin niitä pitääkin pitää.

#### HUOM tästä voi lukea enemmän 7.11.md tiedostosta (kokouksien asialistat ovat vielä vain omalla koneellani)

Suurimmat muutokset kuitenkin näissä oli se, että pidämme tapaamiset äänichatissä ja tilaisuus kestää maksimissaan 15 minuuttia.
Tämä päivä 7.11. näyttäisi olevan myös se päivä, jolloin saimme kokonaisuudessaan valmiiksi idean, jota alamme tekemään. Alta löytyy kyseinen suunnitelma.

- https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/44/T%C3%A4m%C3%A4n-hetkinen-suunnitelma

Tapaamisista en ole enää itse tehnyt muistiinpanoja, ellen huomaa jotain mikä on ongelmallista tai tuon jotain asioita itse esiin.

Tein Azuren wikiin sivuston, jonne ongelmia olisi voinut kirjoitella, mutta näyttäisi siltä, että ongelmia ei olisi. Pieniä ongelmia aina silloin tällöin on löytynyt, jotka ovat sitten daily scrumissa saatu käytyä läpi ja ratkaistua saman tien, joten näitä ongelmia ei tuonne ole listattu. Jotain ongelmia/ kysymyksiä saattaa löytyä pitämistämme kokouksista, joissa kirjoitin aina kommentteja ylös, mikäli ne olivat huomion arvoisia. Voi olla, että kommunikaatio ei ole pelannut ongelmien kirjaamisessa loppuun asti tai sitten niitä ei ole viitsitty sinne kirjoittaa tai sitten niitä ei ole ollutkaan.

- https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/14/Ongelmat

Työn tekemisestä olen ensimmäisestä sprintistä asti huomauttanut ryhmäämme ja osaa tietenkin asia ei koske, mutta joukossa ainakin yksi henkilö löytyy, kenen osallistuminen ryhmämme työn tekoon olisi aivan sama, että onko hän paikalla vai ei. Sanottakoon, että mahdollisesti olisimme pitemmällä, jos kyseistä henkilöä ei olisi ryhmässämme ollut, jolloin ei asiasta olisi tarvinnut huomautella vähän väliin ja samalla olisi pitänyt itsenikin täysillä mukana kurssilla. Kolmannen sprintin keskivaiheella Daily Scrummissa pidin puheen, jossa tein selväksi, että olen käytännössä pettynyt työntekemiseen ja huomatin asiasta viimeisen kerran. Jotenka minulle tämän jälkeen oli aivan sama onko joukossamme henkilöitä, ketkä eivät tee mitään, sillä en enää huomio heitä ollenkaan loppu kurssilla ja näin ollen seuraavan kerran kiinnitän heihin huomiota vertaispalautetta kirjoittaessa.

~Toisen sprintin aikana aloin myös aktiivisemmin tekemään viestintää LIKOn kanssa. Käytännössä toimme molemmat osapuolet ajantasalle siitä, mitä olemme tehneet. Tilanneraportteja pystyy seuraamaan täältä:

- https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/47/Liko

Sinänsä olisi ollut varmaan parempi, jos olisimme pitäneet lyhyen noin puolituntia kestävät puhetilaisuuden, johon olisi osallistunut molemmista ryhmistä "johtaja" ja kirjuri. Mutta sinänsä onko tässä millään enään väliä.


##### Omat tekemiseni

Olen tehnyt hieman kaikkialla asioita tällä kurssilla, eli ryhmässämme olen käytännössä toiminut jokaisessa roolissa. On se sitten hyvä asia tai ei, niin ainakin olemme saaneet jotain aikaiseksi. Sinänsä hieman epäselvää saattaa vielä olla se, onko muut jäsenet tutustuneet kuinka tarkasti Scrum käytäntöihin, sillä minusta tuntuu, että olen aina ollut asioiden alulle panija asiasta riippumatta. Toki voi olla, että olen liian paljon tehnyt asioita, joka voi olla aiheuttanut sen, että muut jäsenet eivät ole pystyneet "esittämään" omia roolejaan. Toisaalta, omasta mielestäni katsoin ryhmämme toimintaa hetken ja otin asioita haltuun sitä mukaan, kuin niitä itse ymmärsin. Voi myös olla, että olen tehnyt kaiken/ suurimman osan asioista väärin, jolloin se on vaikuttanut myös muihin ryhmämme jäseniin.

- Olen toiminut Scrummasterin roolissa pitämällä Daily Scrummeja
    - näitä voi tarkastella Wikistä, tosin siellä on vain muutama muistiinpano Daily Scumeista, sillä koin tarpeettomaksi kirjoittaa ylös asioita, jotka kävimme läpi siinä hetkessä.
    - Olen tiedustella ongelmia/ haasteita vähän väliin ryhmämme tekemisistä ja mikäli näitä ilmeni, pyrin ne ratkaisemaan.

Lopetin näiden asioiden tekemisen, kun Daily Scrumejen vetovastuu lähti pois minulta scrummasterille.

- Olen toiminut kehitystiimin jäsenenä
    - olen tuottanut paljon koodia järjestelmämme valmistumisen kannalta. Sinänsä olin siinä uskossa, että järjestelmä pitää olla valmis kurssin lopussa, mutta noin viikko sitten aloin miettimään, että tässä varmaan enemmänkin harjoittelemme Scrumin periaatteita ja kuinka niitä noudattamalla pystyy kehittämään sovelluksia. Näin ollen seuraavien sprinttien aikana todennäköisesti en osallistu enää niin voimakkaasti kehitystyöhön.
    - Alunperin, miksi lähdin sorvaamaan koodia oli se, että jos en olisi osallistunut kehitystyöhön, niin projekti olisi todennäköisesti ollut todella kaukana siitä, mitä olin ajatellut. Tähän vielä lisäksi yhden henkilön pelleily, niin kehitystiimi pieneni neljään henkilöön. Kyseisillä henkilöillä todennäköisesti olisi ollut vain yhdellä jäsenellä taitoja toteuttaa käyttöliittymä, jolloin työmäärän jakaantuminen olisi ollut todella karu, käytännössä sama tilanne, mikä itselläni oli viime vuoden ohjelmistotuotanto kurssilla.
    - Tein alussa demoja tulevasta järjestelmästä, joita ryhmämme jäsenet pystyivät katsomaan ja mahdollisesti käyttämään hyödyksi kehitystyössä. Ainostaan yksi henkilö näyttäisi olevan kyvykäs toteuttamaan toiminnallisuutta ja kyseisen henkilön kanssa olen rakennellut käyttöliittymää. Eli käytännössä osittain oma toiveeni toteutui tällä kurssilla, joka oli saada tuottaa koodia toisen henkilön kanssa ja näin ollen nähdä hieman tarkemmin, miten toinen henkilö kirjoittaa koodia. Mikä sinällänsä oli todella positiivinen kokemus itselleni. Nyt vain odottelen sitä hetkeä, kun ryhmässä on kuusi henkeä, jotka kaikki osallistuvat kehitystyöhän ja omaavat taidot tuottaa koodia/ toiminnallisuutta.

- Olen toiminut tuoteomistajana
    - Eli tämä on minun varsinainen roolini kurssilla. Johon tulen keskittymään viimeiset ajat kurssilla, tietenkin tämänkin olisi voinut tehdä niin, että olisin vain keskittynyt ylläpitämään ryhmäämme, mutta asioita ei voi muuttaa ja tehty on tehty. Joten menen näillä opeilla eteenpäin ja pyrin olemaan mahdollisimman hyvä tuoteomistaja kurssin lopuun asti.
    - Olen toiminut puheenjohtajana ohjauspalavereissa, joihin olen valmistautunut huolella. Käytännössä asialistan laatiminen ja sen läpikäynti äänen muutaman kerran harjoituksena. Toki puheessani on paljon parannettavaa, kuten hengittäminen, taukojen pitäminen ja äänensävyyn "monimuotoisuutta". Kyseiset asialistat löytyvät nekin vain ja ainoastaan tällä hetkellä koneeltani, mutta tuon ne julki myös teille jossain vaiheessa. Lisättäköön tähän vielä se, että olen pyrkinyt vaihtamaan puheenjohtajuutta palavereissa, mutta halukkaita hommaan ei ole ollut, joten olen ne hoitanut itse.
    - Pekan tapaamiset olen pyrkinyt hoitamaan huolella. Ensimmäinen tapaaminen oli hieman outo, käytännössä ensimmäinen koskaan tilanne, jossa olen "asiakkaan" kanssa tekemisissä ja näin ollen saattoi hieman olla "kyllä, kyllä" meinikiä esittelyssäni. Tosin myös materiaaliakin ei ollut niinkään tarjolla minulla esityksessä, tosin johtui itsestäni, joten ongelma oli vain ja ainoastaan itseaiheutettu. Mitä tulee 2. ja 3. Pekan tapaamiseen, olen laatinut niistäkin asialistat, jotka tuli harjoiteltua useampaan kertaan ennen esitystä ja näin ollen varmistaen, että edes kuulostaisin siltä, että tiedän mistä puhun. Nämäkin luonnokset ovat saatavilla jossain vaiheessa.
    - Olen toiminut viestinvälittäjänä oman ryhmäni sekä LIKOn välillä. Eli pidän molemmat osapuolet tietoisina toistemme tekemisistämme.
    - Ja lopuksi vielä mainittakoon, että olen hoitanut ryhmämme asioita mm. tehtävien palauttaminen, ohjauspalavereiden ajanvaraukset ja käytännössä kaiken muunkin, joka vaatii toimenpiteitä.
    - Olen pyrkinyt kannustamaan ryhmääni tekemään töitä ja kuuntelemaan ongelmia/ kysymyksiä/ haasteita jne., jotka sitten käymme porukassa selvittämään ja muokkamaan ryhmämme toimintaa.
    - Olen pitänyt sprinttien suunnittelu palavereita, mutta ainakin tähän asti niistä ei juurikan ole tullut mitään, sillä vähän aikaa sitten ymmärsin käytännössä, mitä PBL ja SBL ovat ja näin ollen pyrin saamaan edes viimeisen sprintin suunnittelun onnistumaan edes melkein sillä tavalla, kuin se pitää tehdä.
    - Olen pitänyt sprinttien retrospektejä, joista ensimmäinen kerta oli hieman hakuilua ja se oli käytännössä nopeasti ohi. Tämän jälkeen emme pitäneet sitä toisesta sprintistä, vaan tarkoitus oli yhistää sekä toinen että kolmas sprintti yhtenäiseksi katselmoinniksi. Tähän syynä oli alhainen työnteko osalla porukkaa ja pidin "puheeni" työntekemisestä ennen kyseistä palaveria. Kyseinen paveleri pidettiin 26.11. ja omasta mielestäni asia ainakin itseäni auttoi "henkisesti", sillä kävimme läpi kukin vuorollaan oman panoksen viime sprintissä ryhmämme panokseen ja myös mahdolliset ongelmat, kysymykset ja muut huomiion otettavat asiat. Ensimmäisen kerran ryhmässämme oli palavereissa omasta mielestäni henkisyyttä mukana, kiitos siitä suurimmalle osalle porukkaa, jotka toivat esiin ongelmia, kysymyksiä ja parannusehdotuksia, joita yhdessä kävimme läpi ja pyrimme korjaamaan tilanteen seuraavaan sprinttiin, ettei kyseisiä asioita tapahdu enää tulevaisuudessa. Tietenkin tapaamista hieman varjosti tämä henkilö, joka ei ole tehnyt mitään, ei myöskään hänellä ole ollut mitään sanottavaakaan näissä kokouksissa koskaan, tosin olen hyväksynyt asian, joten asia ei minua enää kiinnosta.


##### Ryhmän työnteon ohjaaminen (mitä teemme ja muuta)

Noin, suurinpiirtein kaiken olen kertonut, mitä muistan ja minkä voin suunnilleen todentaa dokumenttien avulla. Varmaan jotain jäänyt pois unohtumisen takia, mutta viimeiset viikot tulen tekemään tätä blogia joka päivä. Mielestäni olen hoitanut tuoteomistajan roolin tähän mennessä välttävästi, useammassa kohdassa olisin voinut toimia paremmin ja jopa täysin toisin. (Tarkemmin asiasta 28.11.2019 alempana)





### 27.11.2019

Päivä tuli tehtyä muita asioita, sillä tein "tupla vuoron" eilen, mutta päivän myötä välillä tuli mietittyä kurssin asioita esim. koiran kanssa lenkillä.




### 28.11.2019

Aluksi täytyy hieman ottaa aikaisempia sanomisiani takaisin päin. Kehitystiimistä löytyy yksi henkilö, jonka työpanos on ollut kurssin kohdalla enemmän, kuin riittävästi. Näin ollen mikäli en olisi liittynyt kehitystiimiin olisimme kyllä saaneet varmasti jotain aikaiseksi, tosin tällöin kyseiselle henkilölle olisi kyllä kasautunut työtä sen verran paljon, ettei hän olisi enää koskaan tehnyt töitä tulevaisuudessa. Tämä henkilö on työpanoksellaan vienyt ryhmämme toteutusta todella pitkälle ja mikä asiasta tekee vielä paremman on se, että pystyn hänen kanssaan tuottamaan koodia ja näin ollen sain sen mitä odotinkin kurssilta. Ongelmiahan tästä syntyy työmäärän jakautumisen suhteen. Käytännössä ryhmämme työpanos kehitystyön aikana on jakautunut todella epätasaisesti. Suunnilleen tämän henkilön työpanos on ollut noin 3/ 7 ryhmämme tekemisistä kehitystyössä. Itselleni sanoisin sen olevan jotain 2/ 7 luokkaa. Äsken mainittuun työpanokseen kuuluu vain uuden toiminnallisuuden tekeminen järjestelmään, näin ollen loput 1/ 7 jakautuu kolmen henkilön kesken ja 0/ 7 kuulu yhdelle henkilölle.

Tänään tapahtui todella positiivinen asia itselleni "ryhmässä" työskentelystä. Käytännössä työskentelemme parityönä, mutta se ajaa saman asian, sillä hän on toinen henkilö, joka pystyy tuottamaan koodia. Itse asia oli mielenkiintoinen kokea, joka oli se, että yhdessä rakennamme ohjelmaa, johon molemmat tekevät toiminnallisuutta itsenäisesti, jota sitten ainakin itse pystyn ymmärtämään. Mitä se sitten olisikaan ollut, jos meitä olisi ollut se kuusi henkeä näpyttelemässä koodia. Tätä emme koskaan tule saamaan selville. Mutta sen verran pystyn sanomaan, että olisimme pystyneet etenemään huomattavasti nopeammin, jolloin järjestelmästäkin olisi tullut käyttökelpoisempi ja mahdollisesti oikea tuote. Mutta, jos ajattelisin tilannetta siltä kannalta, että olisimme yritys, niin olisin puhaltanut pelin poikki jo hyvin kauan sitten.

---

Eilisen päivän mietiskelystä jäi parhaiten mieleen se, että PBL:n kohdalla saatoin ajatella asioita aivan liian monimutkaisesti. Listaan seuraavaksi alle ongelmia, joita totesin omassa tekemisissäni.

- Suurin ongelma ehkä oli se, että otin itselleni todella monta roolia ryhmässämme.
    - Ryhmämme jäsenten taitojen kartoituksesta ja mielenkiinnosta kurssia kohtaan sain sen käsityksen, että ongelmia ei olisi pitänyt ilmetä, jos tekisin malleja, miten asioita voisi toteuttaa. Tämä tietenkin söi omaa aikaani ja lopputulos oli mikä oli. Käytännössä tekemiseni oli enemmän turhaa, kuin siitä oli hyötyä. Toisaalta saatoin auttaa yhtä henkilöä kehitystiimistä, mutta hän todennäköisesti olisi nämä asiat pystynyt myös sisäistämään itsenäisesti käyttäen google.comia.
    - Seuraava rooliongelma oli se, että tiedostin kehitystiimin taitojen puutteet, joten koin tarpeelliseksi liittyä itse kehitystiimiin. Tosin halusinkin katsoa hieman sitä, miltä ryhmässä koodaaminen tuntuu ja miten se käytännössä toimii. Mutta tästä johtuen suuriosa ajastani kului enemmän koodatessa, kuin PBL hallinassa.

- PBL tekeminen alusta asti oli hieman kankeaa.
    - Asiaa hieman pohtineena tulin siihen lopputulokseen, että mietin PBL enemmän koodaus mielessä, kuin järjestelmän yleiskuvan kannalta. Käytännössä mietin, enemmän sitä kuinka asioita voisi toteuttaa (koodi mielessä), kuin sitä, mitä asioita pitäisi toteuttaa. Näin ollen yleiskuva mielessäni tuotteesta oli enemmän koodirivimäistä, kuin itse tarkempi kuvaus järjestelmästä.
    - Kurssin alkupuolella tein tarkemman idean tuotteesta, mutta jostain syystä se jäi sitten taka-alalle. Olisi ollut huomattavasti parempi, jos olisin keskittynyt tuotteen idean jalostamiseen ilman, että olisin miettinyt toteutuksen liittyviä asioita ja näin ollen asiat eivät olisi menneet sekaisin.
        - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/44/T%C3%A4m%C3%A4n-hetkinen-suunnitelma

- Ajatus siitä, mitä ryhmä pystyy tekemään vaikutti paljon toimintaani.
    - Mikäli ryhmä olisi omannut taitoa ja osaamista, ei minun olisi tarvinnut "puuttua" kehitystiimin tekemisiin, sillä viiden henkilön voimalla olisi järjestelmä jo valmis.

- Passiivisuus ryhmän sisällä kurssin alussa
    - Ryhmän passiivisuus veti minun kiinnostukseni kurssia kohtaan hyvin matalaksi, näin ollen tein asiat mitkä olivat pakko tehdä ja jätin itsenäisen asioiden selvittämisen todella pieneksi, kuin mitä se on ollut muilla kursseilla itselläni.

- "Epäselvyys" mitä opiskelemme
    - Käytännössä tiedostin jo alussa, että opiskelemme Scrumia, mutta keskityin huomattavasti enemmän siihen, että meillä olisi valmis järjestelmä kurssin lopussa. Sinänsä jos olisin puhtaasti keskittynyt tuoteomistajana kurssiin ja miettinyt onko vain yhden henkilön järkevää kehittää tuotetta sillä välin, kuin neljä muuta katsoo vierestä, mitä hän tekee, olisin keskeyttänyt työnteon jo aikoja sitten. Sinänsä asia ei ollut ihme, sillä suuriosa projekteistahan myöhästyy tai kokonaan lopetetaan ennen niiden valmistumista.

---

Aamuyön käytin PBL muokkaamiseen ja nyt olen siihen huomattavasti tyytyväisempi kuin, mitä se oli esim. viikko sitten. Näin ollen pystymme vihdoin pitämään sprintin suunnittelun, tosin jos vain yksi henkilö on kehitystiimissä, niin siinä ei paljoa suunnitella. Ottaen myös huomiion, että kurssi on kohta ohi, niin montaa pistettä toiminnastani ei kyllä voi antaa.

**Alla löytyy graaffi omasta kiinnostuksen tasosta kurssilla (mennyt + tuleva)**

![](https://i.imgur.com/HrsiQjf.png)

0. Kurssin alku, tässä olin innostunut aloittamaan kurssin ja työskentelemään ryhmänä. Aika nopeasti ensimmäisten päivien aikana kiinnostukseni tippui olemattomiin ja samalla tekeminen loppui tyystin.
1. Suunnittelu viikko, ei tekemistä itsellä
2. Suunnittelu viikko, ei tekemistä itsellä
3. Ensimmäinen sprintti, aloin hyvin varoivaisesti tekemään asioita, tosin kiinnostus saattoi käydä viitosen tienoilla, mutta mikäli näin oli, tuli se sieltä hyvin nopeasti alas.
4. Toinen sprintti, oikeastaan sama homma, asia ei kiinnostanut, sillä viime viikon hommista jäi ryhmältä suurinosa tekemättä, joten olin itsekkin sitten sillä kannalla, että ei täällä kenenkään tarvitse tehdä mitään.
5. Kolmas sprintti, itse kehitystyön aloitus. Alku viikosta olin toteuttamassa toimintaa osana kehitystiimiä. Kiinnostukseni alkoi hieman nousta tässä vaiheessa, sillä ainakin yksi henkilö omasi taidot tehdä asioita.
6. Neljäs sprintti, kiinnostukseni nousi takaisin "normaaliksi", eli keskityn kurssiin nämä viimeiset hetket. Tämä johtui siitä, että kehitystiimistä tosiaan löytyi henkilö, joka on saanut huomattavan määrän aikaan itsekseen.
7. Viides sprintti, jatkan samaan malliin, kuin edellisenkin sprintin. Keskityn työskentelemään kurssin viimeiset ajat niiden henkilöiden kanssa, jotka tekevät asioita.
8. Kuudes sprintti, mikäli semmoinen on, olen siinäkin mukana ja varsinkin, jos järjestelmästä on esitys tehtävä Pekalle.

---

Mikäli järjestelmä on käyttökelpoinen tämän sprintin aikana, eli muutaman sivun aloittaminen ja niiden tekeminen käyttökelpoiseksi, niin voi olla, että seuraavan sprintin alussa, voisi LIKOlaiset mahdollisesti tutustua tuotteeseen ja kertoa omia mielipiteitä sekä parannusehdotuksia tuotteesta. Ongelmia tästä tulee siinä, että mahdollisien muutoksien tekeminen saattaa jäädä pois, tämä oikeastaan riippuu puhtaasti siitä, että onko kurssissa kuudes sprintti, jolloin viidennessä sprinttissä teemme viimeiset toiminnallisuudet loppuun ja käytämme kuudenen sprintin tuotteen hiomiseen.


---

### 30.11.2019

REPO- päivityksien tilannekatsaus tähän mennessä. Azure DevOps vaati adminoikeuksia, että lisäosien asentaminen olisi onnistunut, joten päätin tehdä pikaisesti pienen koodipätkän, joka käy läpi, jokaisen tehdyn päivityksen (commits) REPOon.

    Commiter { _CommiterName: 'JaniHarkonen', _CommitsCount: 41 },
    Commiter { _CommiterName: 'Riku H├ñrk├╢nen', _CommitsCount: 46 },
    Commiter { _CommiterName: 'Valeria Vasylchenko', _CommitsCount: 20 },
    Commiter { _CommiterName: 'Mika Marjomaa', _CommitsCount: 85 },
    Commiter { _CommiterName: 'Lauri Hiltunen', _CommitsCount: 65 },

Perjantaina pyysin ryhmää täyttämään alla olevan dokumentin omalta kohdaltaan, jonka pohjalta voin katsoa, mitä tulemme tekemään seuraavassa sprintissä.

- https://hackmd.io/rYSZQjRCQ6W0_MIABoCuRQ?view


___

### 01.12.2019

https://hackmd.io/0VCVYh1STJyQSqQ-YfZrFg?view

- LIKOlle tehty tilanneraportti ryhmämme tilanteesta

---


## Sprintti 4 (26.11.-2.12.)

### Aikataulun seuranta:
#### https://clockify.me/bookmarks/5de50acb9fbfdb5c5296d8ea

### Omia kommitteja repoon
- https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1/commits?user=Mika%20Marjomaa&userId=61fd4bcb-cb25-6311-9036-cef75664cad3&fromDate=2019-11-25T00%3A00%3A00.000Z&toDate=2019-12-03T23%3A59%3A59.999Z

Kommitit koskevat lähinnä bugien/ glitchien korjauksia sekä pienien muutoksien tekemiseen siellä sun täällä koodia.


### Työpanokseni ja sitoutumiseni projektiin

Olen nyt tehnyt asioita paremmin viimeisen viikon ajan ja pystyn näkemään monia ongelmakohtia, joita syntyi/ oli kurssin alussa. Suurimpana ongelmani pidin tuoteidean näkemistä itselläni selkeänä kokonaisuutena, sillä olin siinä ajatuksessa, että ryhmästä löytyisi paljon osaamista ja näin ollen pystyisimme toteuttamaan lähestulkoon mitä vain. Kun asia ei näin ollut, jouduin ajattelemaan toteutusta enemmän 'onko toteutus mahdollinen/ kuinka toteutus tehdään' periaattella ja näin ollen itse tuoteidea sumeni päässäni. Tämän seurauksena kaikki ongelmakohdat yhdistyivät yhdeksi suureksi epäonnistumiseksi, jos ajattelen omaa tekemistä Scrum periaatteiden kannalta kurssin alusta neljänteen sprinttiin.

Olen viime viikosta asti pitänyt asioita enemmän mielessä ja myös dokumentoinut niitä tarkemmin tässä blogissa. Sanottakoon vielä kerran tähän se, että jos olisin kurssin alusta asti pitänyt huolellisesti PBL yllä ja sen mukaan pitänyt sprintin suunnittelun kerralla sprintin alussa, jossa olisi tullut tehtyä SBL. Näin ollen SBL ei olisi muuttunut lähestulkoon päivittäin ensimmäisten sprinttien aikana. Toisaalta, tehtävien tekeminen tuntui osalla jäävän viimeisille päiville, joten olisiko tuolla omalla toiminnallani ollut loppujen lopuksi mitään merkitystä. Työnteko rantin jälkeen, jonka pidin kolmannessa sprinttissä, alkoi ryhmämme osa jäsenistä tekemään huomattavasti enemmän töitä ja näin ollen saimme paljon aikaiseksi lyhyessä ajassa. Jos työnteko olisi ollut samanlaista kurssin alusta, niin olisi lopputulos järjestelmästämmekin ollut paljon parempi.

Työn jakautumista tasapuolisesti ryhmämme sisällä, ei mitenkään olisi voinut tehdä oikeuden mukaisesti. Sillä osaaminen ryhmämme jäsenillä oli laidasta laitaan, jolloin vain harva pystyi ottamaan osaa itse kehitystyöhön. Toki olen aina pyrkinyt jokaisessa sprintissä kysymään, mitä kukin voi tehdä ja samalla kysellyt osaamistakin. Myös osa ryhmästä on tuonut aina välillä ideoita, jonka pohjalta heille on saatu työtä tehtäväksi.

Sinänsä pyrin nämä viimeiset hetket parantamaan ryhmämme toimimista Scrum periaatteiden pohjalta. Olen aktiivisesti katsonut asioita, joita teemme 'väärin' ja näin ollen pyrkinyt parantamaan niitä. Toisaalta, periaate Scrumista on ollut koko kurssin ajan selvä, mutta pienet asiat yhdessä muodostavat suuren epäkohdan ja näin ollen, olen pyrkinyt muokkaamaan toimintatapaa pienissä asioissa 'oikeammaksi'.

Olen tiputtautunut melkein kokonaan pois kehitystiimistä, tosin saatan pieniä muutoksia tai bugeja korjailla, mikäli törmään semmoisiin järjestelmää käyttäesssä. Sinänsä 'ryhmässä' työskentelyä tällä kurssilla ei ole tapahtunut, johtuen ryhmän jäsenten taito eroista. Varsinkin nyt, kun lopetin kehitystyön tekemisen, on töiden tekeminen pääsääntöisesti yhden jäsenen harteilla.



### Oman työn seuranta ja kehittäminen

Olen pistänyt vain työtunnit ylös, jotka ovat tuoneet arvoa projektille, kuten esim. asiakastapaamiset ja siihen kuuluvan esityksen laatiminen+harjoittelu, sprinttien suunnitellu ja retrospektit, daily scrumit, mikäli olen osallistunut niihin muutenkin, kuin vain kuuntelemalla ja muut asiat, jotka ovat auttaneet suoraan projektin/ ryhmän tekemistä.

Asioita, joita ei tule laitettua ylös, ovat olleet: Scrumin käytäntöjen opiskelu, projekti blogin kirjoittelu ja muut asiat, joista ei ole suoranaisesti tullut kongreettista arvoa projektille.

Työstämme ei tulisi kenenkään maksaa yhtään mitään, tai mikäli joku olisi rahoittanut toimintaamme olisi se ollut todella iso virhe. Pitkään oma mielipiteeni projektistamme on ollut se, että sen tulisi olla valmis niin nopeasti, kuin mahdollista. Näin ollen emme tuhlaa kenenkään rahoja ja vältymme sopimussakoilta.



### Reflektio- ja itseohjautuvuusosaaminen

Idea kehittää sovellusta Scrum periaatteita noudattaen on mielenkiintoinen ja varmasti toimiikin, mutta kehityksen onnistumisen takaamiseksi olisi hyvä, jos ryhmästä löytyisi oikeaa halua toteuttaa sovellusta, sekä taitoa tehdä asioita itsenäisesti. Mikäli ryhmässä ei olisi vapaamatkustajia ja työskentely olisi ollut kaikilla aktiivisempaa jo heti kurssin alusta asti, näin ollen olisin itsekkin katsonut tarkemmin Scrum periaatteita jo kurssin alusta alkaen. Sinänsä kurssin edetessä on myös osa ryhmästäkin alkanut enemmän tekemään asioita, joka on positiivista.

Kurssin puoleen väliin luulin arvosanan koostuvan enemmän lopullisesta työstä, jonka toteutamme, mutta viime viikolla aloin miettimään kurssin päätarkoitusta, joka varmaan on enemmän sitä, että ajatus olisi opiskella Scrumin toimintaa, kuin itse järjestelmään kehitystä. Näin ollen keskityin henkikökohtaisesti siihen, että meillä olisi järjestelmä valmiina kurssin lopussa. Tässä sprintissä kääntyi ajatukseni opiskelemaan itse Scrumia järjestelmän toteuttamisen sijaan. Todennäköisesti ja niin kuin onkin, tapahtui muutos liian myöhään, mutta Scrumin perusideat itselläni on hallussa ja olen tämän sprintin ajan opiskellut/ tutkinut Scrumia, miten se toimii. Joitain huomioita tähän asti omasta työskentelystä.

- Mikäli olisin keskittynyt vain ja ainoastaan tuoteomistajan rooliin, olisi Scrumin asioiden toteuttaminen onnistunut paremmin.
    - Tästä johtuen olin kurssin alussa aktiivisesti toteuttamassa järjestelmään toiminnallisuutta.
- Azure devopsi osoittautui mielenkiintoiseksi ja todella hyödylliseksi paikaksi hallinnoida projektia.
    - PBL:n toteutus oli/ on pirstaleina eri paikoissa. Osa tehtävistä löytyy Azuresta ja loput TODO-listasta reposta.
    - Sprintin suunnittelu olisi voinut olla paremmin toteutettu. Nämäkin löytyvät osittain mistä sattuu. Osa sprinttien suunnitteluista tuli tehtyä Daily Scrumeissa kurssin alkuvaiheessa. Viimeisen suunnittelun pidimme tänään (3.12.) ja se onnistui paremmin, toisaalta jos kehitystiimissä on vain yksi jäsen, ei asioita SBL voinut hirveästi laittaa.
- Scrummasteri ei välttämättä ole katsonut hänelle kuuluvia tehtäviä läpi
    - Tästä johtuen pidin Daily Scrumit kurssin alussa
        - Nämä olivat myös eräänlaisia kokouksia, jonka jälkeen niistä tuli oikeaoppisia Daily Scrum tapaamisia ja todennäköisesti onnistuneet parhaiten Scrumin liittyvistä asioita.
    - Sprinttien retrospektejäkin on tullut itse pidettyä
        - Tänään (3.12.) syventyessä Scrumin oppeihin huomasin, että kyseiset tapahtumat kuuluvat scrummasterille.

Sinänsä olen pyrkinyt aina parhaimpaan lopputulokseen, kun olen työskennellyt 'täysillä' ja etenkin nyt, kun olen perehtynyt Scrumin oppeihin syventyen. Tosin Scrumin 'leikkiminen' yksin on haaste itsessään ja etenkin silloin, jos asioita pitäisi tehdä ryhmänä, mutta vain muutama osallistuu tekemiseen/ hoitaa asiansa. Kauheasti muutoksia 'en voi' tehdä enää asioihin, joita on tullut tehtyä kurssin ajan, jotta olisin itse tyytyväinen, sillä muutoksien tekeminen vaatisi kaiken aloittamista alusta ja tämä olisi hieman hankalaa tehdä kurssin asiat viikon aikana.

Vahvuuksia omassa työskentelyssä on itsenäisesti asioiden opiskelu ja opitun asian omaksuminen. Olen jatkuvasti arvioimassa omaa tekemistäni ja miettien mitä ja miten olisin voinut tehdä toisin, jotta asiat olisivat paremmin/ onnistuisivat.


### Omien työtehtävien hallinta

- Asiakastapaamiset
    - Näissä olen pyrkinyt parantamaan omaa esitystä jokaisen kerran jälkeen. Selvästi asioiden esiin tuonti on ollut suurin asia, jossa on vielä hiottavaa ja näin olenkin pyrkinyt luomaan selkeän esityksen, jota harjoittelen ennen esitystä.
- Tuoteidea
    - Olen pyrkinyt pitämään sen mahdollisimman selkeänä itselläni, jotta tarpeen tullessa pystyn selittämään, miksi/ mitä/ miten asioiden tulisi olla.
    - Tämän sprintin keskityin ainoastaan tuoteomistajan rooleihin, joten pyrin selventämään entuudestaan tuoteideaa itselleni ja näin ollen priorisoimaan tehtäviä asioita viimeiseen sprinttiin.
- PBL
    - Asiat ovat olleet hieman sekaisin, missä sattuu, mutta tässä sprintissä tarkastelin google.comista, miltä PBL pitäisi näyttää ja tämän pohjalta myös pyrin tekemään ideastani PBL:n.
- SBL
    - Suunnittelin seuraavan sprintin tehtävien pohjalta, enkä toteutettavien asioiden kannalta. Kyseiset tehtävät kävimme läpi yhdessä kehitystiimin kanssa ja päätimme onko ne mahdollista toteuttaa sprintissä.
- Viestintä
    - Mielestäni olen onnistunut vuorovaikutuksessa sekä kehitystiimin että markkinointitiimin kanssa. Itse viestin perille menossa tai kommunikaatiossa ei ole ilmennyt ainakaan itselleni ongelmia. En myöskään ole kuullut muilta, että ongelmia olisi, joten kehittäminen tässä on hieman vaikeaa, voihan olla, että viestintä itselläni on täydellistä, tosin epäilen asiaa hyvin vahvasti.


### Oma viestintä

Olen pyytänyt vähän väliin palautetta/ kommentteja/ huomioita ryhmämme tekemisistä ja oletan, että kukin antaa vapaasti kaikista mahdollisista asioita palautetta, jotka heidän mielestä ovat huonosti toteutettu tai kaipaavat parannusta. Suoraan en ole mitään asiaa erikseen kysellyt, vaan odotan kaiken olevan kunnossa, mikäli itsealoitteisesti asioita ei tuoda esille ja olen nimenomaan tuonut asian ryhmälle selväksi, että mikäli teen asioita huonosti tai niissä on parennetavaa, olisi hyvä jos tuotte asiat julki, niin pyrimme ratkaisemaan ne ja parantamaan työskentelyä jatkossa.

Olen suurimman osan päivästä tavoitettavissa ja mikäli olen koneella tulee myös vastaus lähestulkoon samantien, etenkin silloin jos se on tarkoitettu minulle. Poikkeuksena on, mikäli jäsen, joka ei ole tehnyt juurikaan mitään, kysyy jotain, voi mennä vastauksen kirjoittamisessa pieni tovi.

Asiat tähän mennessä viestinnän suhteen omasta mielestäni ovat menneet hyvin, tästä voi johtua projektimme nopea eteneminen tämän sprintin ajalta, tosin se voi myös olla sattumaa. En ole kokenut omaa toimintaa ongelmalliseksi ja en ole myöskään saanut huomioita asiasta, niin en ole asiaan kiinnittänyt juurikaan huomiota. Tämä voi johtua myös siitä, etten tiedä paremmasta viestinnästä, jolloin parannettavaa ei ole. Muutama sääntö omasta viestinnästä:

1. Pyrin selkeään tekstiin viestissä
2. Mikäli selitys tekstissä omasta mielestäni on sekavaa, pyydän varmistusta asian perille menosta ja tarvitseeko asia lisäselvitystä
3. Mikäli henkilö on 'ansainnut' nopean viestinnän, niin pyrin vastaamaan kysymyksiin mahdollisimman pian
4. Pidän lupaamani asiat
5. Muutoksista tai aikataulu asioista ilmoittelen ja muistuttelen välillä ryhmää


---



### 03.12.2019

Pekan tapaamisesta tuli paljon hyvää tietoa viimeiselle sprintille ja etenkin nyt, kun teemme tarkemmin Scrumin asioita, kuten esim. sprintin suunnittelu, jossa priorisoin asiat, jotka pitää olla valmiita seuraavalle viikolle. Tietenkin asiat olisivat olleet helpompia, mikäli olisin pitänyt kunnollista listaa toiminnallisuuksista ja näin ollen PBL olisi ollut alusta asti toiminnassa kunnolla. Tämän myötä olisi ollut kehitystyökin tarkempaa ja enemmän hallittavissa, sillä viime sprintti oikeastaan meni TODO-listaa katsellessa.

- https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1?path=%2FAproto%2FreadME.md&_a=preview

Tietenkin asioita olisi helpottanut, mikäli olisin pystynyt tekemään tuoteomistajan asioita kurssin alusta alkean ja tietenkin tähän vielä se, että ryhmämme olisi omannut enemmän taitoja asioiden toteuttamiseksi, sillä ryhmässämme vain muutama teki toiminnallisuutta järjestelmään. Toki yhden jäsenen työpanoksen ollessa mitätön, vei myös se resursseja työtekemisestä.

Tarkoituksena tänään sprintin alussa, on pitää sprintin palaveri, jossa katsomme, mitä pystymme tekemään ja sen mukaan luomme SBL:n viimeiselle sprintille. Sinänsä olen suunnitellut viimeisen sprintin asioita siihen malliin, että tärkeimmät asiat, jotka järjestelmästä vielä puuttuu, niin ne tulisi tehtyä vähintään. Tämän jälkeen lisäämme vielä siihen lisää tehtäviä, mikäli taitoa/ aikaa kehitystiimistä löytyy.

- https://gitlab.com/Maici/agile2019/blob/master/Sprinttien_Suunnittelut/Sprintti_5.md

Sprintin suunnittelun jälkeen päädyimme seuraavanlaiseen SBL

- https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_sprints/taskboard/Ryhm%C3%A4%20A_1%20Team/Ryhm%C3%A4%20A_1/Sprint%205

Mikäli työt edistyvät nopeammin, kuin kehitystiimi on ajatellut, voi SBL lisätä muita tehtäviä, tosin jos kehitystiimi koostuu yhdestä henkilöstä, näin ollen on todennäköistä, että jotain asioita saattaa jäädä tekemättä, tehtävien lisäämisestä puhumattakaan.


**REPO kommitit tähän mennessä**

    Commiter { _CommiterName: 'Lauri Hiltunen', _CommitsCount: 79 }
    Commiter { _CommiterName: 'Mika Marjomaa', _CommitsCount: 88 }
    Commiter { _CommiterName: 'Riku Härkönen', _CommitsCount: 48 }
    Commiter { _CommiterName: 'JaniHarkonen', _CommitsCount: 46 }
    Commiter { _CommiterName: 'Valeria Vasylchenko', _CommitsCount: 22 }


Tulen miettimään viimeiset päivät kurssista sitä, miten valmiiden ryhmien jakaminen todennäköisesti sattumanvaraisesti tulee mahdollisesti pilaamaan koko kurssin idean, mikä on oppia uutta ja tässä tapauksessa tehdä se muiden kanssa. Minkä takia ryhmiä ei ole voitu muodostaa siten, jossa käytyjen kurssien arvosanojen mukaan ryhmät jakautuisivat, jolloin oikeasti he ketkä haluavat oppia asian, olisi heidät jaettu omiin ryhmiinsä ja he ketkä ovat osallistumassa kurssille, olisivat he sen sijaan omissa oloissaan pilaamatta päivää muilta.

Sinänsä minulle on aivan sama se, osaako joku jotain vai ei, mutta jos 'osallistuminen' on ainut asia, jota jäsen tekee ryhmässä, on se pienoinen ihmetystä aiheuttava asia itselleni. Taas kurssi, jossa ideana olisi tehdä asioita ryhmänä ja täten oppia uutta, on se ainakin omalta osaltani jäänyt tyystin saavuttamatta. Näin ollen, olisi ollut melkein ihan sama tai jopa parempi, jos kurssin olisi voinut suorittaa itsekseen (miten se sitten tapahtuisi, on minulle mysteeri).

Toinen asia on se, jos kurssille pääse kuka vain, niin miksi tehtävänanto on huomattavasti vaikeampaa, kuin esim. ristinolla pelin tekeminen? Suurin osa ryhmästä ei tuota todellista arvoa itse kehitystyöhön, jonka seurauksena vain osa joutuu tekemään kaiken työn. Vai olisiko tässä minun tuoteomistajana pitänyt tietää, mitä kukin osaa ja tämän myötä tehdä tuoteidea ryhmänä? Sinänsä kysyin useampaan otteeseen, mitä kukin osaa ja sen perusteella meidän kehitystiimissä olisi pitänyt olla jokainen kyväkäs toteuttamaan kyseisen järjestelmän. Olisko minun pitänyt kysellä tarkemmin asioita? Olisiko meidän pitänyt katsoa kaikkien osaaminen läpi juurta jaksaen ja sen jälkeen tehdä tuoteidea huonoiten osaavan taitojen perusteella?

Käytännössä tämän kurssin aiheet ovat olleet minulle vain opiskeltavia asioita, joita tulen hyödyntämään jatkossa tulevaisuudessa, kun teen kehitystyötä, joko itsekseni tai osana oikeaa ryhmää, joka oikeasti haluaa saada aikaan jotain.

Oli miten oli, kiinnostukseni ryhmän toimintaan on minulle aivan sama, tosin asia oli jo hyvin kauan sitten jo itselläni tiedossa. Tosin jossain vaiheessa tulen 'simuloimaan' tämän kurssin itsekseni, tekemällä kehitystyötä Scrum periaatteita noudattaen, tietenkin tämä saattaa olla hankalaa, mutta niin se on ollut tälläkin kurssilla ja eteenpäin olemme päässeet. Joten todennäköisesti ratkaisu siihen kyllä löytyy ja etenkin kun ei tarvitse katsella touhua, mitä tällä kurssilla olen 'joutunut' todistamaan.

Todennäköisesti tulen kirjoittamaan huomennakin samanlaisen rantin asiasta.

___


### 04.12.2019

Ensimmäinen kunnollinen retrospektiivi tuli pidettyä ryhmämme sisällä. Sinänsä mielenkiintoinen tapahtuma ja olisi todellakin ollut mielenkiintoista tehdä kehitystyö noudattaen tarkasti Scrumin periaatteita.

- Scrum-masterin pitämä
    - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/213/Sprinttien-retrospektiivit

LIKOn kanssa laitoimme loppuesityksen aluille ja jatkamme sitä huomenna, kun saan tiedon siitä, ketkä meidän puolelta osallistuu esityksen tekoon ja mahdollisesti esitykseen. Ideana olisi tehdä yhtenäinen esitys, jossa ensin esitellään tuote ja sen jälkeen jaetaan markkinointiasioita. Tämän jälkeen olisi ominaisuuksien esittely tuotteesta, jonka jälkeen mahdollisesti lisää markkinointihommia ja lopuksi molemmilta lopussa yhteenveto.

Päivä päivältä tullut katseltua itsenäisesti Scrumin asioita ja siinä samalla syttynyt halu toteuttaa ohjelmisto näitä periaatteita käyttäen. Esimerkiksi eilen pidetyssä sprintin retrospektiivissä kävimme läpi asioita, mitkä onnistuivat ja epäonnistuivat viime sprintissä, jonka pohjalta olisi mielenkiintoista nähdä seuraavan sprintin osalta, miten asiat voisivat muuttua ryhmän sisällä. Sinänsä harmi, että ensimmäinen kokemus Scrumista oli itselleni pettymys, toisaalta olen huomannut useassa asiassa sen, mikäli ensimmäisen kerran asioita tekee suunnilleen tai totaalisen väärin, niin tapahtumasta saa yleensä huomattavasti enemmän irti oppimisen kannalta, kuten tällä kurssilla on käynyt. Mikäli tulen jossain vaiheessa elämääni toteuttamaan ohjelmistoa Scrum menetelmää käyttäen, niin olen ainakin tietoinen riskeistä, joita tällä kurssilla on tullut huomattua ryhmästämme. Seuraavan kerran, kun olen ryhmätyössä, niin todennäköisesti en odota keneltäkään enään yhtään mitään. Tiedustelen jäseniltä ketkä ovat tulleet oppimaan/ tekemään asioita, jonka jälkeen keskitän työni tekemiseni heidän kanssa toimimiseen.

Vieläkin suurta ihmetystä aiheuttaa kehitystiimin jäsenten taitojen 'puutteellisuus' kurssin tehtävänannon kannalta. Mikäli tällä kurssilla ainut tarkoitus on opetella Scrum periaatteita ja kuinka niitä noudattaen olisi 'helpompi' rakentaa sovelluksia, niin siinä tapauksessa kurssin tehtävänanto olisi omasta mielestäni pitänyt olla aivan toisenlainen, tai edes vähintään ryhmien muodostaminen tavalla, jossa taitoeroa ei ryhmän jäsenten välillä juurikaan ole. Kuten ryhmässämmekin tällä hetkellä vain yksi jäsen toteuttaa toiminnallisuutta järjestelmään, jonka seurauksena suurin osa ryhmästä jää tehtäviä vailla. Mikä asiasta tekee vielä vaikeampaa on se, että priorisoinnissa uudet toiminnallisuudet tai koodin korjaaminen jää näin ollen ainoastaan yhden henkilön vastuulle, jonka seurauksena tuotteen toiminnallisuus jää huomattavasti jälkeen siitä, mitä sen 'olisi' pitänyt olla.

jatkuu...

100k merkkejä tuli täyteen

https://gitlab.com/Maici/agile2019/blob/master/BlogiBackupOSA2.md

