



# Ryhmän ja ryhmän jäsenten arviointi


### Commitien määrä REPOon

    Commiter { _CommiterName: 'JaniHarkonen', _CommitsCount: 58 },
    Commiter { _CommiterName: 'Lauri Hiltunen', _CommitsCount: 90 },
    Commiter { _CommiterName: 'Antti Salo', _CommitsCount: 2 },
    Commiter { _CommiterName: 'Mika Marjomaa', _CommitsCount: 90 },
    Commiter { _CommiterName: 'Valeria Vasylchenko', _CommitsCount: 23 },
    Commiter { _CommiterName: 'Riku Härkönen', _CommitsCount: 48 }

- https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1/commits

### Työtunnit (07.12.2019)

    Jani Harkonen           93h
    Mika Marjomaa           88h
    Lauri Hiltunen          64h
    Valeria Vasylchenko     63h
    Riku Härkönen           47h
    Antti Salo              6h

- Muiden työtunneista en tiedä, mutta omat tunnit ovat vain niitä, jotka ovat tuoneet arvoa projektissamme.

![](https://i.imgur.com/ijiEfMl.png)


## Huomioitavaa

- Mikäli Antti kurssista saa arvostelun, oli se sitten hylätty tai läpäisty numerolla, näin ollen ryhmän muut jäsenet ansaitsevat automaattisesti 5 kurssista arvosanaksi.
    - Hänen työpanoksensa kurssilla oli lähestulkoon olematon.
        - Suunnitelutehtäviä kurssin alussa hän ei tehnyt yhtään, paitsi yhden.
        - Kehitystyöhön hän ei ole tuonut mitään arvoa, kuten kommitien määrästä sen näkee.
        - 6 tuntia hänen työstään on tullut tehtyä viimeisellä sprintillä ja olettaisin, että nämäkin ovat tulleet clockfyin päällä pitämisestä tai kurssiblogin kirjoittamisesta.

- Omasta mielestäni ainakin itse epäonnistuin kurssilla, jolloin itse kurssin asiat tuli opiskeltua näiden blogikirjoituksien avulla.
    - Olisi ollut mielenkiintoista nähdä, miten oikeasti kehitystiimi toimii.

- Suurin osa ryhmästä ei omannut minkäänlaista itsealoitteisuutta.
    - jonka seurauksena ryhmässä työskentely oli hyvin raskasta.

- Suurin osa ryhmästä ei myöskään omannut minkäänlaisia ohjelmointitaitoja, jotka olisivat auttanut ryhmää
    - Näin ollen kaikki vaativampi työ jäi ensisijaisesti Janin sekä minun tehtäväksi.
    - Pidän outona, miksi he eivät hakeneet apua opettajilta koulutus mielessä.

- Puhtaasti rahallisesti ajateltuna ryhmän kokoonpano olisi ollut todella huono tuomaan arvoa kulutettuihin kolikkoihin nähden. Onneksi palkat kumminkin olivat ryhmän sisällä pieniä, joten sinänsä vahinkoa ei aiheutettu.

- Oppimisen kannalta en voi puhua, kuin omasta puolestani, joka käytännössä oli se, kun näin miten asiat toimivat ryhmässä, jossa itsealoitteisuutta itseopiskeluun ei näyttänyt juuri olevan yhtään ja kuinka muut jäsenet vetivät rooliaan ryhmässämme. Näin ollen tarkkailin ryhmämme toimia ja pohdin kurssiblogissani, miten asiat pääsivät menemään näin ja kuinka asioita olisi voinut tehdä toisin.


## Ryhmän toiminnan arvionti

- Ryhmä A_1

    - **Arvosana**: 0- 3

    - **Perustelut**: Oma-aloitteisuuden puutos ryhmässä, teki ryhmässä työskentelyn todella raskaaksi. Tähän lisättynä ohjelmointitaitojen puutteet, olisi ollut aivan sama tehdä koko kurssi yksin tai Janin kanssa. Itse aloitteisesti tiedon etsiminen ja Scrumin periaatteiden oppiminen jäi tödennäköisesti monelta alussa tekemättä ja osa tuskin niitä koskaan katsoikaan. Mahdollisuus avun saantiin ohjelmointiin opettajilta ryhmässä jäi kokonaan käyttämättä ja näin ollen työmäärääkin osalta ryhmästä oli todella pieni, sillä suurin osa ei omannut minkäänlaisia ohjelmointitaitoja. Minkäänlaista aikomustakaan ei ollut tehdä vähemmän vaativimpia tehtäviä heidän osalta, jotka eivät osanneet ohjelmoida.

    - Kehitystyön perusteella, eli taitojen mukaan, ei ryhmässä työskentelyssä ollut mitään järkeä, sillä kehitystiimi koostui yhdestä henkilöstä. Tämän myötä järjestelmäkin on sen laatuinen.

    - Scrumin periaatteita noudatimme suurimmilta osin, tosin suurin osa asioista oli minun aloittamiani.

    - Noin puolet ryhmästä oli aktiivisesti tekemässä asioita ja pohtimassa yhdessä mitä teemme ja miten parannetaan jatkoa miettien.

    - Tuoteidean olisi pitänyt olla huomattavasti pienempi kuin, mitä se oli, jos olisimme tienneet kehitystiimin taidot.

    - Minkäänlaista oma-aloitteisuutta suurimmalta osalta ryhmästä ei löytynyt.

    - Osasin odottaa, että ryhmässä työskentely tulee olemaan 'mielenkiintoista', mutta sen, mitä tällä kurssilla olen todistanut ryhmässämme, sai minut miettimään hyvin tarkasti sitä, miten tulen jatkossa suhtautumaan ryhmätöihin tämän 'koulutuksen' aikana.


## Ryhmän jäsenten arviointi

Arviot perustuvat jäsenen tuomaan arvoon ryhmälle. Otan huomiion asiat, kuten tehdyt tehtävät/ työt, taidot, roolin esitys ja kyky toimia ryhmässä.

---
- **Mika**
    - **Arvosana**: 0- 5

    - **Perustelut**: Arvosanaa on itselleni hyvin vaikea antaa, sillä monet asiat olisi minun pitänyt tehdä toisin. Kurssin alussa ryhmän jättäminen heitteille kahdeksi viikoksi, oli huono asia ja näin ollen katosi ryhmästämme aikaa toteuttaa asioita. Toisaalta kukaan ei miettynyt missäköhän mennään noina kahtena viikkona.

    - Työntekeminen
        - Noudatin ryhmämme käytänteitä
            - työtunnit merkattu ylös
            - kokouksiin osallistuminen
        - Pistin kehitystyön alulle
            - toteutin kurssin alussa esimerkki toteutuksia järjestelmästä
            - osallistuin myös kehitystyöhön
        - Asiakastapaamiset
        - Ohjauspalavereiden puheenjohtajuus x5 (kaikki)
        - Ryhmän asioiden hoitaminen
        - Viestintä tiimin ja sidosryhmien välillä
        - Sprinttien suunnittelu

    - Itsealoitteisuus
        - Pistin suurimman osan asioista ryhmämme sisällä käyntiin
            - Otin asioita esille
                - pyrin etsimään vapaaehtoisia asioihin
                    - ohjauspalaverit sekä muut ryhmää koskevat asiat
                        - mikäli henkilöitä ei löytynyt, niin tein asiat itse
        - 'vahingossa' tuli tehtyä scrummasterin asioita
            - Daily Scrumit
            - Sprintin retrospektiivit

    - Alta löytyvät omat työt/ tehtävät
        - noin puolet kommiteista bugien korjauksia/ 'hot fixejä'

    - https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1/commits?user=Mika%20Marjomaa&userId=61fd4bcb-cb25-6311-9036-cef75664cad3

    - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_workitems/recentlycreated/

    - Tein asioita kurssin aikana jokaisesta roolista. Otin ryhmän johtajuuden itselleni, sillä kukaan muu sitä ei halunnut kurssin alussa. Pyrin olemaan aktiivinen työskentelyssä ja samalla koittaen saada ryhmämme toimimaan paremmin, mutta ongelmakohtia ei voinut välttää ja niitä tuli esiin yhä enemmän kurssia edetessä. Kaikki asiat olen nyt käynyt läpi ja olisin nyt valmis olemaan projektissa tuoteomistaja, sillä olen perillä siitä, mitä asioita kannattaa tehdä ja mitä varoa. Jos ryhmä olisi ollut saman henkisempi itseni kanssa, olisin näin ollen tehnyt myös ryhmän asioita enemmän/ paremmin. Omalla kohdalla katsoisin itseni enemmän tuloksi silloin, kun tein asioita aktiivisesti ja menoksi, kun lopetin aktiivisen toimimisen.

---
- **Jani**
    - **Arvosana**: 5++

    - **Perustelut**: Itsealoitteisuutta työnteossa sekä uuden oppimisessa. Teki huomattavan määrän töitä ryhmän keskiarvoon nähden (arvon kannalta). Omaa mahtavat ohjelmointitaidot, mikäli kehitystiimissä olisi ollut neljä Jania, niin olisimme voineet toteuttaa huomattavasti paremman järjestelmän. Arvosana-asteikkoa ei Janin kohdalla voida käyttää, sillä jos häntä ei olisi ollut ryhmässämme, niin lopputulos olisi ollut hyvin erilainen.

    - Työntekeminen
        - Toteutti järjestelmän yli 50%
        - Korjasi bugeista 90%
        - Uuden oppiminen ei näyttänyt olevan este hänelle
        - Itsealoitteisesti katsoi työtehtäviä
            - toteutti suuren osan järjestelmästä
        - Ryhmän käytäntöjen noudattaminen
            - työtuntejen merkkaus ylös
            - Toi asioita esille Daily Scrumeissa
                - ongelmat
                - tehdyt tehtävänäsä

    - Itsealoitteisuus
        - Ilmoittautui kirjuriksi kokouksiin
        - asioiden miettiminen itsekseen
            - toi asioita julki ryhmälle

    - alta löytyvät hänen työt/ tehtävät

    - https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1/commits?user=jani&userId=

    - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_workitems/recentlycreated/

    - Omaa kaikki ominaisuudet, jotka omasta mielestäni erinomaiselta työntekijältä vaaditaan. Itsealoitteisesti teki asioita, jonka seurauksena projektimme eteni. Toi asioita julki ryhmälle, jotta muut pääsivät asioiden tasalle. Omaa mahtavat ohjelmointitaidot. Ryhmän kannalta hän oli todellakin enemmän kuin tulo, hän toi huomattavasti arvoa projektillemme.

---
- **Lauri**
    - **Arvosana**: 2- 4

    - **Perustelut**: Teki jatkuvasti töitä/ tehtäviä. Kommunikoi eniten koko ryhmästä, toi esiin asioita Daily Scrumeissa ja kokouksissa. Ohjelmointitaitoja ei sinänsä kauheasti löytynyt, mutta pystyi tuottamaan jotain pieniä muutoksia käyttöliittymään, kuten tyylitystä (css). Omasi eniten itsealoitteisuutta ja vastuun kantamista ryhmässä. Avoin henkilö, jonka kanssa oli helppo työskennellä.

    - Työntekeminen
        - Itsealoitteisuutta
            - ilmoittautui ohjauspalavereihin/ asiakastapaamisiin kirjurina
            - teki töitä itsenäisesti
            - toi asioita esille kokouksissa
            - huolehti osittain wikistä
        - Piti kiinni ryhmämme käytänteistä
            - Työtunnit merkattu ylös
            - ongelmien tuonti esiin
            - Teki jatkuvasti töitä
                - vaikka ohjelmointitaitoa ei sinänsä löytynyt
                    - etsi hän omatoimisesti tehtäviä
        - Ei hakenut apua ohjelmointiin opettajilta

    - alta löytyvät hänen tehtävät/ työt
        - suurin osa 'hot fixejä'

    - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_workitems/recentlycreated/

    - https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1/commits?user=Lauri%20Hiltunen&userId=a108e7f0-7dfb-68aa-88de-f5e7697f1a7c

    - Erinomainen työntekijä, mikäli omaisi paremmat ohjelmointitaidot, tosin itsealoitteisuutta henkilöltä löytyy, joten ohjelmointitaitoja voi hieman katsoa läpi sormien, toki olisi auttanut, jos kehitystiimissä olisi ollut enemmänkin, kuin yksi henkilö. Hän toi ryhmälle arvoa ja täten enemmän tulo kuin meno, jos ajatellaan markkina mielessä.

---
- **Valeria**
    - **Arvosana**: 1- 3

    - **Perustelut**: Teki huomattavasti enemmän töitä suunnitteluvaiheessa, kuin muut. Ei omannut taitoja ohjelmointiin ja itsealoitteisuudessa olisi ollut huomattavasti parannettavaa. Ei ilmoittautunut vähemmän vaativiin tehtäviin. Toimi scrummasterina, tosin alkoi vasta tekemään asioita, jotka hänelle kuului vasta sitten, kun itse opin uusia Scrum oppeja ja näin ollen informoin häntä niistä.

    - Työntekeminen
        - Piti kiinni ryhmän käytänteistä
            - Merkitsi ylös työmäärät
            - Toi asoita esille
            - Teki töitä, lähinnä suunnittelu mielessä
        - Ei omannut ohjelmointitaitoja
            - Ei myöskään hakenut opettajilta apua. (ei ole ainakaan omassa tiedossani)

    - Scrummasterina toiminen
        - Kokonaisuudessa heikko esitys
            - Alkoi tekemään asioita vasta, kun itse ne opin ja näin ollen kerroin hänelle niistä.
        - Daily Scrumit
            - Onnistuivat hyvin, asioita tuli esille
                - Oli poissa kerran ilmoittamatta
                    - Onneksi olin pitänyt Daily Scrumeja kaksi ensimmäistä viikkoa, joten otin asian haltuun sinä päivänä.
        - Retrospektiivit
            - Piti viimeisen, joka onnistui sekin hyvin
                - Näin ollen olisi ollut mielenkiintoista nähdä, miten ryhmämme olisi toiminut, jos sprinttejä olisi vielä jäljellä.

    - Itsealoitteisuus
        - Suunnittelutehtäviä hän teki ensimmäisten sprinttien aikana
        - Toi jotain asioita esille tapaamisissa
        - Todennäköisesti ei katsonut/ opiskellut Scrumin periaatteita
        - Ei ilmoittautunut vähemmän vaativiin tehtäviin
        - ei osallistunut loppuesityksen tekoon
        - yms. asioita, joissa ohjelmointitaitoja ei olisi tarvinnut

    - Alla hänen tekemät tehtävät/ työt
        - Suurin osa sijoittuu kurssin alkuun, jolloin suunnittelimme tuoteideaa.

    - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_backlogs/backlog/Ryhm%C3%A4%20A_1%20Team/Epics/?showParents=true

    - https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1/commits?user=Valeria%20Vasylchenko&userId=9874384f-4a12-6597-a2e8-3b0a4bec2853

    - Scrummasterin näkökulmasta omasta mielestäni hänellä olisi voinut olla paljon parannettavaa. Hän teki paljon töitä suunnitteluvaiheessa, mutta sinne ne oikeastaan jäikin, sillä ohjelmointia hän ei niinkään osannut. Arvoa hän ei oikeastaan tuonut muuten, kuin suunnitteluillaan kurssin alussa. Tämän jälkeen hänestä ei paljoa apua ollut ryhmässä. Näin ollen hän olisi ollut enemmän kulu kuin tulo markkina mielessä.

---
- **Riku**
    - **Arvosana**: 1- 3

    - **Perustelut**: Ohjelmointitaitoja hän ei omannut juurikaan, tosin hän yritti tehdä tietokannan, mutta sekin jäi ainakin omasta mielestäni yrityksen tasolle. Oma-aloitteisuutta olisi hänellä voinut olla huomattavasti enemmän ja näin ollen korvannut puutteellisia ohjelmointitaitoja tekemällä ryhmän muita vähemmän vaatia asioita.

    - Työntekeminen
        - Kommitteja häneltä löytyy, mutta useammat niistä ovat 'hot fixejä'.
        - Teki tietokannan järjestelmään
            - Piti yllä tietokannan rakennetta
                - teki aina tarvittavat muutokset
        - Työtunnit verrattuna hänen tuottamaan arvoon ryhmälle on pieni.
            - käytännössä, jos olisimme oikea yritys, olisi hän enemmän kulu kuin tulo
        - Noudatti ryhmän käytäntöjä huolellisesti
            - Ajat merkattu ylös clockifyihin
            - Teki töitä
        - Muutaman kerran jätti tulematta Daily Scrumeihin
            - ei ilmoittanut

    - Daily Scrum
        - Toi esiin tehtyjään asiota
            - Tosin liittyivät aina tietokantaan.

    - Itsealoitteisuus
        - Suunnittelu vaiheessa valitsi tehtäviä ja myös teki niitä.
        - Ei ottanut oikeastaan muita tehtäviä itselleen.
            - ohjauspalavereiden puheenjohtajuus/ kirjuri
            - ei osallistunut loppuesityksen tekoon
            - yms. asioita, joissa ohjelmointitaitoja ei olisi tarvinnut

    - Ei hakenut apua ohjelmointiin opettajilta

    - Alla hänen tekemät työt/ tehtävät

    - https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1/commits?user=Riku%20H%C3%A4rk%C3%B6nen&userId=4a3d7b16-94c1-6857-afa7-b1565c807f77

    - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_workitems/recentlycreated/

    - Enemmän kuin osallistuja kurssilla, mutta ei kumminkaan arvontuoja ryhmälle.
        - Jos katsotaan ryhmämme toimintaa raha näkökulmasta.


---
- **Antti**
    - **Arvosana**: Ei arviontia

    - **Perustelut**: Ei osallistunut ryhmätyön tekemiseen juuri ollenkaan. Näin ollen olisi ollut aivan sama olisiko hän ollut mukana ryhmässämme vai ei. Ei vastuun kantamisen taitoa. Ei kiinnostusta ryhmässä työskentelyssä. Ei tehnyt mitään asioita kurssin aikana, joista olisi ollut edes hieman apua ryhmässä.

    - Alla listattu hänen tekemät tehtävät taskeittain yksittäisinä linkkeinä... Muuta hän ei tehnytkään ryhmässä.

    - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_git/Ryhm%C3%A4%20A_1/commit/580d26dc4d2e932711a8c5f3da34243c3c9584e9/
    - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_git/Ryhm%C3%A4%20A_1/commit/de832971ed9e319fc620abcf8d4db48991763dd7/
    - https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_workitems/edit/316/

    - Ei osallistunut mihinkään, mikä olisi auttanut ryhmämme etenemiseen.
        - Jos ohjelmointia hän ei osannut, olisi hän voinut tehdä muita tehtäviä, kuten:
            - dokumentaatiota
            - testausta
            - työn esittelyn tekemistä
            - wikin päivitystä
            - ohjauspalavereiden pitäminen
            - suunnittelua
            - scrummasterina toiminen
            - tai muuta, mikä ei olisi vaatinut ohjelmointitaitoja...
            - avun pyyntäminen opettajilta ohjelmointiin

    - Näistä tehtävistä hän ei tehnyt mitään ja kuten yllä listatut kolme taskia häneltä, olivat ne ainoat asiat, joita hän teki ryhmässämme.

    - Itsealoitteisuuden puutos.
        - Ei tuonnut 'ongelmia' esille
            - Mikäli hän toi niitä ankaran kyselyn jälkeen, ei hän pystynyt antamaan kongreettisia asioita, joita olisi voinut/ pitänyt parantaa
        - Ei osallistunut mihinkään itsealoitteisesti
            - Töistä hän ei valinnut mitään.
                - Näin ollen hänelle piti niitä määrätä
                    - näitä hän teki spritin ajan (yhtä tehtävää), jonka jälkeen sanoi, että ei voinut tehdä, kun tuli 'ongelmia'.
        - Työpanos
            - Daily Scrumeissa ei hänellä ollut koskaan mitään sanottavaa
                - Mikäli oli, hän piti huolen siitä, että asia tuli ilmi
            - Mihinkään vähemmän taitoja vaativiin tehtäviin hän ei koskaan ilmoittaunut
                - Tilaisuuksia hänellä oli useita näihin ilmoittautua, sillä olen aktiivisesti kysellyt ryhmältä asioille tekijöitä.
            - Ryhmän sopimien käytäntöjen rikkominen
                - Ei käyttänyt REPOa
                    - Jakoi tiedostoja yksityisviestien välityksellä
                - Ei tehnyt töitä
                - Ei merkannut työtunteja ylös
                    - Tosin ei hänellä niitä olisi ollutkaan
                - Ei tuonut mitään arvoa ryhmällemme
                    - Käytännössä hänen panos ryhmässämme oli osallistuminen kurssille

    - Ainut asia, jonka hän teki, oli poissaoloista ilmoittaminen

    - 10.12.2019
    - ![](https://i.imgur.com/a0ywwE0.png)

    - Jos hän kurssista saa arvioinnin, haluaisin enemmän kuin mielelläni kuulla perusteet arviontiin. Voihan olla, että hän tietää kurssin asiat, jos näin oli, ei hän niistä mitään näyttänyt ryhmässä 'työskennellessään'. Ja mikäli häneltä löytyy enemmän työtehtäviä tehtynä, niin niistä en ainakaan itse ole tietoinen, sillä olen ollut istumassa jokaisen Daily Scrumin (paitsi yhden).






