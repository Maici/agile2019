


# 03.12.2019 - Pekan tapaaminen tuotteen esittely


[HUOMIOITA]

- Osa sivuista kesken

- Toiminnallisuudet lähestulkoon valmiit
	- Jotain ominaisuuksia saattaa joutua pudottamaan

- Navigoinnissa paljon parantamista
	- esim. rakennuksen kohdalta pääsisi 'rakennuksen valvonta' sivulle katsomaan kyseistä rakennusta




[OMA_PROFIILI_/_ALOITUSSIVU]

- Hakee omat tiedot

	- Kiinteistöryhmät
		- Kiinteistöt
			- Hälytykset
				- Analyysit

- Rakennuksen pohjapiirrustus tai muuta dataa (käyttäjän valitsemaa)
				
[HÄLYTYKSET]

- Hälytykset
	- Valikko
		aktiiviset, menneet tai kaikki

- Sensoreille viitearvojen asettaminen automatisoidun valvonnan tueksi
	1. Hae sensori
	2. Lisää viitearvot


[SENSORIDATAN_VERTAILU]

- Valitse sensorit
- Rajaa päivämäärän mukaan


[RAPORTIT_/_ANALYYSIT]

- Palvelimen tai käyttäjän luomia
- Palvelin luo automaattisesti hälytyksen yhteydessä
- Käyttäjä voi luoda raportin, jonka voi exportata eri muodoissa ulos, esim. excel


[TIETOKANNAN_HALLINTA]

- Tärkeimmät asiat esillä
	- kiinteistöryhmät, kiinteistöt, huoneet, sensorit

- Lisättävä loput taulut tietokannasta täydellisen hallinnan mahdollistamiseen

- Suuremmasta kokonaisuudesta pienempään

- Suodatin toiminnot
	- Sarakkeiden piilottaminen
	- Sarakehaku

- Tietueen muokkaaminen sekä lisääminen
	- poistaminen puuttuu


[RAKENNUKSEN_VALVONTA]

- Valitse rakennus

- Rakennuksen kaiken tiedon listaaminen
	- huoneet, sensorit, hälytykset

- Rakennuksen pohjapiirrustus puuttuu
	- sensoreiden paikat


[2D_PIIRTÄJÄ]

- Rakennuksen pohjapiirrustuksen tekemiseen

- Sensoreiden lisääminen/ muokkaaminen



