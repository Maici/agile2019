



[INTRO]

* Rakennuksissa sensoreita

* Rakennuskohtainen palvelin, joka kerää datan
    => lähettää sen pääpalvelimelle
        => tekee tarkistuksia arvoille

* Tieto tallennetaan, jota voi halutessaan tarkkailla


* Työpöytä version prototyyppi seuraavaksi

* mobiili ja työpöytä versio erikseen
    => keskitymme aluksi työpöytä versioon


1. Dia [KIRJAUTUMINEN]

    * Käyttöliittymä on web-sovellus

    * Kirjaudutaan järjestelmään


2. Dia [ONNISTUNUT_KIRJAUTUMINEN]

    * Valittavissa joko mobiili/ työpöytä käyttöliittymä

    * Hiomis vaiheessa mahdollisesti tarkistetaan minkälainen laite kyseessä ja sen mukainen käyttöliittymä avataan


3. Dia [ALOITUS_SIVU]

    * Yläpalkki

    * Työntekijän profiili

        * Näkee kokonaistilanetta omalla alueella (omat rakennukset ja mahdolliset hälytykset)

        * Alhaalla hälytykset listattu ongelma kohtineen


        //* Kukin henkiökunnan jäsen voi profiiliansa muokkailla


4. Dia [KIINTEISTÖN_HALLINTA]

    * Kiinteistöryhmät lajiteltu vasemmalle
        * Tässä voi tehdä omia ketegorioita
            * Kuten esim. omat kiinteistöryhmät

    * Kiinteistöryhmän valitessa
        => aukeaa kiinteistö valikko

            => Kiinteistön tiedot

                => sensorit oikeaan puoleen riviin

                    => mahdollisesti rakennuksen pohjapiirrustus alle

                        => johon sensorit näkyviin


5. Dia [TIEDON_TARKKAILU]

    * Valikko muokattaville asioille (työntekijät, rakennukset, sensorit)

    * Katselu ainoastaan, ei muokkausoikeuksia


6. Dia [TIEDON_MUOKKAAMINEN]

    * Jokaisen rivin edessä valintaruutu

    * jossain muualla työkalupalkki, josta voi haluamansa toiminnon suorittaa
        => kuten poistaa, muokata, lisätä

    * Tätä sapluuna voitaisiin käyttää muidenkin hallittavien asioiden hallitsemiseen.


7. Dia [SENSOREIDEN_HALLINTA]

    * Hyvä pohja sensoireiden arvojen vertailuun

    * Hakukenttä jolla rajataan sensoreita näytettäväksi
        => valittujen sensoreiden datan historian näyttö oikealla 2d/ 3d muodossa
            => mitä nyt keksitään


8. Dia [TURHA_DIA?]

    * 7. Dian asioita


9. Dia [MENUT]

    * perusidea menuista





