



# Aloitussivu

- Käyttäjälle mahdollisesti valtaa valita mitä sivlla näkyy

	- erilaiset widgetit
	- rakennuksen tarkkaileminen 2D muodossa
		- demo lopussa
	
- Hälytykset

	- Ongelma tilanteissa luodaan automaattisesti raportti
		- jota käyttäjä pystyy sitten
			- katsomaan
			- tulostamaan
			- lähettää eteenpäin sähköposti
	
	
- Tietojen hakeminen

	- Käyttäjä pystyy suodattamaan haettavia kohteita
		- suodatin asetukset voi tallentaa, joita hyödyntää tulevaisuudessa
		
	- vasemmalta oikella
		- tarkennetaan hakua kokoajan
		
	
- Hakutulokset

	- Listataan käyttäjälle suodattimia vastaavat tulokset


- Tietueiden hallinta

	- valitaan ensimmäiseksi haluttu kategoria
	
	- listataan kaikki tietueet, tässä mahdollisesti myös suodatin mahdollisuus
	
	- Jokaisen rivin edessä valinta ruutu
		- valitaan halutut rivit
	- ja suoritetaan tominto, esim. poistetaan valitut kohteet tai muokataan


- Sensoridatan vertaileminen

	- käyttäjä pystyy suodattamaan haluamansa sensorit
		- esim. tietyt lämpötila sensorit eri rakennuksista
		
	- ajan mukaan pystyy sensoridataa säätämään näytölle
		- sitä mukaan piirretään datan historiaa näytölle
		



# Sensori esittely

- Ulko- ja sisälämpötila
    - lämpötilat vaikuttaa keskuslämmitykseen
- Hiilidioksidipitoisuus
    - voidaan kytkeä automaattisesti säätämään ilmanvaihtoa

## Sensorit, joita harkitaan
- Ilmanpaine
    - näkee miten ilmanvaihto toimii
- Ilmankosteus
    - voi viestiä ilmanlaadusta
- Sähkönkulutus
    - kokokulutus, rajaus tiettyihin osiin (esim. valaistuspisteet)
	ja paljon sähköä vaativiin laitteisiin (esim. keittiön uunit)
- Sähköntuotto
    - tuulivoima ja aurinkopaneelit
- Vedenkulutus
    - mahdollisesti rajata kylmä/lämmin vesi tai huoneittain
- Liiketunnistimet
    - hallitsee valaistusta automaattisesti
    - voidaan käyttää hanoissa hallitsemaan veden kulutusta
- Äänenvoimakkuuden mittari
	- Sen avulla seurataan melun arvoja kiinteistöissä.
	Jos arvo nousee yli sallitun rajan, järjestelmä ilmoittaa siitä.
- Ilmanlaadunmittari
	- Sen avulla mitataan kuinka saastunut ilma on.
	Mittari voi olla erityisen hyödyllinen teollisuusalueilla.
- Murtohälytys
	- Ilmaisimen avulla pystyy havaitsemaan poikkeamia ja välittämään tietoa poikkeamista esim.
	hätäkeskukseen.
- Avoimen oven seuranta: miten usein mitäkin ovia käytetään
    - Auttaa hahmottamaan miten ihmiset liikkuu kiinteistön sisällä.
	
	



# KYSYMYKSET

Minkä tyyppisiä rakennuksia Joensuulla on


