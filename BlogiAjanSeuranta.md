

# Ajanseuranta omista työtehtävistä
### Kurssin alusta - 06.12.2019

![](https://i.imgur.com/Ef7DFpk.png)


Alla lajiteltu oma ajankäyttöni. Kaikkia asioita en ole ajastanut ylös. Nämä asiat ovat mm. sellaisia, jotka eivät sinänsä ryhmän projektia ole viennyt eteenpäin, kuten Scrumin opiskelu ja kurssiblogin kirjoittaminen. Joitain merkintöjä clockifyista puuttuu, mutta kun siellä ne eivät ole ylhäällä, niin niitä ei ole koskaan tapahtunut.

- https://clockify.me/bookmarks/5dedef5e6afcf51b014346ce

- ohjauspalaverin asialista (4h)
    - 0:30
    - 1:30
    - 0:50
    - 1:30

- Wiki (2h)
    - 1:15
    - 0:40

- Kehitystyö (30h)
    - 19:00
    - 8:50
    - 0:45
    - 6:20
    - 1:00
    - 2:50
    - 0:35

- Daily Scrum (suurin osa ei merkittynä ylös) (4h)
    - 1:10
    - 1:50
    - 0:10
    - 1:00

- Ryhmän asioita (2h)
    - 0:20
    - 0:05
    - 0:50
    - 0:30

- Daily Scrum/ kokoukset + asialistat (8h)
    - 3:00
    - 3:10
    - 1:55

- LIKO viestintä (suurin osa ei merkittynä ylös) (1h)
    - 1:20

- Tunnilla olot (suurin osa ei merkittynä ylös) (2h)
    - 0:30
    - 1:20
    - 0:20

- Sprinttien suunnittelu + PBL (13h)
    - 4:10
    - 3:40
    - 0:55
    - 2:30
    - 1:05
    - 0:30

- Sprinttien retrospektiivit (1h)
    - 0:05
    - 0:30
    - 0:30

- Tuoteidean suunnittelu (5h)
    - 3:00
    - 0:10
    - 1:35
    - 0:15

- Testaaminen (suurin osa ei merkittynä ylös) (1h)
    - 0:25

- Pekan tapaamiset + niihin valmistautumiset (5h)
    - 0:05
    - 2:00
    - 1:15
    - 1:10

