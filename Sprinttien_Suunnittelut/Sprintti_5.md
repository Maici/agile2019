

- Ryhmän jäsenten taidot
https://hackmd.io/rYSZQjRCQ6W0_MIABoCuRQ?view


- Jani
    - Kaikki onnistuu

- Lauri
    - Käyttöohjeiden tekeminen

- Riku
    - Testaaminen

- Valeria
    - Testaaminen?


- Tee näin:
    - Osallistu kokoukseen
        - Valmistaudu
    - Tehtävien priorisointi
    - PBO:n asioiden oltava selkeitä
        - vähentää ongelmatilanteita
    - Vastaa kysymyksiin/ selvennä ongelmissa päivän loppuun asti


# Päätavoite

- Järjestelmän toiminnallisuudet valmiiksi



# Sprintti #5 suunnittelu - 03.12.2019

- Tärkeät tehtävät - Prioriteetti #1

    - 2D pohjapiirrustus näkymän lisääminen sovellukseen
        - #937  (pohjapiirrustuksen luominen) [OK]
        - #932  (muokkaaminen) [OK]

    - 2D pohjapiirrustus näkymä rakennuksen valvontasivulle
        - #918  (huoneiden piirot) [OK]
        - #917  (sensoreiden piirto) [OK]

    - Rakennuksen valvontasivun viimeistely 
        - #920  (asioiden listaaminen) [OK]
        - #921  (asioiden linkittäminen lisäämään käytettävyyttä) [OK]

    - Käyttöliittymän muokkaaminen käytettäväksi!
        - elementtien asettelua [OK]
        - tyylitys [OK]

    - Suomenkieli kuntoon [OK]

    - Automatisointia
        - #942  (visuaalinen viestijärjestelmä) [OK]

    - Käytettävyyden kannalta asioita
        - #913  (linkkien lisääminen) [OK]
        - #905  (varmistus palvelimelta käyttäjälle pyynnön lopputilasta) [OK]
        - #845  (taulujen yhdisteleminen -> ID:llä pelleily loppuu) [OK]

    - Hälytyssivu
        - #929  (hälytyksien lajittelu, menneet, aktiiviset, kaikki) [OK]
        - #931  (uusien viitearvojen asettaminen) [OK]
        - #1029 (hälytyksien hallinta tietokannan hallintasivulla) [OK]

---
- Prioriteetti #2

    - Sensoridatan vertailu
        - #1021 (viimeisen kuukauden ajalta datan piirtäminen)

    - Automatisointia
        - #941  (uusien hälytyksien hakeminen)

    - Tietokannan hallintasivu
        - #946  (sivuttaminen)

    - Sensoridatan vertailu
        - #960  (värisokeus, väripaletin vaihtaminen)
        - #1022 (näytä sensoridataa viime viikko-, viime kuukausi-, viime vuosi- painikkeita)

    - Raportti
        - #952

    - Tietokannan hallintaa
        - #1019 (tiedon poistaminen)
        - #943  (taulujen yhdisteleminen)
        - #944  (kaikkien taulujen lisääminen tietokanta hallitasivulle)
        - #1020 (tiedon poistaminen tietokannasta)

---        
- Prioriteetti #3

    - Raportit
        - #958  (raporttien tallentaminen)

---
- Prioriteetti #4

    - Kaikki muu PBL:ssa







