


# 2. Sprint
* ----------------------------------------------------------
    * Luonnoksien tekeminen loppuun

        * käyttöliittymän läpikäynti
            => viimeiset muutokset
                => Pekan ideat ja huolet
                    => kuinka toteutamme käyttöliittymän (staattinen vai dynaaminen)
                        => käyttöliittymän näppäileminen

        * palvelin
            => toimintamallien tekeminen loppuun
                * sensoridatan vastaanottaminen [DONE]        
                * tiedon hakeminen [-]
                    * Henkilökunta
                    * Sensorit
                    * Rakennukset
                * tiedon hallintaa [-]
                    * Henkilökunta
                    * Sensorit
                    * Rakennukset
                * palvelin toimintaa [-]                
                    * tiedostojen jako
                    * tietokantayhteys
                    * kirjautuminen

        * Huomitoita
            * KS. käyttöliittymä, minkälaista toiminnallisuutta palvelin kaipaa
            * tietuiden muokkaaminen, lisääminen ja poistaminen
            * KS. sensoridatan vastaanottaminen [Archimate] malli palvelin hakemistosta REPOsta