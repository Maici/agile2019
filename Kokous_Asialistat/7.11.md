


#### Mitä daily scrum meetingit ovat

    * max. 15 minuuttiset
    * ei ideointia enää
        -> katsotaan daily scrum meeting jälkeen
            -> kysymykset otan ylös ja vastaan niihin (jos pitkiä)
    * alkaa ajallaan

    * äänichatti

    * kaikki käy läpi vuorollaan:
        1. Mitä teki eilen auttaakseen tiimiä etenemään sprintissä.
        2. Mitä tulen tekemään tänään, jotta tiimi pääsee eteenpäin sprintissä.
        3. Ongelmia, jotka saattaisivat/ haittaavat tiimin etenemistä sprintissä.

    * isommat ongelmat käydään läpi niiden kanssa keitä asiat koskevat

    * seisaaltaan pidettä kokous

    * jokainen kehitystiimistä pääsee perille, mitä muut tekevät

    * ongelmat esiin, vaikkakin kuinka pieniä olisivat



* Scrum masteri pitää työtä kehitystiimin sisällä



* Seuraava Pekan sparraus on tiistaina 12.11. mahdollisesti aamulla saamaan aikaan ~9:00
* Pekan sparraus ajat aina tiistaisin?

* LIKO tyyppejä päivitetty tämän hetkisestä tilasta

* Ohjaustunti wikisivu Azureen

* Perjantai ohjaustunti 11- 12
* https://eu.bbcollab.com/guest/891e4dbfbe4f4811bc0a4d50116a46a3

* Ohjaustunnin valmistelut

    * puheenjohtaja     =   Mika
    * kirjuri           =   Lauri

* Seuraavilla kerroilla vaihdetaan rooleja
    -> Roolit kiertää


* Asialistan teko palaveriin

    * Ryhmän omat asiat
    * Ryhmän ja yksittäisten jäsenten tilannekatsaukset
    * Scrumlistan läpikäynti
    * ohjaajien asiat


* Ohjeistus: https://moodle.karelia.fi/course/view.php?id=3692#section-10


#### EHDOTUKSIA

* Mahdollisesti käytetään .md päätteisiä tiedostoja .txt sijaan?
    -> "tyylitys"
        -> "selkeämpi" luettava

* Azure repon käyttöönotto?
* mahdollisesti gitlab tai jokin muu?


#### MITEN EDETÄÄN --->

* Sprint #1
    -> käyttöliittymän ja palvelimen prototyyppi/ luonnoksien tekeminen

    >>> Jonka jälkeen >>>

    -> Puretaan luonnokset pienempiin palasiin
        * helposti muokattavissa
        * selkeät selitykset jokaiselle toiminnolle luonnoksessa


* Sprint #2 
    -> Käyttöliittymän prototyypin esitys [PEKALLE]
        -> Huomautuksien/ ehdotuksien kirjaaminen ylös
            -> punnitaan muutosehdotukset
                -> tehdään mahdolliset muutokset käyttöliittymä luonnokseen
                    -> tarkistetaan palaset, tehdään niihin mahdolliset muutokset


    >>> ALOITETAAN KEHITYSTYÖ palasien perusteella


#### MITEN EDETÄÄN LOPPUU


#### ONGELMIA VIIME TEHTÄVISTÄ ???
    - listaa ongelmat tehtävänannoissa
        -> vältetään jatkossa samoja virheitä

        -


#### KYSYMYKSET ???
    - listaa kysymykset ja niistä muodostuvat tehtävät tänne.

        - 

#### KYSYMYKSET LOPPUU




#### PROTIP: discord yv max pituus 2000 merkkiä







* Arkkitehtuuri

    * Toimintamalli
        - Yksi yhtenäinen palvelin kaikelle datan käsittelylle/ tarkkailulle
            VAI
        - tekeekö jokainen rakennuspalvelin omia juttuja?
        - Toimiiko rakennuskohtainen palvelin pelkkänä datan välittäjänä, jolloin kaikki toiminta olisi
            keskitettynä pääpalvelimelle.
                -> vähemmän "liikkuvia osia"
                    vai pyöriikö näillä palvelimella oma sovellus?
        - Rakennuskohtainen "palvelin" kerää sensoreilta dataa ja lähettää sen pääpalvelimelle
            - saadulla sensoridatalla on tunnistus id, jolla saadaan tietokannasta haettua
                viitearvot kyseiselle sensorille.
                    - arvoja verrataan toisiinsa.
                        -> mikäli OK, ei toimenpideitä
                        -> mikäli EI, rakennus-/ sensorikohtainen tai muu? ilmoitus luodaan ja jaetaan eteenpäin
        - palvelimen konfigurointia
            - mitä pitäisi/ voisi konfiguroida?
                * palvelimen sammutus/ uudelleen käynnistys, sensoreiden hallinta, tiedonlähetyksen tahti, tiedonkeruun lopettaminen, muuta?


    * Palvelin
        - kommunikaatio käyttöliittymän kanssa
        - kuittaus toiminnon jälkeen (onnistui/ epäonnistui)
        - monet käyttäjät käyttävät samaan aikaan palvelinta? (mitä mahdollisia ongelmia)
        - tietoturva
        - lokin pitäminen käyttäjien toimenpiteistä

        - SMART vahti?

        - Sensoridatan tarkkailu
        - 
        - ilmoituksien luonti poikkeavasta datasta
        - custom varoituksien luonti
            - mitä parametrejä voisi asettaa?
                * kaipaa selkeämmän mallin, jotta lopullinen idea saadaan kasaan
                    - arvo/ arvot, vertailu, muuta?

    * Tietokanta

        - [TABLE] kiinteisto
            (malli) -> id, nimi, osoite, FOREIGN KEY k_r_ID
        - [TABLE] kiinteisto_ryhma
            (malli) -> id, nimi
        - [TABLE] henkilokunta
            (malli) -> id, nimi, FOREIGN KEY k_r_ID
        - [TABLE] sensorit
        - [TABLE] varoitukset

        - Alustava raakile tietokannasta.
        - Work in progress!
        - Voiko kiinteistö ryhmällä olla enemmän, kuin yksi henkilökunnan jäsen?
        - voiko henkilökunnan jäsenellä olla enemmän, kuin yksi kiinteistöryhmä?
            - monen suhde moneen, yhden suhde moneen, monen suhde yhteen?
                * kaipaa testailua ja ideointia

    * Käyttöliittymä
        - kommunikaatio palvelimen kanssa
        - kirjautumisjärjestelmä

        - kiinteistöt lajiteltu ryhmiin
        - ilmoituksien järjesteleminen tärkeysjärjestykseen
        - useamman kiinteistön tietojen tarkasteleminen yhtäaikaa (vertailu?)
        - "kiinteä" valikko nopeuttakseen navigointia
        - käyttöliittymän kustomointi
            - väriteema
                - [EPIC] modulaarinen toteutus?
                    * esim. valikon siirtäminen, asioiden järjesteleminen näytöllä

        - hälytyksien haku automaattisesti tietyin väliajoin
            - [EPIC] socket.io
                -> socket.io käyttö olisi kyllä mielenkiintoista
                    * selvitellään myöhemmin, kunhan saadaan hommia eteenpäin
                        - mahdollisuuksia on monia

        - sivuja
            - etusivu
            - kiinteistödata
            - sensoridata
            - [WAITS_APPROVAL] rakennusryhmien, henkilökunnan ja muun vastaan hallinta käyttöliittymästä
                -> ainoastaan mahdollista työpöytä versiossa?
                    * vähentää mahdollisien virheiden tekemistä, mikäli puhelimella mahdollista?

        
        - graaffista helppolukuista/ näyttävää/ eeppistä tietoa?
            - [EPIC] mikäli aikaa on ja innostusta oppia uuttaa
                -> mahdollisuudet avoinna


        - palvelimen asioita hallittava käyttöliittymästä

        - responsiivinen käyttöliittymä
            -> mobiili tien päällä
                * helppokäyttöinen
                * selkeä
                * turha tieto pois
                * oleellinen ainoastaan näkyville
                * mahdollisesti lisätietoa saatava?
                    -> tarkastellaan asiaa, kunhan tarkentuu asiat
            -> konttorissa järeämpää kalustoa
                * tiedon tarkasteleminen/ katsominen
                    -> eeppisiä 2d/ 3d toteutuksia?
                    -> tietoa tulisi pystyä järjestelemään
                        * ajanjakson mukaan
                        * kiinteistöjen
                        * muuta?
                            -> vertailuja eri rakennuksien välillä?
                                * [VILLI_IDEA] hae vedenkulutus / rakennuksen päivittäisellä käyttäjämäärällä
                                    -> voitaisiin alkaa miettimään mitä dataa rakennuksista voitaisiin saada
                                        -> mitä sille tehdään
                                            -> mitä data voi tehdä meille?

        - "Kriittiset" vaihtoehdot pois mobiilista
            - esim. sulje palvelimen


        - käyttäjällä profiili
            - profiilin mukauttaminen
        


