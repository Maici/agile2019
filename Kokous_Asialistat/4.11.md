

# Ryhmän muodostaminen - 4.11.2019

Kaksi viikkoa kurssia takana ja tämän hetkinen tilanne ryhmän edistymisessä, heikko. Itse ryhmäytyminen onnistui heti kurssin alussa hyvin, sillä saimme kasaan päivän aikana koko ryhmän, pois lukien Ville Holopaisen, kenestä ei ole kuulunut mitään, vaikka useita sähköposteja kyseiselle henkilölle on tullut laitettua.

Osa ryhmän jäsenistä ovat passiivisia kokouksissa, joita olemme pitäneet, tosin kyseiset kokoukset eivät ole tuottaneet oikeastaan mitään tähän mennessä, joten olemme vieläkin kahden viikon jälkeen alkupisteessä.

Ilmoittauduin ryhmämme johtajaksi ensimmäisellä viikolla, joten ryhmämme tilanteesta olen ainakin osittain syyllinen. Kiinnostukseni kurssia kohtaan, sekä ryhmän johtamiseen on vaikuttanut huomattavasti osa ryhmämme jäsenten passiivisuus. Kommunikoidessamme kokouksissa vain muutama henkilö ottaa aktiivisesti osaa keskusteluun, kun loput henkilöistä ovat hiljaa tai eivät ole edes viitsineet tulla paikalla ja jättäneet ilmoittamatta olevansa poissa kokouksista.

Kuten jo edellä mainitsin, niin kaksi viikkoa kurssista on takana, eikä ryhmämme ole saanut mitään aikaan. Nyt uudestaan motivoituneena johtamaan ryhmäämme alan suunnittelemaan huomattavasti tarkemmin aikataulua ryhmällemme ja kuinka jatkossa tulemme toimimaan eritilanteissa, kuten kokukset, työskentely jne. Alan myös pitämään kurssipäiväkirjaa ryhmämme tekemisistä ja omista ajatuksistani, jota olisin voinut alkaa tekemään jo kurssin alussa, mutta sille emme voi enää mitään. Joten tästä eteenpäin toivon ryhmämme pystyvän etenemään kurssilla eteenpäin.


## "Hätäkokous" Maanantai 14:00

Kuten jo enteilin kurssin alussa, työskentely tulee olemaan vaikeaa, sillä vastuun ottaminen kurssista tuntuu olevan useammalle henkilölle haaste. Kurssin alussa tiedustelin olisiko vapaaehtoisia ryhmämme johtajaksi, johon yksi totesi "emme tarvitse sitä vielä", toinen kieltäytyi ja loput olivat hiljaa tai poissa sopimastamme kokouksesta. En omaa paljoa kokemusta ryhmässä työskentelystä, mutta mitä tähän mennessä olen oppinut on se, että jos ryhmässä ei ole johtajaa, niin usein ellei aina, lopputulos on huono ja tähän on myös muistissa viime vuonna käymäni ohjelmistotuotanto kurssi, joka käytännössä alkoi ja todennäköisesti tulee olemaan tarkka kopio tämän kurssin tapahtumista. Joten ilmoittauduin ryhmämme johtajaksi, jotta saisimme kurssin alulle ja sitä myötä etenemään.


### Kokouksessa käytävät asiat

*   Ryhmämme jäsenten roolit kurssilla.
    *   scrummaster:    -
    *   tuoteomistaja:  -
    *   kehitystiimi:   -

*   Ryhmän jäsenten panos kurssityöhön

*   Pois jääneet opiskelijat ryhmästämme.
    *   Ville Holopainen

*   Työskentelyaikataulu
    *   Viikoittainen kokouspäivä ja kellon aika
        *   "Pakollinen" kaikille TIKOn jäsenille
    *   Poissaoloista ilmoitettava
    *   Työskenteleminen
        *   Kysykää apua

*   5.11. Pekan 15 minuuttinen

*   Mitä olemme tekemässä tällä kurssilla.
    *   ?

*   Viikon 44 tehtävä ja sen palautus.


### Kokouksen yhteenveto

Roolien jakaminen ryhmässämme onnistui nopeasti. Kokoonpano ryhmässämme on seuraavanlainen:
*   Kehitystiimi
    *   Riku
    *   Lauri
    *   Antti
    *   Jani

*   Scrummaster
    *   Valeria

*   Tuoteomistaja
    *   Mika


Ville Holopaista ei ole näkynyt koko kurssin aikana


Daily scrum tapaamiset
*   Aikataulutus:
    *   ma, ke, to
        *   9:00
    *   to, pe
        *   14:00


##### Mitä ollaan tekemässä:

Kokonainen järjestelmä, johon liittyy tietokanta, palvelin ja käyttöliittymä.


Tehtävät:
*   Karkea prototyyppi huomiselle:
    Riku vastuussa

*   Moodletehtävä viikko 44
    Mika


Muuta:
    Scrum checklist tullaan tekemään myöhemmässä ajanjaksossa.



## "hätäkokouksen" jälkeen

Kokous onnistui mielestäni hyvin, saimme jokaisen kohdan listalta tehtyä. Tehtävän jaossa ilmeni ongelmia, mutta kyseessä oli väärinymmärrys, jonka jälkeen tehtävät saatiin jaettua. Kokouksen jälkeen aloitimme tehtävien tekemisen.

Tällä hetkellä olen positiivisella mielellä kurssia kohtaan, sillä ryhmämme näyttäisi toimivan ja saamme mahdollisesti tehtyä jotain aikaiseksi tällä kurssilla.






