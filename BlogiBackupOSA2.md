

# Prohjekti blogi jatkuu... Osa 2



Tosin, jos olisin 'pystynyt' olemaan mukana koko kurssin ajan tuoteomistajan roolia noudattaen, näin ollen  meillä olisi aina ollut jokaiselle sprintille kunnollinen SBL, jonka kehitystiimi olisi rakentanut ja sitoutunut tekemään sprintin aikana. Näin ollen olisi asioita pitänyt saman tien karsia pois ja mahdollisesti jopa keskeyttää koko kehitystyö, sillä nopeasti tuli asia ryhmän sisällä selväksi, että ryhmä ei pidä sisällään oikeastaan mitään taitoja, joita olisi tarvittu projektin valmistamiseen. Näin ollen minusta tuli osa kehitystiimiä, jonka tarkoitus oli, että saisimme enemmän aikaan. Aluksi kaikki meni hyvin, tosin huomatessani yhden jäsenen panoksen ryhmässämme, aloin miettimään sitä, miksi täällä kenenkään tulisi tehdä mitään. Kyseinen henkilö sitoutui kurssin alussa tekemään asioita, kuten muutkin. Näin ainakin ajattelin, sillä hän ei sanonut mitään vastaan tai muutenkaan reagoinut asioihin. Hän ei ole laittanut työtuntejaan ylös clockifyihin, hän ei myöskään ole käyttänyt REPOa kuin vasta kerran (jakaa/ jakoi tiedostoja discordissa dm:nä), hän ei myöskään ole tuonut mitään arvoa ryhmälle, olisi ollut aivan sama, jos häntä ei olisi edes näkynyt koko kurssilla, tai jopa parempi, jolloin olisin itse 'pystynyt' olemaan 'paremmin' mukana kurssilla. Mikäli tämä henkilö kurssista saa arvostelun, oli se sitten hylätty tai numero (suoritettu), on se minun mielestäni hyvin outoa ja haluasin mielelläni nähdä perustelut arvosteluun. Lisään vielä tähän se, jos hän arvostelun kurssista saa, niin näin ollen ryhmämme muut jäsenet ansaitsevat automaattisen 5 ilman minkäänlaista todentamista asioista, joita he ovat suorittaneet kurssilla. Mikäli haluatte todentamista ryhmämme jäsenistä, ketkä ovat tehneet ja osallistuneet ryhmätyöhön voin ne mieleläni teille tuoda esiin dokumentoiden.





### 05.12.2019

- http://agile19a3.westeurope.cloudapp.azure.com:8080/

Tänään kauheasti asioita ei tullut tehtyä näin projektin eteenpäin viemisen mielessä. Suuri aika tuli käytettyä Scrumin periaatteiden opiskelussa/ katselemisessa ja tähän vielä lisäksi kurssiblogin kirjoittamista sekä kurssin retrospektiiviä, jossa käyn kohta kohdalta, mitä teimme ja missä olisi voinut tehdä toisin.

Laitoin palvelimen käyntiin Azureen ja tässäkin tietenkin olisi ollut hyvin mielenkiintoista, jos olisimme oikeasti tehnyt jonkinlaisen järjestelmän, josta olisi ollut oikeasti hyötyä. Kun minulla ei ollut pääsyä oikeisiin sensoreihin ja tätä myöten saada oikeaa käsitystä, miten asiat toimivat, niin kurssin tehtävänanto omalta osaltani jäi hieman vajaaksi. Tietenkin tämä ajatus ei ole kovinkaan realistinen, mutta näin ollen olisi ollut mielenkiintoista nähdä, tehdä ja oppia uusia asioita. Käytännössä kurssin tehtävänanto olisi voinut olla kehittää uusi opiskelun seurantajärjestelmä, kuten peppi. Tai muu vastaava, joka olisi ollut huomattavasti selkeämpi, kuin nykyinen tuoteideamme, toisaalta olisimme voineet myös tehdä hyvin yksinkertaisen sovelluksen, mutta siinä taas tulee vastaan se, miksi käytimme kurssilla oikeaa asiakasta, jolloin käytännössä 'tuhlaisimme' vain hänen aikaansa.



### 06.12.2019

kurssin retrospektiivin kirjoittelmista


### 09.12.2019

Päivän työt aloitin loppuesityksen tekemisestä, tosin nopeasti tuli huomattua, että järjestelemästä löytyy niin monta puuttellista asiaa käytettävyyden sekä virheiden kannalta, että olisi jo hyvä aika lopettaa koko projekti. Koitin tehdä esityksen suunnittelua järjestelmän näkökulmasta, mutta tämän hetkinen tuotteen tilanne on todella heikko, joten jätin homman kesken ja palaan illemmalla tai huomenna väsäämään jonkinlaiset esityksen. Mitä se pitää sisällään on minulle vielä mysteeri.

Loppuesityksen teosta, kun ei tullut mitään, niin viimeistelin ryhmänarvioinnin kurssiblogiini.


### 10.12.2019

Tämä päivä on tullut tehtyä loppuesitystä yksin sekä samalla etsittyä bugeja järjestelmästä, sillä kukaan muu ei niitä ole tehnyt.

Pieniä muutoksia järjestelmään tein myös esitysmateriaalia silmällä pitäen.

- https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_git/Ryhm%C3%A4%20A_1/commit/3331a327a821992c4506a7c6f8ec2be706d72cde/

#### Bugeja

![](https://i.imgur.com/0rxLeHl.png)

![](https://i.imgur.com/PTZgjex.png)

![](https://i.imgur.com/Zgn0qSK.png)

- Nämä bugit ovat Jani yksin korjannut.

- Loppuesityksen tekeminen olisi ollut järkevämpää aloittaa tekemään hieman aikaisemmin ja todennäköisesti olisi myös ollut apua, jos muut ryhmän jäsenet olisivat osallistuneet tekoon.


### 11.12.2019

Töiden esittelyn jälkeen olin todella pettynyt omaan suoritukseen kurssilla, sillä nähtyäni mitä muut olivat saaneet aikaan, oli se huomattavasti hienompaa, kuin meidän tuotoksemme. Tosin en voi sanoa, että tämä tuli itselleni yllätyksenä, sillä osasin odottaa, että muilla tulee olemaan hienompia töitä ja niinhän heillä näytti olevankin.




## Sprintti 5 (3.12.-12.12.)

### Aikataulun seuranta:
#### https://clockify.me/bookmarks/5dfbf37ab1a5ec527331ece9

### Omia kommitteja repoon
- https://dev.azure.com/tiko-agile19/_git/Ryhm%C3%A4%20A_1/commits?fromDate=2019-12-02T22%3A00%3A00.000Z&toDate=2019-12-12T21%3A59%3A59.999Z&user=Mika%20Marjomaa&userId=61fd4bcb-cb25-6311-9036-cef75664cad3


Tämä sprintti tuli oikeastaan dokumentaation osalta tehtyä ryhmän jäsenten ja ryhmän arviointien kirjoittamisella sekä katsaus kokonaisuudessaan, mikä ryhmässämme epäonnistui ja miten asiat olisi pitänyt tehdä toisin, jotta olisimme saanet jotain hienoa aikaiseksi.

- arvionti:
    - https://gitlab.com/Maici/agile2019/blob/master/BlogiArviointi.md

- kurssin retrospektiivi:
    - https://gitlab.com/Maici/agile2019/blob/master/BlogiRetro.md


**Mikäli olet tänne asti jaksanut lukea tekstiäni, niin olet varmaan huomannut, että asiat, joita olen käynyt läpi, ovat olleet hyvin ranttimaisia. Tarkoitukseni ei ole ollut missään vaiheessa 'hyökätä' ketään vastaan. Olen vain 'tuohtunut' siitä, mitä odotin kurssilta ja mitä se todellisuudessa oli. Näiden kahden asian ero oli huikea, jolloin näitä muistelmiani kirjoittaessa on saattanut tunteet hieman olla pinnalla. Pyrin aina välttämään vihaisena kirjoittamista ja omasta mielestäni onnistuin siinä hyvin. Olen pääsääntöisesti pettynyt itseeni tällä kurssilla. Kaikki huomiot, joita olen kirjoittanut tänne blogiin ovat ainoastaan minun, joten saatan olla väärässä, mutta seuratessani koko kurssin ajan ryhmän työntekoa, ei se kyllä hyvältä näyttänyt. En todellakaan sano, että oma työn tekeminen kurssilla olisi ollut kovinkaan kummoista itsestäni, mutta voin sanoa, että muutama ryhmän jäsenen panos kurssilla oli naurettava. Johtuisi se mistä johtui, mutta näitä ei koskaan tuotu esiin, vaikka useasti niistä puhuin ryhmässä.**


### Työpanokseni ja sitoutumiseni projektiin

Viimeiset kiinnostuksen rippeet kurssia kohtaan hävisi, kun näin muiden tekemät työt, sillä jos ryhmämme olisi ollut toimiva, niin olisimme mahdollisesti voineet toteuttaa jotain oikeaa, joka oli itselläni odotus kurssin alussa, mutta nopeasti kurssin alussa todellisuus iski minuun ja sen näköinen on myös työt, jonka saimme aikaan. Itsealoitteisuuden puutos ryhmässämme, on pistänyt minut useampaan kertaan miettimään sitä, jos en olisi itse laittanut asioita eteenpäin, niin olisimmeko saaneet juurikaan mitään aikaiseksi.

Pariin kertaan tämä asia onkin jo tullut esiin näissä kirjoitelmissani, joka on se, kun kehitystiimistä löytyi ainoastaan yksi henkilö, joka osasi itsenäisesti tehdä asioita ja ongelmatilanteissa etsiä ratkaisuja internet.comista. Miksi muut jäsenet eivät tähän pystyneet on minulle mysteeri sekä se, miksi he eivät hakeneet opettajilta apua tai opetelleet itsenäisesti asioita. Oliko tämä liian iso haaste heille, sitä en tule koskaan tietämään. Mutta se asia on varma, että jos olisin tiennyt muiden 'taidot', mitä ne oikeasti olivat heti kurssin alussa, niin tuoteidea olisi ollut hyvin erilainen. Käytännössä kurssin töiden jakauma jakautui Janin ja minun välille. Näihin lasken vain tehtävät, jotka ovat tuoneet meidän ryhmässämme arvoa. Eli käytännössä tämä kurssi oli parityö minulle, tosin jos en olisi joutunut todistamaan vierestä katselijoita, niin olisi minulla hyvin todennäköisemmin ollut motivaatiota tehdä huomattavasti enemmän asioita, kuin mitä tuli tehtyä.

Kuten, jo viestintä osiossa tässä tulen kertomaan siitä, miten olisi ollut parempi ryhmällemme, että markkinointipuoli olisi osallistunut suunnitteluun, niin olisimme mahdollisesti tehneet tuotteen, jolla olisi ollut 'oikeaa' arvoa. Tämä olisi ollut hyvä tietää heti kurssin alussa, sillä esittelyjä kuunnellessa, useampi ryhmä oli tehnyt todella tiivistä yhteistyötä molempien opiskeljoiden kanssa.

Kuten jo olen opintojeni aikana nähnyt, on usealle opiskelijalle lähestulkoon mahdoton tehtävä opiskella itsenäisesti asioita ja tämä tuli kyllä esiin ryhmässämme. Sinänsä ikäviä tämmöiset ryhmätyöt, joissa omatut taitojen erot ovat hyvin suuret. Miksi 'vaativimpiin' kursseihin pääsee osallistumaan henkilöt, jotka eivät välttämättä ole päässeet 'perus' kursseja läpi, on minulle ikuinen mysteeri.

Loppuun sanottakoon vielä se, että isoin pettymts kurssilla oli se, kun odotukseni kurssia kohtaan lukuvuoden alussa olivat todella suuret, sillä luulin, että pääsisin vihdoin tekemään kehitystyötä ryhmässä, joka omaa taitoa tehdä asioita. Tähän lisäksi käyttäisimme projektinhallintaa, jolloin ongelmia ei pitäisi ilmetä. Lisäksi ennen kurssin alkua tuli tieto, että teemme kurssin markkinointiopiskelijoiden kanssa, jolloin aloin oikeasti kiinostumaan kurssista vielä enemmän, mikäli se oli mahdollista. Kyselin asioita arkkitehtuuri kaveriltani rakennuksista mitä ne tarkalleen ottaen ovat ja miten niitä pitäisi ylläpitää. Odotukseni kurssia kohtaan olivat hyvin hyvin korkeat, olin ajatellut, että voisimme oikeasti tehdä jotain suurta. Mutta kuten tähän asti elämässäni olen todistanut, että mikään ei mene niin, kuin odottaa, joten oikeastaan pidän ihmeenä, miksi edes olen ihmeissäni vielä tähän päivään asti ryhmämme toiminnasta. Käyttännössä odotukseni kurssia kohtaan olivat 100/ 100, jonka jälkeen odotukseni tippuivat hyvin nopeasti miinuksen puolelle.


### Oman työn seuranta ja kehittäminen

- https://gitlab.com/Maici/agile2019/blob/master/BlogiAjanSeuranta.md

Ylhäältä löytyy ajan seurantani kurssilla. Aikaa olisi tullut käytettyä huomattavasti enemmän, mikäli kiinnostukseni kurssia kohtaan olisi säilynyt samana, mitä se oli ennen kurssia. Kuten useamman kerran olen sanonut, niin jos joku olisi maksanut meille palkkaa tekemästämme työstä, niin olisi hyviun todennäköistä, että joutuisimme kaikki vankilaan hyvin pitkäksi aikaa. Käyn läpi ryhmän jäsenten arvionnissa jokaisen tuomaa arvoa ryhmälle.

Kuten tähänkin asti olin merkannut PBL taskeihin suunnilleen ajan/ pisteitä, kuinka paljon ne merkitsevät projektissamme. Nämä arviot ovat osittain linjassa siinä, mitä mahdollisesti olisin itse niihin käyttänyt aikaa. Onko kukaan sitten muuttanut niitä sen jälkeen, niin siitä en tiädä.

Työn tekemistä en ole varsinaisesti parantanut mitenkään, toki jotain pieniä muutoksia olen siellä sun täällä tehnyt, mutta kun ryhmä on niin passiivinen, kuin olla ja voi, niin käytännössä omat tekemiseni olen joutunut tekemään lähestulkoon aina yksin. Näin ollen kävin itse Scrum periaatteiden opiskelemisen itsenäisesti läpi, kuin sen itse ymmärsin olevan kurssin tarkoitus, johon olisin toivonut, että muutkin ryhmän jäsenet olisivat tehneet samoin. Sinänsä oli mielenkiintoinen kokemus nähdä, kuinka kukaan ei juurikaan tee mitään ja sen perusteella kirjoitella kurssin asioista retrospektiivi, jossa käyn läpi kaikki asiat, joissa itse epäonnistuin ja kuinka ne olisi pitänyt tehdä toisin. Aktiivisempi ryhmä olisi todennäköisesti auttanut kurssia käydessä. Vastaisuudessa ryhmätyöt, joiden kokoonpano on samanlainen, kuin nyt nämä kaksi kurssia tähän asti ovat olleet, niin miellummin tekisin kaiken itse, kuin katsoisin vierestä toisten sormien pyörittämistä. Lisään vielä tähän sen, että minulle on aivan sama osaako joku jotain vai ei, mutta kun mitään ei juurikaan tehdä, niin se pistää itseni miettimään hyvin syvällisesti, mikä on edes tämän koulun tarkoitus.

Kuten kurssin retrospektiivissä käy ilmi, niin omalta osaltani lähestulkoon kaikki asiat epäonnistuivat, jos niitä verrataan siihen, miten olisin halunut toimia aktiivisessa ryhmässä.

### Reflektio- ja itseohjautuvuusosaaminen

Ryhmässämme oli omasta mielestäni vain harva henkilö, joka oli aktiivisesti osa ryhmää ja toi arvoa projektiimme. Motivaation kadotessa täysin, keskityin ainoastaan tekemään itselleni kuuluvia asioita, jonka seurauksena jätin muun ryhmän omaan arvoonsa, sillä osa heistä ei koskaan muuttanut työtapojaan huomautuksistani huolimatta. Tämän kurssin asiat omalta osaltani olivat mittapuun toisessa päässä verratuna muihin kursseihin, kuten peligrafiikan-, pelikehityksen perusteen, c++-, java ohjelmointi, sql kurssi sekä oikeastaan kaikki kurssit, jotka ovat olleet suoraan yhteydessä ohjelmointiin/ pelien tekemiseen.

Nyt jälkikäteen olisi ollut parempi, jos olisin ollut osa kehitystiimiä, tosin en usko, että ryhmästämme olisi löytynyt helposti jäsentä, joka olisi vapaaehtoisesti ollut tuoteomistaja, mikä sinänsä on harmi, sillä elän vieläkin harhaluulossa, että koulussa opiskellaan uusia asioita.

- Heikkouksiani:
    - Hankalien ihmisten kanssa toimiminen
    - Motivaation laskeminen itsestäni riippumattomista syistä
    - Täydellisyyden havitteleminen, tosin en menetä yöunia epäonnistumisista, sillä opin parhaiten virheistä. Tämä johtaa korkeisiin odotuksiin muilta jäseniltä.
    - Itsekriittisyys, tosin auttaa huomattavasti asioiden parantamisessa. Käytännössä double-edged sword.
    - Välillä ymmärrys loppuu ihmisten kanssa.


- Vahvuuksiani:
    - Halu tehdä asioita parhaimmallani osaamallani tavalla ja kehittää itseäni jatkuvasti.
    - Itsenäisesti uuden oppiminen ja asioiden soveltaminen.
    - Uuden tiedon etsiminen itsenäisesti.
    - Kyky toimia osana **toimivaa** ryhmää.
    - Itsealoitteisuutta
    - Virheistä oppiminen
    - Itseni jatkuva tarkkaileminen, missä voisin parantaa ja mikä onnistuu.
    - Kommunikaatio ihmisten kanssa, jotka ottavat työn tosissaan.
    - Käytännössä kaikki, mikä liittyy tietotekniikkaan, niin olen niistä asioita hyvin kiinnostunut.
    - Kyky sopeutua lähestulkoon kaikissa olosuhteisiin.
    - Johtajuus, asioiden alulle laitto.


### Omien työtehtävien hallinta

Tämänkin osioin oikeastaan kirjoitin kurssin retrospektiiviin, jossa käyn läpi kohta kohdalta minulle kuuluvia tehtäviä ja kuinka ne olisi pitänyt tehdä toisin, jotta menestys olisi ollut taattu ryhmällemme. 

Toimintatavat itselleni olivat selkeät, mutta rikkinäinen ryhmä vei minun motivaation mennessään, jolloin asioita tuli tehtyä puolilla valoilla.


### Oma viestintä

Heti kurssin alusta alkaen olisi pitänyt tehdä tiiviimpää yhteistyötä markkinointiopiskelijoiden kanssa ja kehitellä tuoteidea heidän kanssaan. Tämä tietenkin epäonnistui totaalisesti, sillä ensimmäisellä tunnilla kuulin, kuinka joku markkinoinnin opettajista sanoi jotain tämän tapaista: "tietojenkäsittelijät tekevät ohjelmiston, johon te sitten teette markkinointisuunnitelman". Tämä jostain syystä jäi mieleen hyvin voimakkaasti, jolloin tuoteidea oli meidän osalta huono. Jos olisin pystynyt tekemään ensimmäiset kaksi viikoa tai oikeastaan koko kurssin ajan aktiivisesti töitä sekä ottanut kurssin tosissaan, niin olisi markkinointipuoli ollut mukana suunnittelemassa tuotetta. Tietenkin tämän jälkeen myös olisi pitänyt katsoa, mitä kukin osaa tarkemmin ja vielä scrummasterin toiminta heti kurssin alusta alkaen sekä ryhmän jäsenten ongelmien tuonti esiin sekä uuden oppiminen ryhmän kehitystiimin jäsenille, tässä tapauksessa nodejs+html+jquery, jota kukaan muu ei tehnyt paitsi Jani. Vaikea edes kuvitella, mitä kurssista olisi tullut ilman Jania.





## Sprintti 6 (12.12.-20.12.)

### Aikataulun seuranta:
#### NaN

Ensimmäisen kerran kurssilla suunnittelun jälkeen alkoi suurin osa ryhmästä tekemään asioita. Sprintin suunnittelussa sovimme, että käyttäisimme tämän sprintin bugien etsimiseen ja niiden korjaamiseen sekä kirjoittaisimme käyttöohjeet tuotteestamme. Mikä sinänsä oli omasta mielestäni turhaa, sillä tuotettamme ei ole mitään järkeä edes käyttää prototyyppinä oikean tuotteen tekemisessä.

- https://dev.azure.com/tiko-agile19/Ryhm%C3%A4%20A_1/_wiki/wikis/Ryhm%C3%A4-A_1.wiki/247/J%C3%A4rjestelm%C3%A4n-betatestaus

Bugien etsimisessä/ testaamisessa pidän outona, sillä neljä henkilöä löysivät yhteensä vähemmän bugeja järjestelmästä, kuin minä itse järjestelmää käyttäessä esityksien tekoon (ICT-tuki kurssi sekä loppuesitys). Joko koodi meillä on hyvin laadukasta tai sitten kukaan ei tehnyt yhtään mitään, mikä ei olisi ollut minulle yllätys. Bugeja korjasi käytännössä Jani yksin, tosin itse saatoin pieniä muutoksia tehdä siellä sun täällä.

Käyttöohjeiden kirjoittamiseen osallistui Valeria, Riku sekä Lauri. Jälleen kerran Antti ei tehnyt mitään, joka sekään ei tullut yllätyksenä minulle.

Itse tullut tämä viikko käytettyä irtautumiseksi ryhmästämme ja näin ollen käyttäen omaa aikaani järkevämmin itsenäiseen oman projektin eteenpäin viemiseen, joka liitty datan hallintaan. Samalla tullut katsottua ohjelmointia lisää ja näin ollen syventäen osaamistani.







