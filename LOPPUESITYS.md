

# LOPPUESITYS - 11.12.2019

- Terve minunkin puolestani, Olen Marjomaan Mika

- Toimin tuoteomistajana projektissa



## Järjestelmän käynti lyhyesti

- Mikä on Smart Vahti?

- lyhyesti sanottuna kyseessä

- kiinteistöjen automatisoituun valvontaan suunniteltu järjestelmä



### Mitä järjestelmällä voi tehdä

- Järjestelmässä on kolme pääominaisuutta

    - **tiedon hallinta**
    - **sensoridatan vertailu**
    sekä
    - **rakennuksen yleisnäkymä**


- Seuraavissa dioissa käydään jokainen ominaisuus läpi yksitellen


[Oma_profiili]

- **Ennen sitä**

- Käyttäjä tuodaan sivulle kirjautumisen jälkeen

- tarkoitus toimia portaalina

    => oleellinen tieto

    - **valvonnan alaisuudessa olevat kiinteistöryhmät**

    - **Kiinteistöt**

        sekä

    - **Aktiiviset hälytykset kiinteistöissä**


---
- Ominaisuuksiin


    - **Tiedon hallinta**

        - Data keskitetty yhteen paikkaan

            - Tiedon selaaminen helppoa
            ja
            - Tiedon saatavuus kaikille työntekijöille taattu
            **riippumatta missä hän on**
                - Oli hän sitten toimistolla, matkalla kiinteistöön tai itse kiinteistössä

        **tiedon hallinnasta**
        - Tietueiden lisäämään, poistamaan ja muokkaaminen

        - Suodatin vaihtoehdot
            - sarakkeiden piilottaminen
            - sarakkeen haku
            - sarakkeen järjestäminen


[Tiedon_hallinta]


- Käyttäjä kulkee suuremmasta kokonaisuudesta pienempää

    - **Esimerkkinä lämpösensoreiden etsiminen useammasta huoneesta**
        - ei tiedä kiinteistöjä

        - Valitse asiat
            => siirry
        - Valitun 

        - SENSORIT
            - sarakehaku
                => valitsee sensorit vertailua varten


---
- 

    - **Sensoridatan vertailu**

        - Mukautettavissa käyttäjän tarpeiden mukaan

        - **uudet sensorityypit**
            - lisätään järjestelmään sensorin tiedot
                => sensoreita voidaan lisätä kiinteistöihin


        - **Kiinteistöjen sensorit tuottavat jatkuvasti dataa**
            => jonka seurauksena manuaalinen tarkkailu olisi hieman hankalaa
                - Jokaiselle sensorille pystytään luomaan omat viitearvot
            

            [TOIMINNALLISUUDESTA]
            - Palvelin vastaanottaa uuden arvon sensorilta
                - **vertaa uuttaa arvoa kyseisen sensorin viitearvoihin**
                    - uusi arvo viitearvojen ulkopuolella
                        => palvelin luo hälytyksen
                            => toimitetaan käyttäjille
                                => selvittämään asiaa järjestelmän avulla tai paikan päälle
                                    - riippuen tilanteesta

                            - Vähentäen tarkistuskierroksien tarvetta kiinteistöissä, jotka ovat järjestelmässä


[SENSORIDATAN_VERTAILU]

- Aiemman esimerkin lämpösensorit

- Käyttäjä voi tutkia kyseisten sensoreiden tuottamaa dataa

    - **sensoreita voi piilottaa**
        sekä
    - **aikaväliä muuttaa haluamakseen**

        - helpottaa graaffin lukemista
            **ja**
        - yksityiskohtaisten raportin tekemistä

            [RAPORTIT]
                - raportit sisältävät aktiiviset sensorit ja aikavälin graafista

                - ongelmatilanteissa viikoittain



    - **Rakennuksen yleisnäkymä**


[RAKENNUKSEN_VALVONTASIVU]

- Käyttäjä näkee yleiskuvan kokonaisuudessaan kiinteistöstä

- Kiinteistön interaktiivinen näkymä
    - helpottavat navigointia kohteessa

--- 
- valinnut kiinteistön

    - Listat
        - Sensorit
        - huoneet
        - hälytykset

    - **Alhaalla kiinteistön pohjapiirustus**
        - johon käyttäjälle korostetaan valitsemansa huone tai sensori
            - helpottaa paikantamista kiinteistössä




[LOPPU]


- jos Kysymyksiä nousi, otan lopussa vastaan

- Annariina jatkaa tästä










