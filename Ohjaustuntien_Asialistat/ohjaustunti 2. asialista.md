


#### OHJAUSPALAVERI asialista 2. 14.11.2019 kello 13- 14


# Ryhmän asiat:

- kehitysideoita
    - clockifyin työnimikkeen paikalle azuren tehtävän ID + tehtävän kuvaus tai muu vastaava
    - äänestys pollin käyttö ryhmän sisällä eri asioista


- Daily Scrumeja alettiin tekemään oikeammalla tavalla
    - jossain vaiheessa veto siirtyy tuoteomistajalta scrummasterille
        - tähän menessä tapaamiset olleet enemmänkin kokouksia, joissa on päätelty erilaisia asioita ryhmän asioista.


- Sprtinttien suunnittelua pääpainotteisesti kehitystiimin ja scrummasterin toimesta sprintti 3. lähtien.
    - suunnittelu palaveri on tiistaisin


- Scrum retrospektiivi pidettiin
    - kauheasti siinä ei ollut käytävää, kun osa tehtävistä jäi tekemättä 1. sprintissä
    - kysymykset, joita siinä nousi, käytiin samantien läpi


- BackLogin käyttö parantunut entisestä
    - vielä hiottavaa
        - jatkossa mahdollisesti paranee


- LIKOn kanssa käyty keskusteluja toistemme tekemisistä
    - mahdollinen tilanneraportti aika ajoin molemmilta puolelta toiselle
    - mahdollisesti toimivan prototyypin testaamista
        - saavat paremman käsityksen mitä tehdään
            - syntyy mahdollisesti kehitysideoita



# Ryhmän tilannekatsaus

- Työntekemisessä ongelmia, lähinnä tehtävien tekeminen
    - asia otettu esille useamman kerran daily scrumeissa
        - tarkkaillaan työntekoa tarkemmin tämän 2. spritin loppuun
            - mahdolliset toimenpiteet


- Tänään (torstaina) pitäisi suunnittelutehtävät saada viimeinkin valmiiksi
    - kehitystyö alkaa mahdollisesti jo perjantaina, viimeistään 3. sprintissä
        - koodia aletaan hahmottelemaan ja tarkentamaan miten kaikki toteutetaan

- Palvelin          : toteutetaan NodeJS
- Tietokanta        : MariaDB
- Käyttöliittymä    : JQuery + eri JS kirjastoja käyttäen [TAI] JS frameworkilla
    * todennäköisesti JQuery


- Oman datan simulointi 
    * nopeasti tehty sensoridatan luova scripti löytyy

- Todella karkea demo siitä, kuinka järjestelmä toimisi
    1. rakennuspalvelin, joka tuottaa simuloitua sensoridataa
    2. data lähetetään pääpalvelimelle
    3. pääpalvelin vastaanottaa datan
    4. tietokantaa demossa vielä ei ole, mutta on helppo tehdä
    5. sen sijaan sensorin arvo menee palvelimen muistiin talteen.
    6. jonka käyttöliittymä pytää palvelimelta

- Mahdolinen demo esitys mikäli kiinnostusta nähdä käytännössä, miten toimii [VALMIIKSI_LAITETTU_PYÖRIMÄÄN_ENNEN_PALAVERIA]


- Wikiin on lisätty reippaasti tavaraa
    - kokouksien asiat
        - seuraavat lisäykset ovat daily scrum pieniä yhteenvetoja
    - pekan tapaamiset
    - LIKOn asioita
    - ohjauspalaverit
    - ongelmat

    - Kehitystyön edetessä, mahdollisesti sinne lisätään jonkinnäköistä dokumentaatiota tehtävästä järjestelmästä


- REPOa alettu käyttämään reippaalla tahdilla
    - tehtävät asiat eritelty REPOon
        - palvelin
        - käyttöliittymä
        - tietokanta
        - muut asiat
    - käytännössä kaikki, mitä tuotamme menee sinne (tuotteen kehityksessä)
        - ainut poikkeus vielä on suunnittelu tehtävien osaset
            - näiden lopullinen muoto menee REPOn myös
                - tehtävät, jotka jo valmiita, niin ne on mennyt REPOon jo



# Jäsenten tilannekatsaus

    * Mika

    * Antti

    * Lauri

    * Riku

    * Jani

    * Valeria



# Scrum lista



# Omia kysymyksiä


* Onko toteutuksessa mitään järkeä?

* Onko näin, että ensi tiistaina Pekan tapaamista ei ole?



