



# OHJAUSPALAVERI asialista 3. 22.11.2019 kello 12- 13


# Ryhmän asiat:

[DAILY_SCRUM]
- Daily Scrumit vetää Valeria päivittäin
    - 15 minuuttia
    - Käymme läpi vuorollaan, mitä tehnyt viime kerrasta
    - Pienet kysymykset/ ongelmat selvitämme samantien
    - Isompien ongelmien selvittäminen jätetään Daily Scrumin ulkopuolelle

[SPRINT_RETROSPEKTIIVI]
- Sprinttien 2. ja 3. retrospektiivi
    - Käydään ensi viikon tiistaina
    - Selvitämme miten ryhmän työskentely on onnistunut
        - ongelmat
        - työskentely
        - projektin tilanne

[LIKO]
- LIKOn kanssa jatkuvaa viestintää
    - Olivat pahoillaan, kun eivät tienneet Pekan tapaamisista
    - Osallistuvat seuraavan kerran, kun Pekka on paikalla
        - Esittävät omat asiansa, mitä ovat tehneet

[SPRINT_PLANNING]
- Sprintin suunnittelu pidettiin
    - Parannettavaa kyllä löytyy seuraavalle kerralle
    - Tehtiin asioita vähän miten sattuu

[PRODUCT_BACKLOG]
- Product backlogin ylläpito
    - Toimii jotenkin
    - Kaipaa työtä

[REPO]
- REPOn käyttö onnistunut koko porukalta
    - Lähestulkoon kaikki asiat menevät sinne
    - Poikkeuksena viestintä, joka toimii Azuren wikissä ja Discordissa

---
# Ryhmän tilannekatsaus

[TYÖSKENTELY]
- Työskentelyyn piti hieman puuttua jälleen kerran
    - Sprintin retrospektiivissä käymme läpi ongelmat
        - Mahdollisten ongelmien selvittäminen

[KEHITTÄMINEN]
- Kehitystyö alkanut
    - Palvelin toimii
    - Käyttöliittymä toimii
        - Toteutuksessa parannettavaa
            - 3. Sprintissä oli tarkoitus saada järjestelmä suunnilleen käyttökelpoiseksi
                - Seuraavissa sprinteissä lisäämme toiminnallisuutta ja siistimme toteutusta
    - 2D- pohjapiirrustus "sovellus" toimii
        - Idea on kasassa ja toteutus aloitettu
            - Kaipaa hiomista

[DEMOT]
- Demot voidaan käydä läpi tässä
    - Käyttöliittymä
    - 2D- pohjapiirrustus sovellus
    - Palvelin


---
# Jäsenten tilannekatsaus

- Collaboraten osallistujalistan mukaan


---
# Scrum Lista

- Ruksiteltu asioita ja palautettu ajallaan


---
# Omia Kysymyksiä






