

# OHJAUSPALAVERI asialista 5. 05.12.2019 kello 9- 10

# Ryhmän asiat:

---
# Ryhmän tilannekatsaus


- Työn esittely
    - Tehdään yhtenäinen esitys LIKOn kanssa

    - Esityksen tekeminen aloitettu


- Järjestelemän tilanne
    - Viimeinen sprintti hiotaan Pekan haluamat toiminnallisuudet järjestelmään
        - 2D rakennuksen näkymä


---
# Jäsenten tilannekatsaus

- Collaboraten osallistujalistan mukaan


---
# Scrum Lista

- Tämän hetkiset ruksit ovat todennettu


---
# Omia Kysymyksiä/ huomioita

- Minkälainen esitystilaisuus tulee olemaan?

    - Onnistuuko järkevästi collaboraten kautta?
    - Olisiko parempi esittäjien olla paikalla

- Projekti blogeista
    - luetteko kaiken, mitä blogeihin on kirjoitettu?

---
# Opettajan tuokio



