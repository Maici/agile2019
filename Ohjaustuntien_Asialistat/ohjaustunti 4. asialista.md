



# OHJAUSPALAVERI asialista 4. 29.11.2019 kello 9- 10


# Ryhmän asiat:

- Scrumin perusideat hallussa (tuoteomistajana)
    - PBL päivitetty asianmukaisemmaksi
    - Seuraavaan sprintiin pidetään kunnollinen sprintin suunnittelu
    - Tämän sprintin retrospektiivi ensi viikolla

- Tehtyjen asioiden dokumentaation parantaminen jatkossa

- Janin työpanos tässä sprintissä
    - superb

- Daily Scrum
    - Asiat tulee selville kaikille, mitä ollaan tehty
    - Pienet ongelmat selvitetty niissä
    - Isommat ongelmat hoidetaan Daily Scrumin jälkeen

- LIKOn porukka testaa/ kokeilee järjestelmää ensi viikolla
    - mahdollinen palaute
        - palautteen kannalta mahdolliset muokkaukset
            - mm. käytettävyys ja muut huomiot


---
# Ryhmän tilannekatsaus

- Mikäli kurssi alkaisi nyt, tekisin useita asioita eri tavalla
    - Keskittyminen tuoteomistajan asioihin
    - Selkeä tuotevisio =>
        - PBL
            - sprinttien suunnittelut
            - sprinttien retrospektiivit
    - Paremman dokumentaation ylläpitäminen

- Onnistunut tähän mennessä hyvin
    - Daily Scrumit

- Jonkinlainen järjestelmä kurssin lopussa
    - Hyödyllisyys/ käytettävyys asia erikseen

- Nopea esitys käyttöliittymästä halutessaan
    - miten toimii
    - mitä ollaan tehty
    - kokonaiskuva

---
# Jäsenten tilannekatsaus

- Collaboraten osallistujalistan mukaan


---
# Scrum Lista

- Ruksiteltu asioita ja palautettu ajallaan


---
# Omia Kysymyksiä/ huomioita

- Keskityn viimeiset viikot lähinnä tuoteomistajan asioihin

---
# Opettajan tuokio



